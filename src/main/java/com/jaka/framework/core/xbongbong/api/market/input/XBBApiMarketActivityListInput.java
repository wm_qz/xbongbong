package com.jaka.framework.core.xbongbong.api.market.input;

import com.jaka.framework.core.xbongbong.base.XBBAbstractAPIPageInput;

import java.util.List;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/22 17:31
 * @description：市场活动列表接口入参
 * @version: 1.0
 */
public class XBBApiMarketActivityListInput extends XBBAbstractAPIPageInput {

    /**
     * 表单id
     */
    private Long formId;
    /**
     * 条件集合
     */
    private List<Conditions> conditions;

    public Long getFormId() {
        return formId;
    }

    public void setFormId(Long formId) {
        this.formId = formId;
    }

    public List<Conditions> getConditions() {
        return conditions;
    }

    public void setConditions(List<Conditions> conditions) {
        this.conditions = conditions;
    }

    public static class Conditions {

        private String attr;

        private String symbol;

        private List<String> value;

        public String getAttr() {
            return attr;
        }

        public void setAttr(String attr) {
            this.attr = attr;
        }

        public String getSymbol() {
            return symbol;
        }

        public void setSymbol(String symbol) {
            this.symbol = symbol;
        }

        public List<String> getValue() {
            return value;
        }

        public void setValue(List<String> value) {
            this.value = value;
        }
    }

}
