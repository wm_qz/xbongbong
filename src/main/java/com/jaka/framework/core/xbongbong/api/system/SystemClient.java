package com.jaka.framework.core.xbongbong.api.system;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jaka.framework.common.core.base.IHttpUtils;
import com.jaka.framework.core.xbongbong.api.system.org.input.XBBApiDepartmentListInput;
import com.jaka.framework.core.xbongbong.api.system.from.input.XBBApiFormGetInput;
import com.jaka.framework.core.xbongbong.api.system.from.input.XBBApiFormListInput;
import com.jaka.framework.core.xbongbong.api.system.org.input.XBBApiUserListInput;
import com.jaka.framework.core.xbongbong.api.system.org.result.XBBApiDepartmentListResult;
import com.jaka.framework.core.xbongbong.api.system.from.result.XBBApiFormGetResult;
import com.jaka.framework.core.xbongbong.api.system.from.result.XBBApiFormListResult;
import com.jaka.framework.core.xbongbong.api.system.org.result.XBBApiUserListResult;
import com.jaka.framework.core.xbongbong.base.XBBDefaultAPIActionProxy;
import com.jaka.framework.core.xbongbong.base.XBBRequestMappering;
import com.jaka.framework.core.xbongbong.config.XBBAPIClientConfiguration;
import com.jaka.framework.core.xbongbong.util.SignUtils;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/22 14:16
 * @description：系统模块客户端
 * @version: 1.0
 */
public class SystemClient {

    private final IHttpUtils httpUtils;
    private final XBBAPIClientConfiguration configuration;
    private final SignUtils signUtils;

    public SystemClient(final XBBAPIClientConfiguration configuration,
                        final IHttpUtils httpUtils,
                        final SignUtils signUtils) {
        this.configuration = configuration;
        this.httpUtils = httpUtils;
        this.signUtils = signUtils;
    }

    /**
     * 用户列表接口
     *
     * @param input 入参
     * @return
     * @throws Exception
     */
    public XBBApiUserListResult getAPIUsetList(final XBBApiUserListInput input) throws Exception {
        input.setCorpid(configuration.getCorpId());
//        input.setUserId(configuration.getUserId());
        input.setCmdId(XBBRequestMappering.api_user_list.getMapperName());
        JSONObject jsonObject = removeSignData(input.toJsonString());
        return XBBDefaultAPIActionProxy.getInstance().doAction(input,
                XBBApiUserListResult.class,
                configuration,
                httpUtils,
                signUtils,
                input.toRequestBody(jsonObject),
                jsonObject);
    }

    /**
     * 表单模板字段解释接口
     *
     * @param input
     * @return
     * @throws Exception
     */
    public XBBApiFormGetResult getAPIFomGet(final XBBApiFormGetInput input) throws Exception {
        input.setCmdId(XBBRequestMappering.api_form_get.getMapperName());
        input.setCorpid(configuration.getCorpId());
        input.setUserId(configuration.getUserId());
        JSONObject jsonObject = removeSignData(input.toJsonString());
        return XBBDefaultAPIActionProxy.getInstance().doAction(input,
                XBBApiFormGetResult.class,
                configuration,
                httpUtils,
                signUtils,
                input.toRequestBody(jsonObject),
                jsonObject);
    }

    /**
     * 部门列表接口
     *
     * @param input
     * @return
     * @throws Exception
     */
    public XBBApiDepartmentListResult apiDepartMentList(final XBBApiDepartmentListInput input) throws Exception {
        input.setCmdId(XBBRequestMappering.api_department_list.getMapperName());
        input.setCorpid(configuration.getCorpId());
        input.setUserId(configuration.getUserId());
        JSONObject jsonObject = removeSignData(input.toJsonString());
        return XBBDefaultAPIActionProxy.getInstance().doAction(input,
                XBBApiDepartmentListResult.class,
                configuration,
                httpUtils,
                signUtils,
                input.toRequestBody(jsonObject),
                jsonObject);
    }

    /**
     * 表单模板列表接口
     *
     * @param input
     * @return
     * @throws Exception
     */
    public XBBApiFormListResult apiFormList(final XBBApiFormListInput input) throws Exception {
        input.setCmdId(XBBRequestMappering.api_form_list.getMapperName());
        input.setCorpid(configuration.getCorpId());
        input.setUserId(configuration.getUserId());
        JSONObject jsonObject = removeSignData(input.toJsonString());
        return XBBDefaultAPIActionProxy.getInstance().doAction(input,
                XBBApiFormListResult.class,
                configuration,
                httpUtils,
                signUtils,
                input.toRequestBody(jsonObject),
                jsonObject);
    }


    public final JSONObject removeSignData(String data) {
        JSONObject signData = JSON.parseObject(data);
        if (signData.containsKey("cmdId")) {
            signData.remove("cmdId");
        }
        if (signData.containsKey("signUtils")) {
            signData.remove("signUtils");
        }
        return (signData.isEmpty()) ? null : signData;
    }

}
