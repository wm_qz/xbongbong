package com.jaka.framework.core.xbongbong.api.contractOutstock.result;

import com.jaka.framework.core.xbongbong.api.contractOutstock.result.dto.list.ContractOutStockListListResult;
import com.jaka.framework.core.xbongbong.base.XBBAbstractAPIResult;

/**
 * @author ：james.liu
 * @date ：Created in 2022/12/2 14:00
 * @description：
 * @version: 1.0
 */
public class ProV2ApiContractOutStockListResult extends XBBAbstractAPIResult {

    private ContractOutStockListListResult result;

    public ContractOutStockListListResult getResult() {
        return result;
    }

    public void setResult(ContractOutStockListListResult result) {
        this.result = result;
    }
}
