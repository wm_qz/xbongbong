package com.jaka.framework.core.xbongbong.api.crm.customer.input;

import com.jaka.framework.core.xbongbong.base.XBBAbstractAPIInput;

import java.util.List;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/23 10:12
 * @description：客户添加协同人接口
 * @version: 1.0
 */
public class XBBApiCustomerDeleteCoUserInput extends XBBAbstractAPIInput {

    /**
     * 客户id
     */
    private Long dataId;
    /**
     * 协同人id,取‘用户列表接口’返回的userId
     */
    private List<String> businessUserIdList;

    public Long getDataId() {
        return dataId;
    }

    public void setDataId(Long dataId) {
        this.dataId = dataId;
    }

    public List<String> getBusinessUserIdList() {
        return businessUserIdList;
    }

    public void setBusinessUserIdList(List<String> businessUserIdList) {
        this.businessUserIdList = businessUserIdList;
    }
}
