package com.jaka.framework.core.xbongbong.api.contractOutstock.input.dto.save;

/**
 * @author ：james.liu
 * @date ：Created in 2022/12/2 14:07
 * @description：
 * @version: 1.0
 */
public class ProV2ApiContractOutStockAddAddress_2 {

    private String address;

    private String province;

    private String city;

    private String district;

    private ProV2ApiContractOutStockAddAddress_2Location location;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public ProV2ApiContractOutStockAddAddress_2Location getLocation() {
        return location;
    }

    public void setLocation(ProV2ApiContractOutStockAddAddress_2Location location) {
        this.location = location;
    }

    public static class ProV2ApiContractOutStockAddAddress_2Location{

        private Float lon;

        private Float lat;

        public Float getLon() {
            return lon;
        }

        public void setLon(Float lon) {
            this.lon = lon;
        }

        public Float getLat() {
            return lat;
        }

        public void setLat(Float lat) {
            this.lat = lat;
        }
    }
}
