package com.jaka.framework.core.xbongbong.api.contractOutstock.input;

import com.jaka.framework.core.xbongbong.base.XBBAbstractAPIInput;

/**
 * @author ：james.liu
 * @date ：Created in 2022/12/5 10:23
 * @description：
 * @version: 1.0
 */
public class ProV2ApiContractOutStockDetailInput extends XBBAbstractAPIInput {

    /**
     * 合同订单id
     */
    private Long dataId;

    public Long getDataId() {
        return dataId;
    }

    public void setDataId(Long dataId) {
        this.dataId = dataId;
    }
}
