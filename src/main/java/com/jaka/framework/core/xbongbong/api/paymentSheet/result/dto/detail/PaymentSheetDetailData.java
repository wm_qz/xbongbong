package com.jaka.framework.core.xbongbong.api.paymentSheet.result.dto.detail;

import java.util.List;

/**
 * @author ：james.liu
 * @date ：Created in 2022/11/21 14:48
 * @description：
 * @version: 1.0
 */
public class PaymentSheetDetailData {

    private String text_1;

    private PaymentSheetText_14 text_14;

    private PaymentSheetCreatorId creatorId;

    private List<String> coUserId;

    private PaymentSheetText_10 text_10;

    private List<PaymentSheetOwnerId> ownerId;

    private String serialNo;

    private int date_1;

    private List<PaymentSheetArray_1> array_1;

    private PaymentSheetText_3 text_3;

    private List<AmountDetail> amountDetail;

    private int num_7;

    private List<PaymentSheetText_5> text_5;

    private List<PaymentSheetText_4> text_4;

    private int num_1;

    private PaymentSheetText_6 text_6;

    public String getText_1() {
        return text_1;
    }

    public void setText_1(String text_1) {
        this.text_1 = text_1;
    }

    public PaymentSheetText_14 getText_14() {
        return text_14;
    }

    public void setText_14(PaymentSheetText_14 text_14) {
        this.text_14 = text_14;
    }

    public PaymentSheetCreatorId getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(PaymentSheetCreatorId creatorId) {
        this.creatorId = creatorId;
    }

    public List<String> getCoUserId() {
        return coUserId;
    }

    public void setCoUserId(List<String> coUserId) {
        this.coUserId = coUserId;
    }

    public PaymentSheetText_10 getText_10() {
        return text_10;
    }

    public void setText_10(PaymentSheetText_10 text_10) {
        this.text_10 = text_10;
    }

    public List<PaymentSheetOwnerId> getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(List<PaymentSheetOwnerId> ownerId) {
        this.ownerId = ownerId;
    }

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public int getDate_1() {
        return date_1;
    }

    public void setDate_1(int date_1) {
        this.date_1 = date_1;
    }

    public List<PaymentSheetArray_1> getArray_1() {
        return array_1;
    }

    public void setArray_1(List<PaymentSheetArray_1> array_1) {
        this.array_1 = array_1;
    }

    public PaymentSheetText_3 getText_3() {
        return text_3;
    }

    public void setText_3(PaymentSheetText_3 text_3) {
        this.text_3 = text_3;
    }

    public List<AmountDetail> getAmountDetail() {
        return amountDetail;
    }

    public void setAmountDetail(List<AmountDetail> amountDetail) {
        this.amountDetail = amountDetail;
    }

    public int getNum_7() {
        return num_7;
    }

    public void setNum_7(int num_7) {
        this.num_7 = num_7;
    }

    public List<PaymentSheetText_5> getText_5() {
        return text_5;
    }

    public void setText_5(List<PaymentSheetText_5> text_5) {
        this.text_5 = text_5;
    }

    public List<PaymentSheetText_4> getText_4() {
        return text_4;
    }

    public void setText_4(List<PaymentSheetText_4> text_4) {
        this.text_4 = text_4;
    }

    public int getNum_1() {
        return num_1;
    }

    public void setNum_1(int num_1) {
        this.num_1 = num_1;
    }

    public PaymentSheetText_6 getText_6() {
        return text_6;
    }

    public void setText_6(PaymentSheetText_6 text_6) {
        this.text_6 = text_6;
    }
}
