package com.jaka.framework.core.xbongbong.api.crm.contract.result.model.detail.dto;

/**
 * <pre>
 *  Result
 * </pre>
 * @author toolscat.com
 * @verison $Id: Result v 0.1 2022-11-15 11:01:04
 */
public class Result{

    /**
     * <pre>
     * 
     * </pre>
     */
    private Integer	addTime;

    /**
     * <pre>
     * data
     * </pre>
     */
    private Data	data;

    /**
     * <pre>
     * 
     * </pre>
     */
    private Integer	dataId;

    /**
     * <pre>
     * 
     * </pre>
     */
    private Integer	formId;

    /**
     * <pre>
     * 
     * </pre>
     */
    private Integer	updateTime;


    public Integer getAddTime() {
      return this.addTime;
    }

    public void setAddTime(Integer addTime) {
      this.addTime = addTime;
    }

    public Data getData() {
      return this.data;
    }

    public void setData(Data data) {
      this.data = data;
    }

    public Integer getDataId() {
      return this.dataId;
    }

    public void setDataId(Integer dataId) {
      this.dataId = dataId;
    }

    public Integer getFormId() {
      return this.formId;
    }

    public void setFormId(Integer formId) {
      this.formId = formId;
    }

    public Integer getUpdateTime() {
      return this.updateTime;
    }

    public void setUpdateTime(Integer updateTime) {
      this.updateTime = updateTime;
    }

}
