package com.jaka.framework.core.xbongbong.api.callback.center.customerCommunicate;

/**
 * @author ：james.liu
 * @date ：Created in 2022/9/30 14:05
 * @description：归档跟进记录
 * @version: 1.0
 */
public interface CustomerCommunicateArchivedCallBack {

    /**
     * 添加用户回调
     */
    default void callback() {

    }
}
