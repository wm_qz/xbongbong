package com.jaka.framework.core.xbongbong.api.system.from.input;

import com.jaka.framework.core.xbongbong.base.XBBAbstractAPIInput;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/22 17:04
 * @description：
 * @version: 1.0
 */
public class XBBApiFormGetInput extends XBBAbstractAPIInput {

    /**
     * 表单模板id
     */
    private Long formId;
    /**
     * 当formId对应的表单为回款单表单时，标识表单业务子类型，
     * 默认值为702,
     * 核销回款单：702，
     * 红冲回款单：703，
     * 坏账回款单：704
     */
    private Integer subBusinessType;

    public Long getFormId() {
        return formId;
    }

    public void setFormId(Long formId) {
        this.formId = formId;
    }

    public Integer getSubBusinessType() {
        return subBusinessType;
    }

    public void setSubBusinessType(Integer subBusinessType) {
        this.subBusinessType = subBusinessType;
    }
}
