/**
  * Copyright 2022 bejson.com 
  */
package com.jaka.framework.core.xbongbong.api.contractOutstock.result.dto.list;

/**
 * Auto-generated: 2022-12-05 9:52:43
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class ContractOutStockListAddress_2 {

    private String address;
    private String city;
    private String district;
    private ContractOutStockListLocation location;
    private String province;
    public void setAddress(String address) {
         this.address = address;
     }
     public String getAddress() {
         return address;
     }

    public void setCity(String city) {
         this.city = city;
     }
     public String getCity() {
         return city;
     }

    public void setDistrict(String district) {
         this.district = district;
     }
     public String getDistrict() {
         return district;
     }

    public ContractOutStockListLocation getLocation() {
        return location;
    }

    public void setLocation(ContractOutStockListLocation location) {
        this.location = location;
    }

    public void setProvince(String province) {
         this.province = province;
     }
     public String getProvince() {
         return province;
     }

}