package com.jaka.framework.core.xbongbong.api.crm.customer.result.dto.list;

/**
 * @author ：james.liu
 * @date ：Created in 2022/12/19 9:41
 * @description：
 * @version: 1.0
 */
public class XBBApiCustomerListText_81 {

    private String name;

    private int id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
