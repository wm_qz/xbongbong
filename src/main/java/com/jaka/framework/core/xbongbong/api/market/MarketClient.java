package com.jaka.framework.core.xbongbong.api.market;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jaka.framework.common.core.base.IHttpUtils;
import com.jaka.framework.core.xbongbong.api.market.input.ProV2ApiClueListInput;
import com.jaka.framework.core.xbongbong.api.market.input.XBBApiMarketActivityDetailInput;
import com.jaka.framework.core.xbongbong.api.market.input.XBBApiMarketActivityListInput;
import com.jaka.framework.core.xbongbong.api.market.result.ProV2ApiClueListResult;
import com.jaka.framework.core.xbongbong.api.market.result.XBBApiMarketActivityDetailResult;
import com.jaka.framework.core.xbongbong.api.market.result.XBBApiMarketActivityListResult;
import com.jaka.framework.core.xbongbong.base.XBBDefaultAPIActionProxy;
import com.jaka.framework.core.xbongbong.base.XBBRequestMappering;
import com.jaka.framework.core.xbongbong.config.XBBAPIClientConfiguration;
import com.jaka.framework.core.xbongbong.util.SignUtils;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/22 17:51
 * @description：
 * @version: 1.0
 */
public class MarketClient {

    private final IHttpUtils httpUtils;
    private final XBBAPIClientConfiguration configuration;
    private final SignUtils signUtils;

    public MarketClient(final XBBAPIClientConfiguration configuration,
                        final IHttpUtils httpUtils,
                        final SignUtils signUtils) {
        this.configuration = configuration;
        this.httpUtils = httpUtils;
        this.signUtils = signUtils;
    }

    /**
     * 市场活动列表接口
     *
     * @param input
     * @return
     * @throws Exception
     */
    public XBBApiMarketActivityListResult apiMarketActivityList(final XBBApiMarketActivityListInput input) throws Exception {
        input.setCmdId(XBBRequestMappering.api_market_activity_list.getMapperName());
        input.setCorpid(configuration.getCorpId());
        input.setUserId(configuration.getUserId());
        JSONObject jsonObject = removeSignData(input.toJsonString());
        return XBBDefaultAPIActionProxy.getInstance().doAction(input,
                XBBApiMarketActivityListResult.class,
                configuration,
                httpUtils,
                signUtils,
                input.toRequestBody(jsonObject),
                jsonObject);
    }

    /**
     * 市场活动详情接口
     *
     * @param input
     * @return
     * @throws Exception
     */
    public XBBApiMarketActivityDetailResult apiMarketActivityDetail(final XBBApiMarketActivityDetailInput input) throws Exception {
        input.setCmdId(XBBRequestMappering.api_market_activity_detail.getMapperName());
        input.setCorpid(configuration.getCorpId());
        input.setUserId(configuration.getUserId());
        JSONObject jsonObject = removeSignData(input.toJsonString());
        return XBBDefaultAPIActionProxy.getInstance().doAction(input,
                XBBApiMarketActivityDetailResult.class,
                configuration,
                httpUtils,
                signUtils,
                input.toRequestBody(jsonObject),
                jsonObject);
    }

    /**
     * 线索列表接口
     *
     * @param input
     * @return
     * @throws Exception
     */
    public ProV2ApiClueListResult proV2ApiClueList(final ProV2ApiClueListInput input) throws Exception {
        input.setCmdId(XBBRequestMappering.pro_v2_api_clue_list.getMapperName());
        input.setCorpid(configuration.getCorpId());
        input.setUserId(configuration.getUserId());
        JSONObject jsonObject = removeSignData(input.toJsonString());
        return XBBDefaultAPIActionProxy.getInstance().doAction(input,
                ProV2ApiClueListResult.class,
                configuration,
                httpUtils,
                signUtils,
                input.toRequestBody(jsonObject),
                jsonObject);

    }

    public final JSONObject removeSignData(String data) {
        JSONObject signData = JSON.parseObject(data);
        if (signData.containsKey("cmdId")) {
            signData.remove("cmdId");
        }
        if (signData.containsKey("signUtils")) {
            signData.remove("signUtils");
        }
        return (signData.isEmpty()) ? null : signData;
    }

}
