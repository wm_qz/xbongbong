package com.jaka.framework.core.xbongbong.em;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/22 18:51
 * @description：表单业务类型
 * @version: 1.0
 */
public enum BusinessTypeEnums {

    contract_order(100, "合同订单"),

    return_refund(201, "退货退款"),

    sales_opportunity(202, "销售机会"),

    contact_person(301, "联系人"),

    follow_up_record(401, "跟进记录"),

    payment_collection_plan(501, "回款计划"),

    payment_receipt(701, "回款单"),

    sales_invoice(702, "销项发票"),

    supplier(901, "供应商"),

    purchase_contract(1001, "采购合同"),

    purchase_warehousing_list(1101, "采购入库单"),

    other_warehouse_receipts(1404, "其他入库单"),

    sales_outbound_order(1406, "销售出库单"),

    other_outbound_orders(1504, "其他出库单"),

    transfer_order(1506, "调拨单"),

    inventory_order(1601, "盘点单"),

    product(1701, "产品"),

    clues(2401, "线索"),

    market_activity(8000, "市场活动"),

    warehouse(8100, "仓库"),

    custom_form(1801, "自定义表单"),;

    BusinessTypeEnums(Integer businessType, String businessTypeDesc) {
        this.businessType = businessType;
        this.businessTypeDesc = businessTypeDesc;
    }

    private Integer businessType;

    private String businessTypeDesc;

    public Integer getBusinessType() {
        return businessType;
    }

    public void setBusinessType(Integer businessType) {
        this.businessType = businessType;
    }

    public String getBusinessTypeDesc() {
        return businessTypeDesc;
    }

    public void setBusinessTypeDesc(String businessTypeDesc) {
        this.businessTypeDesc = businessTypeDesc;
    }
}
