package com.jaka.framework.core.xbongbong.api.callback.annotation;

import com.jaka.framework.core.xbongbong.api.callback.center.BaseCallBack;

import java.lang.annotation.*;

/**
 * @author ：james.liu
 * @date ：Created in 2022/10/8 17:37
 * @description：
 * @version: 1.0
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface CallBackLog {

    /**
     * 回调
     *
     * @return
     */
    Class<? extends BaseCallBack> callback() default BaseCallBack.class;

}
