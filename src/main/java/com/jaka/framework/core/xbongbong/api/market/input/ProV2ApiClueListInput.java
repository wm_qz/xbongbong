package com.jaka.framework.core.xbongbong.api.market.input;

import com.jaka.framework.core.xbongbong.base.XBBAbstractAPIPageInput;

import java.util.List;

/**
 * @author ：james.liu
 * @date ：Created in 2022/1/12 18:34
 * @description：线索列表接口
 * @version: 1.0
 */
public class ProV2ApiClueListInput extends XBBAbstractAPIPageInput {

    /**
     * 0:线索列表 1:回收站数据
     */
    private Integer del;
    /**
     * 是否公海线索
     */
    private Integer isPublic;
    /**
     * 表单id
     */
    private Long formId;
    /**
     * 条件集合
     */
    private List<Conditions> conditions;

    public Integer getDel() {
        return del;
    }

    public void setDel(Integer del) {
        this.del = del;
    }

    public Integer getIsPublic() {
        return isPublic;
    }

    public void setIsPublic(Integer isPublic) {
        this.isPublic = isPublic;
    }

    public Long getFormId() {
        return formId;
    }

    public void setFormId(Long formId) {
        this.formId = formId;
    }

    public List<Conditions> getConditions() {
        return conditions;
    }

    public void setConditions(List<Conditions> conditions) {
        this.conditions = conditions;
    }

    public static class Conditions {

        private String attr;

        private String symbol;

        private List<String> value;

        public String getAttr() {
            return attr;
        }

        public void setAttr(String attr) {
            this.attr = attr;
        }

        public String getSymbol() {
            return symbol;
        }

        public void setSymbol(String symbol) {
            this.symbol = symbol;
        }

        public List<String> getValue() {
            return value;
        }

        public void setValue(List<String> value) {
            this.value = value;
        }
    }

}
