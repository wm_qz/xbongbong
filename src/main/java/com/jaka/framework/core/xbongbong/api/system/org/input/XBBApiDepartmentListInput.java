package com.jaka.framework.core.xbongbong.api.system.org.input;

import com.jaka.framework.core.xbongbong.base.XBBAbstractAPIPageInput;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/22 18:27
 * @description：部门列表接口入参
 * @version: 1.0
 */
public class XBBApiDepartmentListInput extends XBBAbstractAPIPageInput {

    /**
     * 部门名模糊查询
     */
    private String nameLike;

    public String getNameLike() {
        return nameLike;
    }

    public void setNameLike(String nameLike) {
        this.nameLike = nameLike;
    }
}
