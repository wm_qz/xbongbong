package com.jaka.framework.core.xbongbong.api.callback.center.customer;

/**
 * @author ：james.liu
 * @date ：Created in 2022/9/30 14:05
 * @description：客户新建回调
 * @version: 1.0
 */
public interface CustomerNewCallBack {

    default void callback() {

    }
}