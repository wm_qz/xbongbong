package com.jaka.framework.core.xbongbong;

import com.jaka.framework.core.xbongbong.api.XBBAPIClient;
import com.jaka.framework.core.xbongbong.config.SetXBBAPIClientConfiguration;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/22 14:31
 * @description：API客户端构造器
 * @version: 1.0
 */
public final class XBBAPIClientBuilder extends SetXBBAPIClientConfiguration {

    private static final XBBAPIClientBuilder instance = new XBBAPIClientBuilder();


    @Override
    public XBBAPIClient build() {
        return super.build();
    }

    /**
     * @return
     */
    public static XBBAPIClientBuilder getInstance() {
        return instance;
    }

}
