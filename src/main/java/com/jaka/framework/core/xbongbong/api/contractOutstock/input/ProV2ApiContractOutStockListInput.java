package com.jaka.framework.core.xbongbong.api.contractOutstock.input;

import com.jaka.framework.core.xbongbong.api.contractOutstock.input.dto.save.ProV2ApiContractOutStockAddDataList;
import com.jaka.framework.core.xbongbong.api.crm.contract.input.XBBApiContractListInput;
import com.jaka.framework.core.xbongbong.base.XBBAbstractAPIInput;

import java.util.List;

/**
 * @author ：james.liu
 * @date ：Created in 2022/12/2 14:00
 * @description：
 * @version: 1.0
 */
public class ProV2ApiContractOutStockListInput extends XBBAbstractAPIInput  {

    /**
     * 条件集合
     */
    private List<ConditionsList> conditions;

    public List<ConditionsList> getConditions() {
        return conditions;
    }

    public void setConditions(List<ConditionsList> conditions) {
        this.conditions = conditions;
    }

    public static class ConditionsList {

        private String attr;

        private String symbol;

        private List<String> value;

        public String getAttr() {
            return attr;
        }

        public void setAttr(String attr) {
            this.attr = attr;
        }

        public String getSymbol() {
            return symbol;
        }

        public void setSymbol(String symbol) {
            this.symbol = symbol;
        }

        public List<String> getValue() {
            return value;
        }

        public void setValue(List<String> value) {
            this.value = value;
        }
    }

}
