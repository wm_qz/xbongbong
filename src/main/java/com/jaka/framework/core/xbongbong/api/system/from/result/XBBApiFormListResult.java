package com.jaka.framework.core.xbongbong.api.system.from.result;

import com.jaka.framework.core.xbongbong.base.XBBAbstractAPIResult;

import java.util.List;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/22 18:41
 * @description：
 * @version: 1.0
 */
public class XBBApiFormListResult extends XBBAbstractAPIResult {

    /**
     * 返回信息
     */
    private ApiForm result;

    public ApiForm getResult() {
        return result;
    }

    public void setResult(ApiForm result) {
        this.result = result;
    }

    private static class ApiForm {

        private List<ApiFormList> formList;

        public List<ApiFormList> getFormList() {
            return formList;
        }

        public void setFormList(List<ApiFormList> formList) {
            this.formList = formList;
        }
    }

    public static class ApiFormList {
        /**
         * 应用id
         */
        private Long appId;
        /**
         * 表单业务类型
         */
        private Integer businessType;
        /**
         * 是否流程表单
         */
        private Integer isProcessForm;
        /**
         * 菜单id
         */
        private Long menuId;
        /**
         * 名称
         */
        private String name;
        /**
         * 表单id
         */
        private Long formId;

        public Long getAppId() {
            return appId;
        }

        public void setAppId(Long appId) {
            this.appId = appId;
        }

        public Integer getBusinessType() {
            return businessType;
        }

        public void setBusinessType(Integer businessType) {
            this.businessType = businessType;
        }

        public Integer getIsProcessForm() {
            return isProcessForm;
        }

        public void setIsProcessForm(Integer isProcessForm) {
            this.isProcessForm = isProcessForm;
        }

        public Long getMenuId() {
            return menuId;
        }

        public void setMenuId(Long menuId) {
            this.menuId = menuId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Long getFormId() {
            return formId;
        }

        public void setFormId(Long formId) {
            this.formId = formId;
        }
    }
}
