package com.jaka.framework.core.xbongbong.api.system.from.result.detail;

/**
 * @author ：james.liu
 * @date ：Created in 2022/11/30 9:36
 * @description：
 * @version: 1.0
 */
public class SubFieldList {

    private Integer accuracy;

    private String attr;

    private String attrName;

    private String dateType;

    private Integer fieldType;

    private Integer noRepeat;

    private Integer required;

    private Integer showType;

    private String verifyRule;

    private Integer numericalLimitsFlag;


    public Integer getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(Integer accuracy) {
        this.accuracy = accuracy;
    }

    public Integer getNumericalLimitsFlag() {
        return numericalLimitsFlag;
    }

    public void setNumericalLimitsFlag(Integer numericalLimitsFlag) {
        this.numericalLimitsFlag = numericalLimitsFlag;
    }

    public String getAttr() {
        return attr;
    }

    public void setAttr(String attr) {
        this.attr = attr;
    }

    public String getAttrName() {
        return attrName;
    }

    public void setAttrName(String attrName) {
        this.attrName = attrName;
    }

    public String getDateType() {
        return dateType;
    }

    public void setDateType(String dateType) {
        this.dateType = dateType;
    }

    public Integer getFieldType() {
        return fieldType;
    }

    public void setFieldType(Integer fieldType) {
        this.fieldType = fieldType;
    }

    public Integer getNoRepeat() {
        return noRepeat;
    }

    public void setNoRepeat(Integer noRepeat) {
        this.noRepeat = noRepeat;
    }

    public Integer getRequired() {
        return required;
    }

    public void setRequired(Integer required) {
        this.required = required;
    }

    public Integer getShowType() {
        return showType;
    }

    public void setShowType(Integer showType) {
        this.showType = showType;
    }

    public String getVerifyRule() {
        return verifyRule;
    }

    public void setVerifyRule(String verifyRule) {
        this.verifyRule = verifyRule;
    }
}
