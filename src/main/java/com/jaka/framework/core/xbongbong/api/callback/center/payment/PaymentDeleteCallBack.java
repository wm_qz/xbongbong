package com.jaka.framework.core.xbongbong.api.callback.center.payment;

/**
 * @author ：james.liu
 * @date ：Created in 2022/9/30 14:09
 * @description：合同订单删除
 * @version: 1.0
 */
public interface PaymentDeleteCallBack {

    /**
     * 添加用户回调
     */
    default void callback() {

    }
}
