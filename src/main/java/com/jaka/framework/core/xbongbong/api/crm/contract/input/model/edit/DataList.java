package com.jaka.framework.core.xbongbong.api.crm.contract.input.model.edit;

import java.util.List;

public class DataList {
    private List<Array_4> array_4;

    private int date_1;

    private int date_2;

    private int num_1;

    private int num_26;

    private int num_27;

    private int num_28;

    private int num_38;

    private String serialNo;

    private String text_1;

    private String text_10;

    private String text_11;

    private String text_12;

    private int text_2;

    private int text_3;

    private int text_4;

    private String text_6;

    private int text_66;

    private String text_7;

    private String text_8;

    private String text_9;

    private String text_24;

    public void setArray_4(List<Array_4> array_4) {
        this.array_4 = array_4;
    }

    public List<Array_4> getArray_4() {
        return this.array_4;
    }

    public void setDate_1(int date_1) {
        this.date_1 = date_1;
    }

    public int getDate_1() {
        return this.date_1;
    }

    public void setDate_2(int date_2) {
        this.date_2 = date_2;
    }

    public int getDate_2() {
        return this.date_2;
    }

    public void setNum_1(int num_1) {
        this.num_1 = num_1;
    }

    public int getNum_1() {
        return this.num_1;
    }

    public void setNum_26(int num_26) {
        this.num_26 = num_26;
    }

    public int getNum_26() {
        return this.num_26;
    }

    public void setNum_27(int num_27) {
        this.num_27 = num_27;
    }

    public int getNum_27() {
        return this.num_27;
    }

    public void setNum_28(int num_28) {
        this.num_28 = num_28;
    }

    public int getNum_28() {
        return this.num_28;
    }

    public void setNum_38(int num_38) {
        this.num_38 = num_38;
    }

    public int getNum_38() {
        return this.num_38;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public String getSerialNo() {
        return this.serialNo;
    }

    public void setText_1(String text_1) {
        this.text_1 = text_1;
    }

    public String getText_1() {
        return this.text_1;
    }

    public void setText_10(String text_10) {
        this.text_10 = text_10;
    }

    public String getText_10() {
        return this.text_10;
    }

    public void setText_11(String text_11) {
        this.text_11 = text_11;
    }

    public String getText_11() {
        return this.text_11;
    }

    public void setText_12(String text_12) {
        this.text_12 = text_12;
    }

    public String getText_12() {
        return this.text_12;
    }

    public void setText_2(int text_2) {
        this.text_2 = text_2;
    }

    public int getText_2() {
        return this.text_2;
    }

    public void setText_3(int text_3) {
        this.text_3 = text_3;
    }

    public int getText_3() {
        return this.text_3;
    }

    public void setText_4(int text_4) {
        this.text_4 = text_4;
    }

    public int getText_4() {
        return this.text_4;
    }

    public void setText_6(String text_6) {
        this.text_6 = text_6;
    }

    public String getText_6() {
        return this.text_6;
    }

    public void setText_66(int text_66) {
        this.text_66 = text_66;
    }

    public int getText_66() {
        return this.text_66;
    }

    public void setText_7(String text_7) {
        this.text_7 = text_7;
    }

    public String getText_7() {
        return this.text_7;
    }

    public void setText_8(String text_8) {
        this.text_8 = text_8;
    }

    public String getText_8() {
        return this.text_8;
    }

    public void setText_9(String text_9) {
        this.text_9 = text_9;
    }

    public String getText_9() {
        return this.text_9;
    }

    public String getText_24() {
        return text_24;
    }

    public void setText_24(String text_24) {
        this.text_24 = text_24;
    }
}