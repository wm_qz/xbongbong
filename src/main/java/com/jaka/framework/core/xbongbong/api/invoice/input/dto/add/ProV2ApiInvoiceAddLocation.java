package com.jaka.framework.core.xbongbong.api.invoice.input.dto.add;

/**
 * @author ：james.liu
 * @date ：Created in 2022/12/13 8:54
 * @description：
 * @version: 1.0
 */
public class ProV2ApiInvoiceAddLocation {

    private double lon;

    private double lat;

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }
}
