package com.jaka.framework.core.xbongbong.api.contractOutstock.input.dto.save;

/**
 * @author ：james.liu
 * @date ：Created in 2022/12/2 14:07
 * @description：
 * @version: 1.0
 */
public class ProV2ApiContractOutStockSubForm_1 {
    /**
     * 类型
     */
    private String text_1;
    /**
     * 电话
     */
    private String text_2;

    public String getText_1() {
        return text_1;
    }

    public void setText_1(String text_1) {
        this.text_1 = text_1;
    }

    public String getText_2() {
        return text_2;
    }

    public void setText_2(String text_2) {
        this.text_2 = text_2;
    }
}
