package com.jaka.framework.core.xbongbong.api.system.from.result;

import com.jaka.framework.core.xbongbong.api.system.from.result.detail.ExplainList;
import com.jaka.framework.core.xbongbong.base.XBBAbstractAPIResult;

import java.util.List;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/22 17:05
 * @description：
 * @version: 1.0
 */
public class XBBApiFormGetResult extends XBBAbstractAPIResult {

    /**
     * 返回信息
     */
    private Explain result;

    public Explain getResult() {
        return result;
    }

    public void setResult(Explain result) {
        this.result = result;
    }

    /**
     * 字段解释列表
     */
    public static class Explain {

        private List<ExplainList> explainList;

        public List<ExplainList> getExplainList() {
            return explainList;
        }

        public void setExplainList(List<ExplainList> explainList) {
            this.explainList = explainList;
        }
    }


}
