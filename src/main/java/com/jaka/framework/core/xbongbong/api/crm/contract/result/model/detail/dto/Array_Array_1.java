package com.jaka.framework.core.xbongbong.api.crm.contract.result.model.detail.dto;

/**
 * @author ：james.liu
 * @date ：Created in 2022/11/30 11:27
 * @description：
 * @version: 1.0
 */
public class Array_Array_1 {

    private String text;

    private String value;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
