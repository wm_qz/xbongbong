package com.jaka.framework.core.xbongbong.api.crm.contract.result.model.detail.dto;

import java.util.List;

/**
 * <pre>
 *  Array_4
 * </pre>
 *
 * @author toolscat.com
 * @verison $Id: Array_4 v 0.1 2022-11-15 11:01:04
 */
public class Array_4 {

    /**
     * <pre>
     *  单价
     * </pre>
     */
    private Integer num_1;

    /**
     * <pre>
     * 库存数量
     * </pre>
     */
    private Integer num_12;

    /**
     * <pre>
     * 出库数量
     * </pre>
     */
    private Integer num_13;

    /**
     * <pre>
     * 未出库数量
     * </pre>
     */
    private Integer num_15;

    /**
     * <pre>
     * 退货数量
     * </pre>
     */
    private Integer num_16;

    /**
     * <pre>
     * 数量
     * </pre>
     */
    private Integer num_3;

    /**
     * <pre>
     * 折扣（%）
     * </pre>
     */
    private Double num_4;

    /**
     * <pre>
     * 售价小计
     * </pre>
     */
    private Integer num_5;

    /**
     * <pre>
     * 售价（元）
     * </pre>
     */
    private Integer num_6;

    /**
     * <pre>
     *
     * </pre>
     */
    private Integer productSubId;

    /**
     * <pre>
     *
     * </pre>
     */
    private Integer refProductId;

    /**
     * <pre>
     *
     * </pre>
     */
    private Integer text_1;

    /**
     * <pre>
     * 价格类型
     * </pre>
     */
    private String text_2;

    /**
     * <pre>
     * 备注
     * </pre>
     */
    private String text_3;

    /**
     * <pre>
     *
     * </pre>
     */
    private String text_4;

    /**
     * <pre>
     * 单位
     * </pre>
     */
    private Array_Text_8 text_8;

    /**
     * 机器人本体
     */
    private Array_Text_5 text_5;
    /**
     * 机器人本体数量
     */
    private Integer num_2;
    /**
     * 控制柜
     */
    private Array_Text_6 text_6;
    /**
     * 控制柜数量
     */
    private Integer num_7;
    /**
     * 选配件
     */
    private List<Array_Array_1> array_1;
    /**
     * 选配件数量
     */
    private Integer num_8;
    /**
     * 其他选配件
     */
    private List<Array_Array_2> array_2;
    /**
     * 其他选配件数量
     */
    private Integer num_9;
    /**
     * 已采购数量
     */
    private Integer num_18;
    /**
     * 状态
     */
    private Integer text_9;


    public Integer getNum_1() {
        return this.num_1;
    }

    public void setNum_1(Integer num_1) {
        this.num_1 = num_1;
    }

    public Integer getNum_12() {
        return this.num_12;
    }

    public void setNum_12(Integer num_12) {
        this.num_12 = num_12;
    }

    public Integer getNum_13() {
        return this.num_13;
    }

    public void setNum_13(Integer num_13) {
        this.num_13 = num_13;
    }

    public Integer getNum_15() {
        return this.num_15;
    }

    public void setNum_15(Integer num_15) {
        this.num_15 = num_15;
    }

    public Integer getNum_16() {
        return this.num_16;
    }

    public void setNum_16(Integer num_16) {
        this.num_16 = num_16;
    }

    public Integer getNum_3() {
        return this.num_3;
    }

    public void setNum_3(Integer num_3) {
        this.num_3 = num_3;
    }

    public Double getNum_4() {
        return this.num_4;
    }

    public void setNum_4(Double num_4) {
        this.num_4 = num_4;
    }

    public Integer getNum_5() {
        return this.num_5;
    }

    public void setNum_5(Integer num_5) {
        this.num_5 = num_5;
    }

    public Integer getNum_6() {
        return this.num_6;
    }

    public void setNum_6(Integer num_6) {
        this.num_6 = num_6;
    }

    public Integer getProductSubId() {
        return this.productSubId;
    }

    public void setProductSubId(Integer productSubId) {
        this.productSubId = productSubId;
    }

    public Integer getRefProductId() {
        return this.refProductId;
    }

    public void setRefProductId(Integer refProductId) {
        this.refProductId = refProductId;
    }

    public Integer getText_1() {
        return this.text_1;
    }

    public void setText_1(Integer text_1) {
        this.text_1 = text_1;
    }

    public String getText_2() {
        return this.text_2;
    }

    public void setText_2(String text_2) {
        this.text_2 = text_2;
    }

    public String getText_3() {
        return this.text_3;
    }

    public void setText_3(String text_3) {
        this.text_3 = text_3;
    }

    public String getText_4() {
        return this.text_4;
    }

    public void setText_4(String text_4) {
        this.text_4 = text_4;
    }

    public Array_Text_8 getText_8() {
        return text_8;
    }

    public void setText_8(Array_Text_8 text_8) {
        this.text_8 = text_8;
    }

    public Array_Text_5 getText_5() {
        return text_5;
    }

    public void setText_5(Array_Text_5 text_5) {
        this.text_5 = text_5;
    }

    public Integer getNum_2() {
        return num_2;
    }

    public void setNum_2(Integer num_2) {
        this.num_2 = num_2;
    }

    public Array_Text_6 getText_6() {
        return text_6;
    }

    public void setText_6(Array_Text_6 text_6) {
        this.text_6 = text_6;
    }

    public Integer getNum_7() {
        return num_7;
    }

    public void setNum_7(Integer num_7) {
        this.num_7 = num_7;
    }

    public List<Array_Array_1> getArray_1() {
        return array_1;
    }

    public void setArray_1(List<Array_Array_1> array_1) {
        this.array_1 = array_1;
    }

    public Integer getNum_8() {
        return num_8;
    }

    public void setNum_8(Integer num_8) {
        this.num_8 = num_8;
    }

    public List<Array_Array_2> getArray_2() {
        return array_2;
    }

    public void setArray_2(List<Array_Array_2> array_2) {
        this.array_2 = array_2;
    }

    public Integer getNum_9() {
        return num_9;
    }

    public void setNum_9(Integer num_9) {
        this.num_9 = num_9;
    }

    public Integer getNum_18() {
        return num_18;
    }

    public void setNum_18(Integer num_18) {
        this.num_18 = num_18;
    }

    public Integer getText_9() {
        return text_9;
    }

    public void setText_9(Integer text_9) {
        this.text_9 = text_9;
    }
}
