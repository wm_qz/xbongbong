package com.jaka.framework.core.xbongbong.api.crm.contract.result.model.detail.dto;

/**
 * <pre>
 *  Text_6
 * </pre>
 * @author toolscat.com
 * @verison $Id: Text_6 v 0.1 2022-11-15 11:01:04
 */
public class Text_6{

    /**
     * <pre>
     * 签约
     * </pre>
     */
    private String	text;

    /**
     * <pre>
     * 
     * </pre>
     */
    private String	value;


    public String getText() {
      return this.text;
    }

    public void setText(String text) {
      this.text = text;
    }

    public String getValue() {
      return this.value;
    }

    public void setValue(String value) {
      this.value = value;
    }

}
