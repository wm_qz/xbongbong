package com.jaka.framework.core.xbongbong.api.crm.contract.result.model.detail;

import com.jaka.framework.core.xbongbong.api.crm.contract.result.model.detail.dto.Data;

/**
 * @author ：james.liu
 * @date ：Created in 2022/11/15 11:13
 * @description：
 * @version: 1.0
 */
public class ContractDetailResult {

    /**
     * <pre>
     *
     * </pre>
     */
    private Integer	addTime;
    /**
     * <pre>
     * data
     * </pre>
     */
    private Data data;

    /**
     * <pre>
     *
     * </pre>
     */
    private Integer	dataId;

    /**
     * <pre>
     *
     * </pre>
     */
    private Integer	formId;

    /**
     * <pre>
     *
     * </pre>
     */
    private Integer	updateTime;

    public Integer getAddTime() {
        return addTime;
    }

    public void setAddTime(Integer addTime) {
        this.addTime = addTime;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public Integer getDataId() {
        return dataId;
    }

    public void setDataId(Integer dataId) {
        this.dataId = dataId;
    }

    public Integer getFormId() {
        return formId;
    }

    public void setFormId(Integer formId) {
        this.formId = formId;
    }

    public Integer getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Integer updateTime) {
        this.updateTime = updateTime;
    }
}
