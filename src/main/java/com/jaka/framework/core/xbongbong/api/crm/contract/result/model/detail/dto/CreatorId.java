package com.jaka.framework.core.xbongbong.api.crm.contract.result.model.detail.dto;

/**
 * <pre>
 *  CreatorId
 * </pre>
 * @author toolscat.com
 * @verison $Id: CreatorId v 0.1 2022-11-15 11:01:04
 */
public class CreatorId{

    /**
     * <pre>
     * 
     * </pre>
     */
    private String	id;

    /**
     * <pre>
     * 张楠楠
     * </pre>
     */
    private String	name;


    public String getId() {
      return this.id;
    }

    public void setId(String id) {
      this.id = id;
    }

    public String getName() {
      return this.name;
    }

    public void setName(String name) {
      this.name = name;
    }

}
