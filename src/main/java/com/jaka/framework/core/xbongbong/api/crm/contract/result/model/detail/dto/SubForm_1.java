package com.jaka.framework.core.xbongbong.api.crm.contract.result.model.detail.dto;

/**
 * <pre>
 *  SubForm_1
 * </pre>
 *
 * @author toolscat.com
 * @verison $Id: SubForm_1 v 0.1 2022-11-15 11:01:04
 */
public class SubForm_1 {

    /**
     * <pre>
     *
     * </pre>
     */
    private Integer num_2;

    /**
     * <pre>
     *
     * </pre>
     */
    private Integer num_3;

    /**
     * <pre>
     *
     * </pre>
     */
    private Integer subDataId;

    /**
     * <pre>
     * text_1
     * </pre>
     */
    private SubFormText_1 text_1;

    /**
     * <pre>
     * text_2
     * </pre>
     */
    private SubFormText_2 text_2;

    /**
     * <pre>
     * text_3
     * </pre>
     */
    private SubFormText_3 text_3;

    public Integer getNum_2() {
        return num_2;
    }

    public void setNum_2(Integer num_2) {
        this.num_2 = num_2;
    }

    public Integer getNum_3() {
        return num_3;
    }

    public void setNum_3(Integer num_3) {
        this.num_3 = num_3;
    }

    public Integer getSubDataId() {
        return subDataId;
    }

    public void setSubDataId(Integer subDataId) {
        this.subDataId = subDataId;
    }

    public SubFormText_1 getText_1() {
        return text_1;
    }

    public void setText_1(SubFormText_1 text_1) {
        this.text_1 = text_1;
    }

    public SubFormText_2 getText_2() {
        return text_2;
    }

    public void setText_2(SubFormText_2 text_2) {
        this.text_2 = text_2;
    }

    public SubFormText_3 getText_3() {
        return text_3;
    }

    public void setText_3(SubFormText_3 text_3) {
        this.text_3 = text_3;
    }
}
