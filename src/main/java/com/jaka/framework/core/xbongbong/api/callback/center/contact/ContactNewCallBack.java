package com.jaka.framework.core.xbongbong.api.callback.center.contact;

/**
 * @author ：james.liu
 * @date ：Created in 2022/9/30 14:09
 * @description：合同订单添加
 * @version: 1.0
 */
public interface ContactNewCallBack {

    default void callback() {
        System.out.println("-----------默认回调，不做任何处理------------");
    }
}