package com.jaka.framework.core.xbongbong.config;

import com.jaka.framework.core.xbongbong.base.XBBRequestMappering;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/21 11:43
 * @description：API接口参数映射
 * @version: 1.0
 */
public class XBBAPIInterfaceMapping {

    /**
     * 描述：输入接口 参数映射
     */
    private static final Map<String, Object> inputMapping = new ConcurrentHashMap<>();

    static {
        //系统模块 start
        registerAPIMapping(XBBRequestMappering.api_user_list.getMapperName(),
                "corpid");
        registerAPIMapping(XBBRequestMappering.api_department_list.getMapperName(),
                "corpid");
        registerAPIMapping(XBBRequestMappering.api_form_get.getMapperName(),
                "corpid,formId");
        registerAPIMapping(XBBRequestMappering.api_form_list.getMapperName(),
                "corpid");
        //系统模块 end

        //CRM模块 start
        //// 客户模块 start
        registerAPIMapping(XBBRequestMappering.api_customer_list.getMapperName(),
                "formId,corpid");
        registerAPIMapping(XBBRequestMappering.api_customer_add.getMapperName(),
                "formId,corpid,dataList");
        registerAPIMapping(XBBRequestMappering.api_customer_edit.getMapperName(),
                "formId,corpid,dataList");
        registerAPIMapping(XBBRequestMappering.api_customer_detail.getMapperName(),
                "corpid,dataId");
        registerAPIMapping(XBBRequestMappering.api_customer_del.getMapperName(),
                "corpid,dataId");
        registerAPIMapping(XBBRequestMappering.api_customer_add_co_user.getMapperName(),
                "corpid,dataId,businessUserIdList");
        registerAPIMapping(XBBRequestMappering.api_customer_delete_co_user.getMapperName(),
                "corpid,dataId,businessUserIdList");
        //// 客户模块 end

        //// 联系人模块 start
        registerAPIMapping(XBBRequestMappering.pro_v2_api_contact_list.getMapperName(),
                "corpid");
        //// 联系人模块 end

        //// 销售机会 start
        registerAPIMapping(XBBRequestMappering.pro_v2_api_opportunity_list.getMapperName(),
                "corpid");
        //// 销售机会 end


        //CRM模块 end

        //市场活动模块 start
        registerAPIMapping(XBBRequestMappering.api_market_activity_list.getMapperName(),
                "formId,corpid");
        registerAPIMapping(XBBRequestMappering.api_market_activity_detail.getMapperName(),
                "dataId,corpid");
        registerAPIMapping(XBBRequestMappering.pro_v2_api_clue_list.getMapperName(),
                "corpid,formId");
        //市场活动模块 end

        // 工单 start
        registerAPIMapping(XBBRequestMappering.api_work_order_temlate_list.getMapperName(),
                "corpid");

        registerAPIMapping(XBBRequestMappering.api_work_order_temlate_detail.getMapperName(),
                "corpid");

        registerAPIMapping(XBBRequestMappering.api_work_order_add.getMapperName(),
                "corpid");

        // 工单 end


        // 回款单 start
        registerAPIMapping(XBBRequestMappering.pro_v2_api_payment_sheet_list.getMapperName(),
                "corpid");

        registerAPIMapping(XBBRequestMappering.pro_v2_api_payment_sheet_detail.getMapperName(),
                "corpid");
        // 回款单 end

        // 产品模块 start

        registerAPIMapping(XBBRequestMappering.pro_v2_api_product_list.getMapperName(),
                "corpid");

        // 产品模块 end

        // 销项发票 start
        registerAPIMapping(XBBRequestMappering.pro_v2_api_invoice_add.getMapperName(),
                "corpid");
        // 销项发票 end

    }

    /**
     * 设置接口拼接地址
     **/
    public static final String PERFIX_SPLICING = "-splicing";
    /**
     * 设置校验参数
     **/
    private static final String PREFIX_REQUIRED = "-required";

    /**
     * @param cmdId         接口名称 <br/>
     * @param inputRequired 口输入参数，必填参数列表<br/>
     */
    private static void registerAPIMapping(final String cmdId,
                                           final String inputRequired) {
        inputMapping.put(cmdId + PREFIX_REQUIRED, inputRequired);
    }

    /**
     * 描述：（接口输入值）获取API 必选参数列表
     *
     * @param cmdId
     * @return
     */
    public static List<String> getRequiredListInput(final String cmdId) {
        Object ob = inputMapping.get(cmdId + PREFIX_REQUIRED);
        if (Objects.isNull(ob)) {
            return null;
        }
        String o = inputMapping.get(cmdId + PREFIX_REQUIRED).toString();
        String[] split = StringUtils.split(o, ",");
        if (Objects.isNull(split)) {
            return null;
        }
        if (split.length <= 0) {
            return null;
        }
        return Arrays.asList(split);
    }

}
