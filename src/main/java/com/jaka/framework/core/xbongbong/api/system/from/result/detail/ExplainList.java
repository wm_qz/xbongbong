package com.jaka.framework.core.xbongbong.api.system.from.result.detail;

import java.util.List;

/**
 * @author ：james.liu
 * @date ：Created in 2022/11/30 9:30
 * @description：
 * @version: 1.0
 */
public class ExplainList {

    private int amountFlag;

    private String attr;

    private String attrName;

    private int comboType;

    private String dateType;

    private int fieldType;

    private List<Items> items;

    private int noRepeat;

    private int required;

    private SerialNumber serialNumber;

    private List<SubFieldList> subFieldList;

    private NumericalLimits numericalLimits;

    private int showType;

    public int getAmountFlag() {
        return amountFlag;
    }

    public void setAmountFlag(int amountFlag) {
        this.amountFlag = amountFlag;
    }

    public String getAttr() {
        return attr;
    }

    public void setAttr(String attr) {
        this.attr = attr;
    }

    public String getAttrName() {
        return attrName;
    }

    public void setAttrName(String attrName) {
        this.attrName = attrName;
    }

    public int getComboType() {
        return comboType;
    }

    public void setComboType(int comboType) {
        this.comboType = comboType;
    }

    public String getDateType() {
        return dateType;
    }

    public void setDateType(String dateType) {
        this.dateType = dateType;
    }

    public int getFieldType() {
        return fieldType;
    }

    public void setFieldType(int fieldType) {
        this.fieldType = fieldType;
    }

    public List<Items> getItems() {
        return items;
    }

    public void setItems(List<Items> items) {
        this.items = items;
    }

    public int getNoRepeat() {
        return noRepeat;
    }

    public void setNoRepeat(int noRepeat) {
        this.noRepeat = noRepeat;
    }

    public int getRequired() {
        return required;
    }

    public void setRequired(int required) {
        this.required = required;
    }

    public SerialNumber getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(SerialNumber serialNumber) {
        this.serialNumber = serialNumber;
    }

    public int getShowType() {
        return showType;
    }

    public void setShowType(int showType) {
        this.showType = showType;
    }

    public List<SubFieldList> getSubFieldList() {
        return subFieldList;
    }

    public void setSubFieldList(List<SubFieldList> subFieldList) {
        this.subFieldList = subFieldList;
    }

    public NumericalLimits getNumericalLimits() {
        return numericalLimits;
    }

    public void setNumericalLimits(NumericalLimits numericalLimits) {
        this.numericalLimits = numericalLimits;
    }
}
