package com.jaka.framework.core.xbongbong.api.crm.customer.result.dto.add;

/**
 * @author ：james.liu
 * @date ：Created in 2022/11/24 13:16
 * @description：
 * @version: 1.0
 */
public class CustomerSubForm_1 {

    private SubFormText_1 text_1;

    private String text_2;

    public void setText_1(SubFormText_1 text_1) {
        this.text_1 = text_1;
    }

    public SubFormText_1 getText_1() {
        return this.text_1;
    }

    public void setText_2(String text_2) {
        this.text_2 = text_2;
    }

    public String getText_2() {
        return this.text_2;
    }
}
