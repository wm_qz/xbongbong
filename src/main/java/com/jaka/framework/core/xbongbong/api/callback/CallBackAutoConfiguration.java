package com.jaka.framework.core.xbongbong.api.callback;

import com.jaka.framework.core.xbongbong.api.callback.support.CallBackContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @author ：james.liu
 * @date ：Created in 2022/10/8 17:58
 * @description：
 * @version: 1.0
 */
@Configuration
@Import({
        CallBackContext.class
})
public class CallBackAutoConfiguration {
}
