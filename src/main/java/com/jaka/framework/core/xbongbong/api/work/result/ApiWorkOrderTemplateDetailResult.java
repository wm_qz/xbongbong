package com.jaka.framework.core.xbongbong.api.work.result;

import com.jaka.framework.core.xbongbong.base.XBBAbstractAPIResult;

import java.util.List;

/**
 * @author ：james.liu
 * @date ：Created in 2022/8/17 10:28
 * @description：
 * @version: 1.0
 */
public class ApiWorkOrderTemplateDetailResult extends XBBAbstractAPIResult {

    private ApiWorkOrderTemplateDetail result;

    public ApiWorkOrderTemplateDetail getResult() {
        return result;
    }

    public void setResult(ApiWorkOrderTemplateDetail result) {
        this.result = result;
    }

    public static class ApiWorkOrderTemplateDetail {

        private List<ApiWorkOrderTemplateDetailList> explainList;

        public List<ApiWorkOrderTemplateDetailList> getExplainList() {
            return explainList;
        }

        public void setExplainList(List<ApiWorkOrderTemplateDetailList> explainList) {
            this.explainList = explainList;
        }
    }

    public static class ApiWorkOrderTemplateDetailList {

        private String attr;

        private String attrName;

        private Integer fieldType;

        private Integer noRepeat;

        private Integer required;

        private Integer patternType;

        private String verifyRule;

        private Integer integerOnly;
        private Integer accuracy;
        private Integer numericalLimitsFlag;

        private NumericalLimits numericalLimits;
        private String dateType;
        private SerialNumber serialNumber;
        private Integer multiple;
        private List<Item> items;

        public String getAttr() {
            return attr;
        }

        public void setAttr(String attr) {
            this.attr = attr;
        }

        public String getAttrName() {
            return attrName;
        }

        public void setAttrName(String attrName) {
            this.attrName = attrName;
        }

        public Integer getFieldType() {
            return fieldType;
        }

        public void setFieldType(Integer fieldType) {
            this.fieldType = fieldType;
        }

        public Integer getNoRepeat() {
            return noRepeat;
        }

        public void setNoRepeat(Integer noRepeat) {
            this.noRepeat = noRepeat;
        }

        public Integer getRequired() {
            return required;
        }

        public void setRequired(Integer required) {
            this.required = required;
        }

        public Integer getPatternType() {
            return patternType;
        }

        public void setPatternType(Integer patternType) {
            this.patternType = patternType;
        }

        public String getVerifyRule() {
            return verifyRule;
        }

        public void setVerifyRule(String verifyRule) {
            this.verifyRule = verifyRule;
        }

        public Integer getIntegerOnly() {
            return integerOnly;
        }

        public void setIntegerOnly(Integer integerOnly) {
            this.integerOnly = integerOnly;
        }

        public Integer getAccuracy() {
            return accuracy;
        }

        public void setAccuracy(Integer accuracy) {
            this.accuracy = accuracy;
        }

        public Integer getNumericalLimitsFlag() {
            return numericalLimitsFlag;
        }

        public void setNumericalLimitsFlag(Integer numericalLimitsFlag) {
            this.numericalLimitsFlag = numericalLimitsFlag;
        }

        public NumericalLimits getNumericalLimits() {
            return numericalLimits;
        }

        public void setNumericalLimits(NumericalLimits numericalLimits) {
            this.numericalLimits = numericalLimits;
        }

        public String getDateType() {
            return dateType;
        }

        public void setDateType(String dateType) {
            this.dateType = dateType;
        }

        public SerialNumber getSerialNumber() {
            return serialNumber;
        }

        public void setSerialNumber(SerialNumber serialNumber) {
            this.serialNumber = serialNumber;
        }

        public Integer getMultiple() {
            return multiple;
        }

        public void setMultiple(Integer multiple) {
            this.multiple = multiple;
        }

        public List<Item> getItems() {
            return items;
        }

        public void setItems(List<Item> items) {
            this.items = items;
        }
    }

    public static class Item {

        private boolean checked;
        private Integer isOther;
        private String text;
        private String value;

        public boolean isChecked() {
            return checked;
        }

        public void setChecked(boolean checked) {
            this.checked = checked;
        }

        public Integer getIsOther() {
            return isOther;
        }

        public void setIsOther(Integer isOther) {
            this.isOther = isOther;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
        
    }


    public static class SerialNumber {
        private String dateFormat;
        private Integer numberDigit;
        private String postfix;
        private String prefix;
        private Integer resetCycle;
        private Integer startNum;

        public String getDateFormat() {
            return dateFormat;
        }

        public void setDateFormat(String dateFormat) {
            this.dateFormat = dateFormat;
        }

        public Integer getNumberDigit() {
            return numberDigit;
        }

        public void setNumberDigit(Integer numberDigit) {
            this.numberDigit = numberDigit;
        }

        public String getPostfix() {
            return postfix;
        }

        public void setPostfix(String postfix) {
            this.postfix = postfix;
        }

        public String getPrefix() {
            return prefix;
        }

        public void setPrefix(String prefix) {
            this.prefix = prefix;
        }

        public Integer getResetCycle() {
            return resetCycle;
        }

        public void setResetCycle(Integer resetCycle) {
            this.resetCycle = resetCycle;
        }

        public Integer getStartNum() {
            return startNum;
        }

        public void setStartNum(Integer startNum) {
            this.startNum = startNum;
        }
    }

    public static class NumericalLimits {

        private Long max;

        private Long min;

        public Long getMax() {
            return max;
        }

        public void setMax(Long max) {
            this.max = max;
        }

        public Long getMin() {
            return min;
        }

        public void setMin(Long min) {
            this.min = min;
        }
    }
}
