package com.jaka.framework.core.xbongbong.api.crm.contract.result.model.list;

public class ContractList {
    private int addTime;

    private ContractListData data;

    private int dataId;

    private int formId;

    private int updateTime;

    public void setAddTime(int addTime) {
        this.addTime = addTime;
    }

    public int getAddTime() {
        return this.addTime;
    }

    public ContractListData getData() {
        return data;
    }

    public void setData(ContractListData data) {
        this.data = data;
    }

    public void setDataId(int dataId) {
        this.dataId = dataId;
    }

    public int getDataId() {
        return this.dataId;
    }

    public void setFormId(int formId) {
        this.formId = formId;
    }

    public int getFormId() {
        return this.formId;
    }

    public void setUpdateTime(int updateTime) {
        this.updateTime = updateTime;
    }

    public int getUpdateTime() {
        return this.updateTime;
    }
}