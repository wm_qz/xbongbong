package com.jaka.framework.core.xbongbong.api.callback.center.quotation;

import com.jaka.framework.core.xbongbong.api.callback.center.BaseCallBack;
import com.jaka.framework.core.xbongbong.api.callback.center.BaseCenter;
import com.jaka.framework.core.xbongbong.api.callback.center.OperateEvent;
import com.jaka.framework.core.xbongbong.api.callback.model.WebHookCallBackInput;

/**
 * @author ：james.liu
 * @date ：Created in 2022/9/30 14:46
 * @description：
 * @version: 1.0
 */
public class QuotationCenter extends OperateEvent implements BaseCenter {

    @Override
    public void center(WebHookCallBackInput webHookCallBackInput, Class<? extends BaseCallBack> callback) {
        super.center(webHookCallBackInput, callback);
    }
}
