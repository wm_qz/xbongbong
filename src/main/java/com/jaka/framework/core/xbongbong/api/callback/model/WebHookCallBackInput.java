package com.jaka.framework.core.xbongbong.api.callback.model;

/**
 * @author ：james.liu
 * @date ：Created in 2022/9/30 13:26
 * @description：销帮帮回调业务处理
 * @version: 1.0
 */
public class WebHookCallBackInput {

    /**
     * 公司id
     */
    private String corpid;
    /**
     * 业务主键id
     */
    private Long dataId;
    /**
     * 回调业务
     */
    private String type;
    /**
     * 操作类型
     */
    private String operate;
    /**
     * 1:销帮帮系统模板数据，2:自定义表单数据
     */
    private Integer saasMark;
    /**
     * 对应数据的表单模板id
     */
    private Long formId;

    public String getCorpid() {
        return corpid;
    }

    public void setCorpid(String corpid) {
        this.corpid = corpid;
    }

    public Long getDataId() {
        return dataId;
    }

    public void setDataId(Long dataId) {
        this.dataId = dataId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getOperate() {
        return operate;
    }

    public void setOperate(String operate) {
        this.operate = operate;
    }

    public Integer getSaasMark() {
        return saasMark;
    }

    public void setSaasMark(Integer saasMark) {
        this.saasMark = saasMark;
    }

    public Long getFormId() {
        return formId;
    }

    public void setFormId(Long formId) {
        this.formId = formId;
    }
}
