package com.jaka.framework.core.xbongbong.base;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/22 12:06
 * @description：销帮帮接口配置
 * @version: 1.0
 */
public enum XBBRequestMappering {

    //系统模块 start
    /**
     * 用户列表接口
     */
    api_user_list("/pro/v2/api/user/list", "用户列表接口"),
    /**
     * 部门列表接口
     */
    api_department_list("/pro/v2/api/department/list", "部门列表接口"),
    /**
     * 表单模板字段解释接口
     */
    api_form_get("/pro/v2/api/form/get", "表单模板字段解释接口"),
    /**
     * 表单模板列表接口
     */
    api_form_list("/pro/v2/api/form/list", "表单模板列表接口"),

    //系统模块 end

    //CRM模块 start

    //// 客户模块 start
    /**
     * 客户列表接口
     */
    api_customer_list("/pro/v2/api/customer/list", "客户列表接口"),
    /**
     * 新建客户接口
     */
    api_customer_add("/pro/v2/api/customer/add", "新建客户接口"),
    /**
     * 编辑客户接口
     */
    api_customer_edit("/pro/v2/api/customer/edit", "编辑客户接口"),
    /**
     * 客户详情接口
     */
    api_customer_detail("/pro/v2/api/customer/detail", "客户详情接口"),
    /**
     * 删除客户接口
     */
    api_customer_del("/pro/v2/api/customer/del", "删除客户接口"),
    /**
     * 客户添加协同人接口
     */
    api_customer_add_co_user("/pro/v2/api/customer/addCoUser", "客户添加协同人接口"),
    /**
     * 客户删除协同人接口
     */
    api_customer_delete_co_user("/pro/v2/api/customer/deleteCoUser", "客户删除协同人接口"),
    //// 客户模块 end


    //// 联系人模块 start
    pro_v2_api_contact_list("/pro/v2/api/contact/list", "联系人列表接口"),
    //// 联系人模块 end

    //// 销售机会模块 start
    pro_v2_api_opportunity_list("/pro/v2/api/opportunity/list", "销售机会列表接口"),
    //// 销售机会模块 end

    //// 跟进记录 start
    pro_v2_api_communicate_list("/pro/v2/api/communicate/list", "跟进记录列表接口"),
    //// 跟进记录 end

    //// 合同订单 start
    pro_v2_api_contract_list("/pro/v2/api/contract/list", "合同订单列表接口"),

    pro_v2_api_contract_detail("/pro/v2/api/contract/detail", "合同订单详情接口"),

    pro_v2_api_contract_edit("/pro/v2/api/contract/edit", "编辑合同订单接口"),
    //// 合同订单 end

    //CRM模块 end

    //市场活动模块 start
    /**
     * 市场活动列表接口
     */
    api_market_activity_list("/pro/v2/api/marketActivity/list", "市场活动列表接口"),
    /**
     * 市场活动详情接口
     */
    api_market_activity_detail("/pro/v2/api/marketActivity/detail", "市场活动详情接口"),

    pro_v2_api_clue_list("/pro/v2/api/clue/list", "线索列表接口"),

    //市场活动模块 end

    // 工单模块 start
    /**
     * 工单模板列表接口
     */
    api_work_order_temlate_list("/pro/v2/api/workOrder/templateList", "工单模板列表接口"),

    api_work_order_temlate_detail("/pro/v2/api/workOrder/templateDetail", "工单模板详情接口"),

    api_work_order_add("/pro/v2/api/workOrder/add", "新建工单接口"),
    // 工单模块 end


    // 回款单模块 start
    pro_v2_api_payment_sheet_detail("/pro/v2/api/paymentSheet/detail", "回款单详情接口"),

    pro_v2_api_payment_sheet_list("/pro/v2/api/paymentSheet/list", "回款单列表接口"),
    // 回款单模块 end

    // 产品模块 start
    pro_v2_api_product_list("/pro/v2/api/product/list", "产品列表接口"),

    pro_v2_api_product_detail("/pro/v2/api/product/detail", "产品详情接口"),

    // 产品模块 end

    // 销售出库单 start

    pro_v2_api_contract_out_stock_add("/pro/v2/api/contractOutstock/add", "新建销售出库单接口"),

    pro_v2_api_contract_out_stock_list("/pro/v2/api/contractOutstock/list", "销售出库单列表接口"),

    pro_v2_api_contract_out_stock_detail("/pro/v2/api/contractOutstock/detail","销售出库单详情接口"),
    // 销售出库单 end


    // 销项发票 start
    pro_v2_api_invoice_add("/pro/v2/api/invoice/add","新建销项发票"),
    // 销项发票 end

    ;

    /**
     * 配置信息
     *
     * @param mapperName 接口地址
     * @param mapperDesc 接口描述
     */
    XBBRequestMappering(String mapperName, String mapperDesc) {
        this.mapperDesc = mapperDesc;
        this.mapperName = mapperName;
    }

    /**
     * 接口地址
     */
    private String mapperName;

    /**
     * 接口描述
     */
    private String mapperDesc;

    public String getMapperName() {
        return mapperName;
    }

    public void setMapperName(String mapperName) {
        this.mapperName = mapperName;
    }

    public String getMapperDesc() {
        return mapperDesc;
    }

    public void setMapperDesc(String mapperDesc) {
        this.mapperDesc = mapperDesc;
    }

}
