package com.jaka.framework.core.xbongbong.api.callback.center;

import com.jaka.framework.core.xbongbong.api.callback.model.WebHookCallBackInput;

/**
 * @author ：james.liu
 * @date ：Created in 2022/10/8 17:32
 * @description：
 * @version: 1.0
 */
public abstract class BaseCallBack {

    /**
     * 业务主键id
     *
     * @param webHookCallBackInput 返回数据
     */
    public abstract void callback(WebHookCallBackInput webHookCallBackInput);
}
