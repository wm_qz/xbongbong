package com.jaka.framework.core.xbongbong.api.product.result.dto;

import com.jaka.framework.core.xbongbong.base.XBBAbstractAPIResult;

/**
 * @author ：james.liu
 * @date ：Created in 2022/11/29 19:30
 * @description：
 * @version: 1.0
 */
public class XBBApiProductDetailResult extends XBBAbstractAPIResult {
}
