package com.jaka.framework.core.xbongbong.api.contractOutstock.result;

import com.jaka.framework.core.xbongbong.api.contractOutstock.result.dto.detail.ProV2ApiContractOutStockDetail;
import com.jaka.framework.core.xbongbong.base.XBBAbstractAPIResult;

/**
 * @author ：james.liu
 * @date ：Created in 2022/12/5 10:23
 * @description：
 * @version: 1.0
 */
public class ProV2ApiContractOutStockDetailResult extends XBBAbstractAPIResult {

    private ProV2ApiContractOutStockDetail result;

    public ProV2ApiContractOutStockDetail getResult() {
        return result;
    }

    public void setResult(ProV2ApiContractOutStockDetail result) {
        this.result = result;
    }
}
