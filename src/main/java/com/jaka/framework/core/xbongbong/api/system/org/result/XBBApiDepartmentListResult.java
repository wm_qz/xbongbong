package com.jaka.framework.core.xbongbong.api.system.org.result;

import com.jaka.framework.core.xbongbong.base.XBBAbstractAPIResult;
import com.jaka.framework.core.xbongbong.base.XBBPageResult;

import java.util.List;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/22 18:28
 * @description：部门列表接口出参
 * @version: 1.0
 */
public class XBBApiDepartmentListResult extends XBBAbstractAPIResult {

    /**
     * 返回信息
     */
    private ApiDepartmentList result;

    public ApiDepartmentList getResult() {
        return result;
    }

    public void setResult(ApiDepartmentList result) {
        this.result = result;
    }

    public static class ApiDepartmentList extends XBBPageResult {
        /**
         * 部门列表
         */
        private List<DepartmentList> depList;

        public List<DepartmentList> getDepList() {
            return depList;
        }

        public void setDepList(List<DepartmentList> depList) {
            this.depList = depList;
        }
    }

    public static class DepartmentList {
        /**
         * 部门路由，由根部门id到当前部门id
         */
        private String depIdRouter;
        /**
         * 部门id
         */
        private Long id;
        /**
         * 部门名
         */
        private String name;
        /**
         * 上级部门id，为0时，则为根部门
         */
        private Long parentId;
        /**
         * 当前部门在父部门下的所有子部门中的排序值
         */
        private Long sort;

        public String getDepIdRouter() {
            return depIdRouter;
        }

        public void setDepIdRouter(String depIdRouter) {
            this.depIdRouter = depIdRouter;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Long getParentId() {
            return parentId;
        }

        public void setParentId(Long parentId) {
            this.parentId = parentId;
        }

        public Long getSort() {
            return sort;
        }

        public void setSort(Long sort) {
            this.sort = sort;
        }
    }
}
