package com.jaka.framework.core.xbongbong.api.crm.customer.result;

import com.jaka.framework.core.xbongbong.base.XBBAbstractAPIResult;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/23 9:21
 * @description：新建客户接口出参；新建客户接口，若没有指定负责人，则会到公海池
 * @version: 1.0
 */
public class XBBApiCustomerEditResult extends XBBAbstractAPIResult {

    /**
     * 返回信息
     */
    private ApiCustomerEdit result;

    public ApiCustomerEdit getResult() {
        return result;
    }

    public void setResult(ApiCustomerEdit result) {
        this.result = result;
    }

    public static class ApiCustomerEdit extends XBBApiCustomerAddResult.ApiCustomerAdd {

    }
}
