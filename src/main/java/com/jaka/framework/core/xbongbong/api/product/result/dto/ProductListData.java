package com.jaka.framework.core.xbongbong.api.product.result.dto;

import java.util.List;

/**
 * @author ：james.liu
 * @date ：Created in 2022/11/23 13:49
 * @description：
 * @version: 1.0
 */
public class ProductListData {

    private String creatorId;

    private List<String> text_70;

    private String text_3;

    private int text_5;

    private List<String> text_9;

    private String text_8;

    private int num_12;

    private int num_13;

    private int num_36;

    private String text_1;

    private String text_4;



    private List<String> file_1;

    private String file_2;

    private int num_39;

    private String serialNo;

    private int num_1;

    private int num_2;

    private int num_3;

    public String getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId;
    }

    public List<String> getText_70() {
        return text_70;
    }

    public void setText_70(List<String> text_70) {
        this.text_70 = text_70;
    }

    public String getText_3() {
        return text_3;
    }

    public void setText_3(String text_3) {
        this.text_3 = text_3;
    }

    public int getText_5() {
        return text_5;
    }

    public void setText_5(int text_5) {
        this.text_5 = text_5;
    }

    public List<String> getText_9() {
        return text_9;
    }

    public void setText_9(List<String> text_9) {
        this.text_9 = text_9;
    }

    public String getText_8() {
        return text_8;
    }

    public void setText_8(String text_8) {
        this.text_8 = text_8;
    }

    public int getNum_12() {
        return num_12;
    }

    public void setNum_12(int num_12) {
        this.num_12 = num_12;
    }

    public int getNum_13() {
        return num_13;
    }

    public void setNum_13(int num_13) {
        this.num_13 = num_13;
    }

    public int getNum_36() {
        return num_36;
    }

    public void setNum_36(int num_36) {
        this.num_36 = num_36;
    }

    public String getText_1() {
        return text_1;
    }

    public void setText_1(String text_1) {
        this.text_1 = text_1;
    }

    public List<String> getFile_1() {
        return file_1;
    }

    public void setFile_1(List<String> file_1) {
        this.file_1 = file_1;
    }

    public String getFile_2() {
        return file_2;
    }

    public void setFile_2(String file_2) {
        this.file_2 = file_2;
    }

    public int getNum_39() {
        return num_39;
    }

    public void setNum_39(int num_39) {
        this.num_39 = num_39;
    }

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public int getNum_1() {
        return num_1;
    }

    public void setNum_1(int num_1) {
        this.num_1 = num_1;
    }

    public int getNum_2() {
        return num_2;
    }

    public void setNum_2(int num_2) {
        this.num_2 = num_2;
    }

    public int getNum_3() {
        return num_3;
    }

    public void setNum_3(int num_3) {
        this.num_3 = num_3;
    }

    public String getText_4() {
        return text_4;
    }

    public void setText_4(String text_4) {
        this.text_4 = text_4;
    }
}
