package com.jaka.framework.core.xbongbong.api.invoice.input.dto.add;

import java.util.List;

/**
 * @author ：james.liu
 * @date ：Created in 2022/12/13 8:52
 * @description：
 * @version: 1.0
 */
public class ProV2ApiInvoiceAddDataList {
    /**
     * 开票编号
     */
    private String serialNo;
    /**
     * 发票号码
     */
    private String text_2;
    /**
     * 申请人
     */
    private List<String> ownerId;
    /**
     * 关联客户
     */
    private Integer text_3;
    /**
     * 关联合同
     */
    private List<Integer> text_4;
    /**
     * 关联回款类型
     */
    private String text_61;
    /**
     * 应收款
     */
    private List<Integer> text_5;
    /**
     * 开票日期
     */
    private Integer date_1;
    /**
     * 开票金额
     */
    private Integer num_2;
    /**
     * 发票类型
     */
    private String text_6;
    /**
     * 备注
     */
    private String text_7;
    /**
     * 发票信息
     */
    private List<ProV2ApiInvoiceAddSubForm_1> subForm_1;
    /**
     * 寄送信息
     */
    private List<ProV2ApiInvoiceAddSubForm_2> subForm_2;

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public String getText_2() {
        return text_2;
    }

    public void setText_2(String text_2) {
        this.text_2 = text_2;
    }

    public List<String> getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(List<String> ownerId) {
        this.ownerId = ownerId;
    }

    public Integer getText_3() {
        return text_3;
    }

    public void setText_3(Integer text_3) {
        this.text_3 = text_3;
    }

    public List<Integer> getText_4() {
        return text_4;
    }

    public void setText_4(List<Integer> text_4) {
        this.text_4 = text_4;
    }

    public String getText_61() {
        return text_61;
    }

    public void setText_61(String text_61) {
        this.text_61 = text_61;
    }

    public List<Integer> getText_5() {
        return text_5;
    }

    public void setText_5(List<Integer> text_5) {
        this.text_5 = text_5;
    }

    public Integer getDate_1() {
        return date_1;
    }

    public void setDate_1(Integer date_1) {
        this.date_1 = date_1;
    }

    public Integer getNum_2() {
        return num_2;
    }

    public void setNum_2(Integer num_2) {
        this.num_2 = num_2;
    }

    public String getText_6() {
        return text_6;
    }

    public void setText_6(String text_6) {
        this.text_6 = text_6;
    }

    public String getText_7() {
        return text_7;
    }

    public void setText_7(String text_7) {
        this.text_7 = text_7;
    }

    public List<ProV2ApiInvoiceAddSubForm_1> getSubForm_1() {
        return subForm_1;
    }

    public void setSubForm_1(List<ProV2ApiInvoiceAddSubForm_1> subForm_1) {
        this.subForm_1 = subForm_1;
    }

    public List<ProV2ApiInvoiceAddSubForm_2> getSubForm_2() {
        return subForm_2;
    }

    public void setSubForm_2(List<ProV2ApiInvoiceAddSubForm_2> subForm_2) {
        this.subForm_2 = subForm_2;
    }
}
