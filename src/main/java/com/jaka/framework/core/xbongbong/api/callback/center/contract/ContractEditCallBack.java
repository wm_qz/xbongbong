package com.jaka.framework.core.xbongbong.api.callback.center.contract;

import com.jaka.framework.core.xbongbong.api.callback.center.BaseCallBack;

/**
 * @author ：james.liu
 * @date ：Created in 2022/9/30 14:09
 * @description：合同订单编辑
 * @version: 1.0
 */
public abstract class  ContractEditCallBack extends BaseCallBack {

}