package com.jaka.framework.core.xbongbong.api.crm.contract.input;

import com.jaka.framework.core.xbongbong.api.crm.contract.input.model.edit.DataList;
import com.jaka.framework.core.xbongbong.base.XBBAbstractAPIInput;

/**
 * @author ：james.liu
 * @date ：Created in 2022/11/18 18:04
 * @description：编辑合同订单
 * @version: 1.0
 */
public class XBBApiContractEditInput extends XBBAbstractAPIInput {

    /**
     * 可修改参数信息
     */
    private DataList dataList;
    /**
     * dataID
     */
    private Long dataId;

    public DataList getDataList() {
        return dataList;
    }

    public void setDataList(DataList dataList) {
        this.dataList = dataList;
    }

    public Long getDataId() {
        return dataId;
    }

    public void setDataId(Long dataId) {
        this.dataId = dataId;
    }
}
