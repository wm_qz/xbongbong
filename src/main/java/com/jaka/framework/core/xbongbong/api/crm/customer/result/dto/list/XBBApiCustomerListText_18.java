package com.jaka.framework.core.xbongbong.api.crm.customer.result.dto.list;

/**
 * @author ：james.liu
 * @date ：Created in 2022/12/19 9:41
 * @description：
 * @version: 1.0
 */
public class XBBApiCustomerListText_18 {

    private String value;

    private String text;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
