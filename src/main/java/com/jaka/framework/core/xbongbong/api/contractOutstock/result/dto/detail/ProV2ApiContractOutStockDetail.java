package com.jaka.framework.core.xbongbong.api.contractOutstock.result.dto.detail;

/**
 * @author ：james.liu
 * @date ：Created in 2022/12/5 10:41
 * @description：
 * @version: 1.0
 */
public class ProV2ApiContractOutStockDetail {

    private long addTime;
    private ContractOutStockDetailData data;
    private long dataId;
    private long formId;
    private long updateTime;

    public long getAddTime() {
        return addTime;
    }

    public void setAddTime(long addTime) {
        this.addTime = addTime;
    }

    public ContractOutStockDetailData getData() {
        return data;
    }

    public void setData(ContractOutStockDetailData data) {
        this.data = data;
    }

    public long getDataId() {
        return dataId;
    }

    public void setDataId(long dataId) {
        this.dataId = dataId;
    }

    public long getFormId() {
        return formId;
    }

    public void setFormId(long formId) {
        this.formId = formId;
    }

    public long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(long updateTime) {
        this.updateTime = updateTime;
    }
}
