package com.jaka.framework.core.xbongbong.em;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/22 19:00
 * @description：表单类型
 * @version: 1.0
 */
public enum SaasMarkEnums {

    system_form(1, "系统表单"),

    custom_form(2, "自定义表单"),;

    SaasMarkEnums(Integer saasMark, String saasMarkDesc) {
        this.saasMark = saasMark;
        this.saasMarkDesc = saasMarkDesc;
    }

    private Integer saasMark;

    private String saasMarkDesc;

    public Integer getSaasMark() {
        return saasMark;
    }

    public void setSaasMark(Integer saasMark) {
        this.saasMark = saasMark;
    }

    public String getSaasMarkDesc() {
        return saasMarkDesc;
    }

    public void setSaasMarkDesc(String saasMarkDesc) {
        this.saasMarkDesc = saasMarkDesc;
    }
}
