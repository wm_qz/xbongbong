package com.jaka.framework.core.xbongbong.api.callback.center.warehouse;

/**
 * @author ：james.liu
 * @date ：Created in 2022/9/30 14:09
 * @description：仓库编辑
 * @version: 1.0
 */
public interface WarehouseEditCallBack {

    /**
     * 添加用户回调
     */
    default void callback() {

    }
}
