/**
  * Copyright 2022 bejson.com 
  */
package com.jaka.framework.core.xbongbong.api.contractOutstock.result.dto.detail;

/**
 * Auto-generated: 2022-12-05 10:27:27
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class ContractOutStockDetailSubForm_3 {

    private ContractOutStockDetaiText_1 text_1;
    private String text_2;

    public ContractOutStockDetaiText_1 getText_1() {
        return text_1;
    }

    public void setText_1(ContractOutStockDetaiText_1 text_1) {
        this.text_1 = text_1;
    }

    public void setText_2(String text_2) {
         this.text_2 = text_2;
     }
     public String getText_2() {
         return text_2;
     }

}