package com.jaka.framework.core.xbongbong.api.work.enums;

/**
 * @author ：james.liu
 * @date ：Created in 2022/8/17 17:27
 * @description：
 * @version: 1.0
 */
public enum ServiceSourceEnums {

    SERVICE_SOURCE_1("客户", "597daa9f-c0be-3b15-330a-1447660ab644"),
    SERVICE_SOURCE_2("公司内部", "c5a57135-2484-203a-400b-c3dc7200f4ad"),
    SERVICE_SOURCE_3("海外客户", "2ea40731-580f-ec25-c8ac-cd1d3b32f11c"),

    ;

    ServiceSourceEnums(String text, String value) {
        this.text = text;
        this.value = value;
    }

    private String text;

    private String value;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
