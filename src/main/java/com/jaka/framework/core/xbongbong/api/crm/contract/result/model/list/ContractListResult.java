package com.jaka.framework.core.xbongbong.api.crm.contract.result.model.list;

import java.util.List;

public class ContractListResult {
    
    private List<ContractList> list;

    private int totalCount;

    private int totalPage;

    public List<ContractList> getList() {
        return list;
    }

    public void setList(List<ContractList> list) {
        this.list = list;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getTotalCount() {
        return this.totalCount;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    public int getTotalPage() {
        return this.totalPage;
    }
}
