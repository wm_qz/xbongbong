package com.jaka.framework.core.xbongbong.api.callback.center.contact;

/**
 * @author ：james.liu
 * @date ：Created in 2022/9/30 14:09
 * @description：合同订单归档
 * @version: 1.0
 */
public interface ContactArchivedCallBack {

    default void callback() {

    }
}