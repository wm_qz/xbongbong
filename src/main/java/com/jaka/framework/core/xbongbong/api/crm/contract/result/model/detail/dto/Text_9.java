package com.jaka.framework.core.xbongbong.api.crm.contract.result.model.detail.dto;

/**
 * <pre>
 * </pre>
 * @author toolscat.com
 * @verison $Id:  v 0.1 2022-11-15 11:01:04
 */
public class Text_9 {
    /**
     * <pre>
     *
     * </pre>
     */
    private String	avatar;

    /**
     * <pre>
     *
     * </pre>
     */
    private String	id;

    /**
     * <pre>
     * 张楠楠
     * </pre>
     */
    private String	name;

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
