package com.jaka.framework.core.xbongbong.api.market.result;

import com.jaka.framework.core.xbongbong.base.XBBAbstractAPIResult;

import java.util.List;

/**
 * @author ：james.liu
 * @date ：Created in 2022/1/12 18:36
 * @description：
 * @version: 1.0
 */
public class ProV2ApiClueListResult extends XBBAbstractAPIResult {

    /**
     * 返回信息
     */
    private RList result;

    /**
     * 总数据量
     */
    private Integer totalCount;

    /**
     * 总页码数
     */
    private Integer totalPage;

    public RList getResult() {
        return result;
    }

    public void setResult(RList result) {
        this.result = result;
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public Integer getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(Integer totalPage) {
        this.totalPage = totalPage;
    }

    public static class RList {

        private List<ResultList> list;

        public List<ResultList> getList() {
            return list;
        }

        public void setList(List<ResultList> list) {
            this.list = list;
        }
    }

    public static class ResultList {
        private Long addTime;
        private Data data;
        private Long dataId;
        private Long formId;
        private Long updateTime;

        public Long getAddTime() {
            return addTime;
        }

        public void setAddTime(Long addTime) {
            this.addTime = addTime;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

        public Long getDataId() {
            return dataId;
        }

        public void setDataId(Long dataId) {
            this.dataId = dataId;
        }

        public Long getFormId() {
            return formId;
        }

        public void setFormId(Long formId) {
            this.formId = formId;
        }

        public Long getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(Long updateTime) {
            this.updateTime = updateTime;
        }
    }

    public static class Data {
        private String text_1;
        private Address address_1;
        private List<String> file_1;
        private SubForm subForm_1;
        private String file_2;
        private String creatorId;
        private String text_10;
        private Integer date_1;
        private Long date_3;
        private String text_2;
        private String text_3;
        private String text_4;
        private String text_5;
        private String text_6;
        private String text_7;
        private String text_8;
        private String text_9;
        private Integer num_1;
        private Integer num_2;
        private Integer num_3;

        public String getText_1() {
            return text_1;
        }

        public void setText_1(String text_1) {
            this.text_1 = text_1;
        }

        public Address getAddress_1() {
            return address_1;
        }

        public void setAddress_1(Address address_1) {
            this.address_1 = address_1;
        }

        public List<String> getFile_1() {
            return file_1;
        }

        public void setFile_1(List<String> file_1) {
            this.file_1 = file_1;
        }

        public SubForm getSubForm_1() {
            return subForm_1;
        }

        public void setSubForm_1(SubForm subForm_1) {
            this.subForm_1 = subForm_1;
        }

        public String getFile_2() {
            return file_2;
        }

        public void setFile_2(String file_2) {
            this.file_2 = file_2;
        }

        public String getCreatorId() {
            return creatorId;
        }

        public void setCreatorId(String creatorId) {
            this.creatorId = creatorId;
        }

        public String getText_10() {
            return text_10;
        }

        public void setText_10(String text_10) {
            this.text_10 = text_10;
        }

        public Integer getDate_1() {
            return date_1;
        }

        public void setDate_1(Integer date_1) {
            this.date_1 = date_1;
        }

        public Long getDate_3() {
            return date_3;
        }

        public void setDate_3(Long date_3) {
            this.date_3 = date_3;
        }

        public String getText_2() {
            return text_2;
        }

        public void setText_2(String text_2) {
            this.text_2 = text_2;
        }

        public String getText_3() {
            return text_3;
        }

        public void setText_3(String text_3) {
            this.text_3 = text_3;
        }

        public String getText_4() {
            return text_4;
        }

        public void setText_4(String text_4) {
            this.text_4 = text_4;
        }

        public String getText_5() {
            return text_5;
        }

        public void setText_5(String text_5) {
            this.text_5 = text_5;
        }

        public String getText_6() {
            return text_6;
        }

        public void setText_6(String text_6) {
            this.text_6 = text_6;
        }

        public String getText_7() {
            return text_7;
        }

        public void setText_7(String text_7) {
            this.text_7 = text_7;
        }

        public String getText_8() {
            return text_8;
        }

        public void setText_8(String text_8) {
            this.text_8 = text_8;
        }

        public String getText_9() {
            return text_9;
        }

        public void setText_9(String text_9) {
            this.text_9 = text_9;
        }

        public Integer getNum_1() {
            return num_1;
        }

        public void setNum_1(Integer num_1) {
            this.num_1 = num_1;
        }

        public Integer getNum_2() {
            return num_2;
        }

        public void setNum_2(Integer num_2) {
            this.num_2 = num_2;
        }

        public Integer getNum_3() {
            return num_3;
        }

        public void setNum_3(Integer num_3) {
            this.num_3 = num_3;
        }
    }

    public static class Address {
        private String address;
        private String province;
        private String city;
        private String district;
        private Location location;

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getProvince() {
            return province;
        }

        public void setProvince(String province) {
            this.province = province;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getDistrict() {
            return district;
        }

        public void setDistrict(String district) {
            this.district = district;
        }

        public Location getLocation() {
            return location;
        }

        public void setLocation(Location location) {
            this.location = location;
        }
    }

    private static class Location {
        private float lon;
        private float lat;

        public float getLon() {
            return lon;
        }

        public void setLon(float lon) {
            this.lon = lon;
        }

        public float getLat() {
            return lat;
        }

        public void setLat(float lat) {
            this.lat = lat;
        }
    }


    public static class SubForm {
        private String text_1;
        private String text_2;

        public String getText_1() {
            return text_1;
        }

        public void setText_1(String text_1) {
            this.text_1 = text_1;
        }

        public String getText_2() {
            return text_2;
        }

        public void setText_2(String text_2) {
            this.text_2 = text_2;
        }
    }
}
