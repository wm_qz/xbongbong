package com.jaka.framework.core.xbongbong.api;

import com.jaka.framework.common.core.base.IHttpUtils;
import com.jaka.framework.core.xbongbong.api.contractOutstock.ContractOutStockClient;
import com.jaka.framework.core.xbongbong.api.crm.CRMClient;
import com.jaka.framework.core.xbongbong.api.invoice.InvoiceClient;
import com.jaka.framework.core.xbongbong.api.market.MarketClient;
import com.jaka.framework.core.xbongbong.api.paymentSheet.PaymentSheetClient;
import com.jaka.framework.core.xbongbong.api.product.ProductClient;
import com.jaka.framework.core.xbongbong.api.system.SystemClient;
import com.jaka.framework.core.xbongbong.api.work.WorkOrderClient;
import com.jaka.framework.core.xbongbong.config.XBBAPIClientConfiguration;
import com.jaka.framework.core.xbongbong.util.SignUtils;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/22 14:27
 * @description：API客户端
 * @version: 1.0
 */
public class XBBAPIClient {

    /** 配置中心参数 **/
    private final XBBAPIClientConfiguration configuration;
    /** HTTP请求客户端 **/
    private final IHttpUtils httpUtils;
    /** 签名工具类 **/
    private final SignUtils signUtils;
    /** 系统模块客户端 **/
    private final SystemClient systemClient;
    /** CRM模块客户端 **/
    private final CRMClient crmClient;
    /** 市场活动客户端 **/
    private final MarketClient marketClient;
    /** 工单 **/
    private final WorkOrderClient workOrderClient;
    /** 回款单 **/
    private final PaymentSheetClient paymentSheetClient;
    /** 产品 **/
    private final ProductClient productClient;
    /** 销售出库单 **/
    private final ContractOutStockClient contractOutStockClient;
    /**销项发票**/
    private final InvoiceClient invoiceClient;

    /**
     * 构造函数
     *
     * @param configuration
     * @param httpUtils
     */
    public XBBAPIClient(final XBBAPIClientConfiguration configuration, final IHttpUtils httpUtils) {
        this.configuration = configuration;
        this.httpUtils = httpUtils;
        this.signUtils = new SignUtils(configuration);
        this.systemClient = new SystemClient(configuration, httpUtils, signUtils);
        this.crmClient = new CRMClient(configuration, httpUtils, signUtils);
        this.marketClient = new MarketClient(configuration, httpUtils, signUtils);
        this.workOrderClient = new WorkOrderClient(configuration, httpUtils, signUtils);
        this.paymentSheetClient = new PaymentSheetClient(configuration, httpUtils, signUtils);
        this.productClient = new ProductClient(configuration, httpUtils, signUtils);
        this.contractOutStockClient = new ContractOutStockClient(configuration, httpUtils, signUtils);
        this.invoiceClient = new InvoiceClient(configuration, httpUtils, signUtils);
    }

    public XBBAPIClientConfiguration getConfiguration() {
        return configuration;
    }

    public IHttpUtils getHttpUtils() {
        return httpUtils;
    }

    public SystemClient getSystemClient() {
        return systemClient;
    }

    public SignUtils getSignUtils() {
        return signUtils;
    }

    public CRMClient getCrmClient() {
        return crmClient;
    }

    public MarketClient getMarketClient() {
        return marketClient;
    }

    public WorkOrderClient getWorkOrderClient() {
        return workOrderClient;
    }

    public PaymentSheetClient getPaymentSheetClient() {
        return paymentSheetClient;
    }

    public ProductClient getProductClient() {
        return productClient;
    }

    public ContractOutStockClient getContractOutStockClient() {
        return contractOutStockClient;
    }

    public InvoiceClient getInvoiceClient() {
        return invoiceClient;
    }
}
