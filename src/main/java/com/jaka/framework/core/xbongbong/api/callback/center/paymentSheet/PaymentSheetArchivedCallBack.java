package com.jaka.framework.core.xbongbong.api.callback.center.paymentSheet;

/**
 * @author ：james.liu
 * @date ：Created in 2022/9/30 14:09
 * @description：合同订单归档
 * @version: 1.0
 */
public interface PaymentSheetArchivedCallBack {

    /**
     * 添加用户回调
     */
    default void callback() {

    }
}
