package com.jaka.framework.core.xbongbong.api.crm.communicate.result;

import com.jaka.framework.core.xbongbong.base.XBBAbstractAPIResult;

import java.util.List;

/**
 * @author ：james.liu
 * @date ：Created in 2022/1/28 16:20
 * @description：
 * @version: 1.0
 */
public class ProV2ApiCommunicateListResult extends XBBAbstractAPIResult {

    /**
     * 返回信息
     */
    private RList result;

    public RList getResult() {
        return result;
    }

    public void setResult(RList result) {
        this.result = result;
    }

    public static class RList {

        private List<ResiltList> list;


        /**
         * 总数据量
         */
        private Integer totalCount;

        /**
         * 总页码数
         */
        private Integer totalPage;

        public List<ResiltList> getList() {
            return list;
        }

        public void setList(List<ResiltList> list) {
            this.list = list;
        }

        public Integer getTotalCount() {
            return totalCount;
        }

        public void setTotalCount(Integer totalCount) {
            this.totalCount = totalCount;
        }

        public Integer getTotalPage() {
            return totalPage;
        }

        public void setTotalPage(Integer totalPage) {
            this.totalPage = totalPage;
        }
    }

    public static class ResiltList {
        private Long addTime;
        private Data data;
        private Long dataId;
        private Long formId;
        private Long updateTime;

        public Long getAddTime() {
            return addTime;
        }

        public void setAddTime(Long addTime) {
            this.addTime = addTime;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

        public Long getDataId() {
            return dataId;
        }

        public void setDataId(Long dataId) {
            this.dataId = dataId;
        }

        public Long getFormId() {
            return formId;
        }

        public void setFormId(Long formId) {
            this.formId = formId;
        }

        public Long getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(Long updateTime) {
            this.updateTime = updateTime;
        }
    }

    public static class Data {
        /**
         *creatorId
         */
        private String creatorId;
        /**
         *拜访时间
         */
        private String date_1;
        /**
         *客户名称
         */
        private String text_1;
        /**
         *对接人
         */
        private String text_2;
        /**
         *
         */
        private String text_3;
        /**
         *跟进方式
         */
        private String text_4;
        /**
         *跟进业务
         */
        private String text_5;
        /**
         *备注
         */
        private String text_6;
        /**
         * 工艺描述
         */
        private String text_8;
        /**
         * 客户计划
         */
        private String text_9;
        /**
         * 个人建议
         */
        private String text_10;
        /**
         * 关联机会
         */
        private String text_15;
        /**
         *图片
         */
        private List<String> file_1;
        /**
         *
         */
        private String file_2;
        /**
         *附件
         */
        private String file_3;
        /**
         * 最新报价金额
         */
        private String num_5;
        /**
         *创建时间
         */
        private String addTime;
        /**
         *更新时间
         */
        private String updateTime;


        public String getText_8() {
            return text_8;
        }

        public void setText_8(String text_8) {
            this.text_8 = text_8;
        }

        public String getText_9() {
            return text_9;
        }

        public void setText_9(String text_9) {
            this.text_9 = text_9;
        }

        public String getText_10() {
            return text_10;
        }

        public void setText_10(String text_10) {
            this.text_10 = text_10;
        }

        public String getText_15() {
            return text_15;
        }

        public void setText_15(String text_15) {
            this.text_15 = text_15;
        }

        public String getNum_5() {
            return num_5;
        }

        public void setNum_5(String num_5) {
            this.num_5 = num_5;
        }

        public String getAddTime() {
            return addTime;
        }

        public void setAddTime(String addTime) {
            this.addTime = addTime;
        }

        public String getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(String updateTime) {
            this.updateTime = updateTime;
        }

        public String getCreatorId() {
            return creatorId;
        }

        public void setCreatorId(String creatorId) {
            this.creatorId = creatorId;
        }

        public String getDate_1() {
            return date_1;
        }

        public void setDate_1(String date_1) {
            this.date_1 = date_1;
        }

        public String getText_1() {
            return text_1;
        }

        public void setText_1(String text_1) {
            this.text_1 = text_1;
        }

        public String getText_2() {
            return text_2;
        }

        public void setText_2(String text_2) {
            this.text_2 = text_2;
        }

        public String getText_3() {
            return text_3;
        }

        public void setText_3(String text_3) {
            this.text_3 = text_3;
        }

        public String getText_4() {
            return text_4;
        }

        public void setText_4(String text_4) {
            this.text_4 = text_4;
        }

        public String getText_5() {
            return text_5;
        }

        public void setText_5(String text_5) {
            this.text_5 = text_5;
        }

        public String getText_6() {
            return text_6;
        }

        public void setText_6(String text_6) {
            this.text_6 = text_6;
        }

        public List<String> getFile_1() {
            return file_1;
        }

        public void setFile_1(List<String> file_1) {
            this.file_1 = file_1;
        }

        public String getFile_2() {
            return file_2;
        }

        public void setFile_2(String file_2) {
            this.file_2 = file_2;
        }

        public String getFile_3() {
            return file_3;
        }

        public void setFile_3(String file_3) {
            this.file_3 = file_3;
        }
    }

}
