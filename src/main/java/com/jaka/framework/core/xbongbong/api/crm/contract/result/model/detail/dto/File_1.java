package com.jaka.framework.core.xbongbong.api.crm.contract.result.model.detail.dto;

/**
 * <pre>
 *  File_1
 * </pre>
 * @author toolscat.com
 * @verison $Id: File_1 v 0.1 2022-11-15 11:01:04
 */
public class File_1{

    /**
     * <pre>
     * 
     * </pre>
     */
    private String	attachIndex;

    /**
     * <pre>
     * 
     * </pre>
     */
    private String	ext;

    /**
     * <pre>
     * 小助机器人本体销售合同-Zu7-20220823.pdf
     * </pre>
     */
    private String	filename;

    /**
     * <pre>
     * 
     * </pre>
     */
    private Integer	size;


    public String getAttachIndex() {
      return this.attachIndex;
    }

    public void setAttachIndex(String attachIndex) {
      this.attachIndex = attachIndex;
    }

    public String getExt() {
      return this.ext;
    }

    public void setExt(String ext) {
      this.ext = ext;
    }

    public String getFilename() {
      return this.filename;
    }

    public void setFilename(String filename) {
      this.filename = filename;
    }

    public Integer getSize() {
      return this.size;
    }

    public void setSize(Integer size) {
      this.size = size;
    }

}
