package com.jaka.framework.core.xbongbong.base;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/22 17:32
 * @description：
 * @version: 1.0
 */
public class XBBAbstractAPIPageInput extends XBBAbstractAPIInput {

    /**
     * 页码，默认为1
     */
    private Integer page;
    /**
     * 每页数量，默认为20，最大值100
     */
    private Integer pageSize;

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }
}
