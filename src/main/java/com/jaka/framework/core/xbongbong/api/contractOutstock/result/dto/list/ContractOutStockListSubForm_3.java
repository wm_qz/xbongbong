/**
  * Copyright 2022 bejson.com 
  */
package com.jaka.framework.core.xbongbong.api.contractOutstock.result.dto.list;

/**
 * Auto-generated: 2022-12-05 9:52:43
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class ContractOutStockListSubForm_3 {

    private String text_1;
    private String text_2;
    public void setText_1(String text_1) {
         this.text_1 = text_1;
     }
     public String getText_1() {
         return text_1;
     }

    public void setText_2(String text_2) {
         this.text_2 = text_2;
     }
     public String getText_2() {
         return text_2;
     }

}