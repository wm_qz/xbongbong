package com.jaka.framework.core.xbongbong.api.work.input;

import com.jaka.framework.core.xbongbong.base.XBBAbstractAPIInput;

/**
 * @author ：james.liu
 * @date ：Created in 2022/8/17 9:00
 * @description：工单模板列表接口
 * @version: 1.0
 */
public class ApiWorkOrderTemplateListInput extends XBBAbstractAPIInput {
}
