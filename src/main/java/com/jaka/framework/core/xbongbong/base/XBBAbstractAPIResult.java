package com.jaka.framework.core.xbongbong.base;

import com.jaka.framework.common.core.model.AbstractToJsonString;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/22 11:41
 * @description：销帮帮出参基类
 * @version: 1.0
 */
public class XBBAbstractAPIResult implements AbstractToJsonString {

    /**
     * 错误码
     */
    private Integer code;
    /**
     * 提示信息
     */
    private String msg;
    /**
     *
     */
    private boolean success;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
