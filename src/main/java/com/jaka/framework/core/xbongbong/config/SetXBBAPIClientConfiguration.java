package com.jaka.framework.core.xbongbong.config;

import com.jaka.framework.common.core.base.OKHttpUtils;
import com.jaka.framework.core.xbongbong.api.XBBAPIClient;

/**
 * @author ：james.liu
 * @date ：Created in 2022/2/16 17:00
 * @description：
 * @version: 1.0
 */
public abstract class SetXBBAPIClientConfiguration extends XBBAPIClientConfiguration {

    public SetXBBAPIClientConfiguration() {

    }

    @Override
    public XBBAPIClient build() {
        autoSetOkHttpClientIfOkHttpClientIsNull();
        setClientConfiguration();
        return new XBBAPIClient(this, new OKHttpUtils(okHttpClient));
    }

}
