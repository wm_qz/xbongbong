package com.jaka.framework.core.xbongbong.api.work.input;

import com.jaka.framework.core.xbongbong.base.XBBAbstractAPIInput;

/**
 * @author ：james.liu
 * @date ：Created in 2022/8/17 10:28
 * @description：
 * @version: 1.0
 */
public class ApiWorkOrderTemplateDetailInput extends XBBAbstractAPIInput {

    /**
     * 工单id
     */
    private Long formId;

    public Long getFormId() {
        return formId;
    }

    public void setFormId(Long formId) {
        this.formId = formId;
    }
}
