package com.jaka.framework.core.xbongbong.api.crm.customer.input;

import com.jaka.framework.core.xbongbong.base.XBBAbstractAPIPageInput;

import java.util.List;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/22 16:15
 * @description：客户列表接口入参
 * @version: 1.0
 */
public class XBBApiCustomerListInput extends XBBAbstractAPIPageInput {

    /**
     * 0:客户列表 1:回收站数据，默认为0
     */
    private Integer del;
    /**
     * 是否公海客户
     */
    private Integer isPublic;
    /**
     * 表单id
     */
    private Long formId;
    /**
     * 条件集合
     */
    private List<ConditionsList> conditions;

    public Integer getDel() {
        return del;
    }

    public void setDel(Integer del) {
        this.del = del;
    }

    public Integer getIsPublic() {
        return isPublic;
    }

    public void setIsPublic(Integer isPublic) {
        this.isPublic = isPublic;
    }

    public Long getFormId() {
        return formId;
    }

    public void setFormId(Long formId) {
        this.formId = formId;
    }

    public List<ConditionsList> getConditions() {
        return conditions;
    }

    public void setConditions(List<ConditionsList> conditions) {
        this.conditions = conditions;
    }

    public static class ConditionsList {

        private String attr;

        private String symbol;

        private List<String> value;

        public String getAttr() {
            return attr;
        }

        public void setAttr(String attr) {
            this.attr = attr;
        }

        public String getSymbol() {
            return symbol;
        }

        public void setSymbol(String symbol) {
            this.symbol = symbol;
        }

        public List<String> getValue() {
            return value;
        }

        public void setValue(List<String> value) {
            this.value = value;
        }
    }


}
