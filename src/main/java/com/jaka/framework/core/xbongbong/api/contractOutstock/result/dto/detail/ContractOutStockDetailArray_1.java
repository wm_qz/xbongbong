/**
 * Copyright 2022 bejson.com
 */
package com.jaka.framework.core.xbongbong.api.contractOutstock.result.dto.detail;

/**
 * Auto-generated: 2022-12-05 10:27:27
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class ContractOutStockDetailArray_1 {

    private long text_1;
    private long refProductId;
    private long num_5;
    private String text_3;
    private int num_6;
    private int num_7;
    private ContractOutStockDetailText_5 text_5;
    private long num_1;
    private long productSubId;
    private int num_2;
    private ContractOutStockDetailText_6 text_6;
    private int num_3;
    private double num_4;
    private ContractOutStockDetailText_8 text_8;

    public void setText_1(long text_1) {
        this.text_1 = text_1;
    }

    public long getText_1() {
        return text_1;
    }

    public void setRefProductId(long refProductId) {
        this.refProductId = refProductId;
    }

    public long getRefProductId() {
        return refProductId;
    }

    public void setNum_5(long num_5) {
        this.num_5 = num_5;
    }

    public long getNum_5() {
        return num_5;
    }

    public void setText_3(String text_3) {
        this.text_3 = text_3;
    }

    public String getText_3() {
        return text_3;
    }

    public void setNum_6(int num_6) {
        this.num_6 = num_6;
    }

    public int getNum_6() {
        return num_6;
    }

    public void setNum_7(int num_7) {
        this.num_7 = num_7;
    }

    public int getNum_7() {
        return num_7;
    }


    public void setNum_1(long num_1) {
        this.num_1 = num_1;
    }

    public long getNum_1() {
        return num_1;
    }

    public void setProductSubId(long productSubId) {
        this.productSubId = productSubId;
    }

    public long getProductSubId() {
        return productSubId;
    }

    public void setNum_2(int num_2) {
        this.num_2 = num_2;
    }

    public int getNum_2() {
        return num_2;
    }

    public void setNum_3(int num_3) {
        this.num_3 = num_3;
    }

    public int getNum_3() {
        return num_3;
    }

    public void setNum_4(double num_4) {
        this.num_4 = num_4;
    }

    public double getNum_4() {
        return num_4;
    }

    public ContractOutStockDetailText_5 getText_5() {
        return text_5;
    }

    public void setText_5(ContractOutStockDetailText_5 text_5) {
        this.text_5 = text_5;
    }

    public ContractOutStockDetailText_6 getText_6() {
        return text_6;
    }

    public void setText_6(ContractOutStockDetailText_6 text_6) {
        this.text_6 = text_6;
    }

    public ContractOutStockDetailText_8 getText_8() {
        return text_8;
    }

    public void setText_8(ContractOutStockDetailText_8 text_8) {
        this.text_8 = text_8;
    }
}