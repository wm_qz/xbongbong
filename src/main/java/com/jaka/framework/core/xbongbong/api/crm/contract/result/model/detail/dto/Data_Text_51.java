package com.jaka.framework.core.xbongbong.api.crm.contract.result.model.detail.dto;

/**
 * @author ：james.liu
 * @date ：Created in 2022/11/30 17:22
 * @description：
 * @version: 1.0
 */
public class Data_Text_51 {

    private boolean checked;

    private Integer isOther;

    private String text;

    private String value;

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public Integer getIsOther() {
        return isOther;
    }

    public void setIsOther(Integer isOther) {
        this.isOther = isOther;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
