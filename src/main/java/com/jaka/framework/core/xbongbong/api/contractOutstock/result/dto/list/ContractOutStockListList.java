/**
  * Copyright 2022 bejson.com 
  */
package com.jaka.framework.core.xbongbong.api.contractOutstock.result.dto.list;

/**
 * Auto-generated: 2022-12-05 9:52:43
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class ContractOutStockListList {

    private long addTime;
    private ContractOutStockListData data;
    private long dataId;
    private long formId;
    private long updateTime;
    public void setAddTime(long addTime) {
         this.addTime = addTime;
     }
     public long getAddTime() {
         return addTime;
     }

    public ContractOutStockListData getData() {
        return data;
    }

    public void setData(ContractOutStockListData data) {
        this.data = data;
    }

    public void setDataId(long dataId) {
         this.dataId = dataId;
     }
     public long getDataId() {
         return dataId;
     }

    public void setFormId(long formId) {
         this.formId = formId;
     }
     public long getFormId() {
         return formId;
     }

    public void setUpdateTime(long updateTime) {
         this.updateTime = updateTime;
     }
     public long getUpdateTime() {
         return updateTime;
     }

}