package com.jaka.framework.core.xbongbong.api.callback.center.customer;

import com.jaka.framework.core.xbongbong.api.callback.center.BaseCallBack;

/**
 * @author ：james.liu
 * @date ：Created in 2022/9/30 14:05
 * @description：客户新建回调
 * @version: 1.0
 */
public abstract class CustomerEditCallBack extends BaseCallBack {

}