package com.jaka.framework.core.xbongbong.api.product.result.dto;

/**
 * @author ：james.liu
 * @date ：Created in 2022/11/23 13:46
 * @description：
 * @version: 1.0
 */
public class ProductListResult {

    private int addTime;

    private ProductListData data;

    private int dataId;

    private int formId;

    private int updateTime;

    public int getAddTime() {
        return addTime;
    }

    public void setAddTime(int addTime) {
        this.addTime = addTime;
    }

    public ProductListData getData() {
        return data;
    }

    public void setData(ProductListData data) {
        this.data = data;
    }

    public int getDataId() {
        return dataId;
    }

    public void setDataId(int dataId) {
        this.dataId = dataId;
    }

    public int getFormId() {
        return formId;
    }

    public void setFormId(int formId) {
        this.formId = formId;
    }

    public int getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(int updateTime) {
        this.updateTime = updateTime;
    }
}
