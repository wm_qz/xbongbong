package com.jaka.framework.core.xbongbong.api.market.input;

import com.jaka.framework.core.xbongbong.base.XBBAbstractAPIInput;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/22 18:01
 * @description：市场活动详情接口入参
 * @version: 1.0
 */
public class XBBApiMarketActivityDetailInput extends XBBAbstractAPIInput {

    /**
     * 市场活动id
     */
    private Long dataId;

    public Long getDataId() {
        return dataId;
    }

    public void setDataId(Long dataId) {
        this.dataId = dataId;
    }
}
