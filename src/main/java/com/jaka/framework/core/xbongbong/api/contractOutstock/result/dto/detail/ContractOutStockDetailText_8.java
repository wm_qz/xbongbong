/**
 * Copyright 2022 bejson.com
 */
package com.jaka.framework.core.xbongbong.api.contractOutstock.result.dto.detail;

/**
 * Auto-generated: 2022-12-05 10:27:27
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class ContractOutStockDetailText_8 {

    private String text;
    private String value;

    public void setText(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}