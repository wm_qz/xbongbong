package com.jaka.framework.core.xbongbong.api.invoice.input.dto.add;

/**
 * @author ：james.liu
 * @date ：Created in 2022/12/13 8:53
 * @description：
 * @version: 1.0
 */
public class ProV2ApiInvoiceAddAddress_1 {

    private String city;

    private String address;

    private String district;

    private String province;

    private ProV2ApiInvoiceAddLocation location;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public ProV2ApiInvoiceAddLocation getLocation() {
        return location;
    }

    public void setLocation(ProV2ApiInvoiceAddLocation location) {
        this.location = location;
    }
}
