package com.jaka.framework.core.xbongbong.api.system.from.result.detail;

/**
 * @author ：james.liu
 * @date ：Created in 2022/11/30 9:31
 * @description：
 * @version: 1.0
 */
public class SerialNumber {

    private String dateFormat;

    private int numberDigit;

    private String postfix;

    private String prefix;

    private int resetCycle;

    private int startNum;

    public String getDateFormat() {
        return dateFormat;
    }

    public void setDateFormat(String dateFormat) {
        this.dateFormat = dateFormat;
    }

    public int getNumberDigit() {
        return numberDigit;
    }

    public void setNumberDigit(int numberDigit) {
        this.numberDigit = numberDigit;
    }

    public String getPostfix() {
        return postfix;
    }

    public void setPostfix(String postfix) {
        this.postfix = postfix;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public int getResetCycle() {
        return resetCycle;
    }

    public void setResetCycle(int resetCycle) {
        this.resetCycle = resetCycle;
    }

    public int getStartNum() {
        return startNum;
    }

    public void setStartNum(int startNum) {
        this.startNum = startNum;
    }
}
