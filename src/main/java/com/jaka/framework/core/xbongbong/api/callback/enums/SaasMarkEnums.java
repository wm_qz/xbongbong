package com.jaka.framework.core.xbongbong.api.callback.enums;

/**
 * @author ：james.liu
 * @date ：Created in 2022/9/30 13:37
 * @description：
 * @version: 1.0
 */
public enum SaasMarkEnums {
    /**
     * 销帮帮系统模板数据
     */
    saas_mark_1(1, "销帮帮系统模板数据"),
    /**
     * 自定义表单数据
     */
    saas_mark_2(2, "自定义表单数据"),
    ;

    SaasMarkEnums(Integer value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    private Integer value;

    private String desc;

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
