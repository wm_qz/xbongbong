package com.jaka.framework.core.xbongbong.api.crm.contract.result.model.detail.dto;

/**
 * @author ：james.liu
 * @date ：Created in 2022/11/30 11:28
 * @description：
 * @version: 1.0
 */
public class Array_Text_5 {

    private String color;

    private boolean checked;

    private String text;

    private String value;

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
