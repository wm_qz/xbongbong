package com.jaka.framework.core.xbongbong.api.paymentSheet.result.dto.list;

/**
 * @author ：james.liu
 * @date ：Created in 2022/11/21 14:48
 * @description：
 * @version: 1.0
 */
public class PaymentSheetList {

    private int addTime;

    private PaymentSheetListData data;

    private int dataId;

    private int formId;

    private int updateTime;

    private Integer alone;

    private String uuid;

    public int getAddTime() {
        return addTime;
    }

    public void setAddTime(int addTime) {
        this.addTime = addTime;
    }

    public PaymentSheetListData getData() {
        return data;
    }

    public void setData(PaymentSheetListData data) {
        this.data = data;
    }

    public int getDataId() {
        return dataId;
    }

    public void setDataId(int dataId) {
        this.dataId = dataId;
    }

    public int getFormId() {
        return formId;
    }

    public void setFormId(int formId) {
        this.formId = formId;
    }

    public int getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(int updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getAlone() {
        return alone;
    }

    public void setAlone(Integer alone) {
        this.alone = alone;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
