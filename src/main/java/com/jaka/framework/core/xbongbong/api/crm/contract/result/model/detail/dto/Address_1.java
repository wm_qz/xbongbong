package com.jaka.framework.core.xbongbong.api.crm.contract.result.model.detail.dto;

/**
 * <pre>
 *  Address_1
 * </pre>
 * @author toolscat.com
 * @verison $Id: Address_1 v 0.1 2022-11-15 11:01:04
 */
public class Address_1{

    /**
     * <pre>
     * 
     * </pre>
     */
    private String	address;

    /**
     * <pre>
     * 
     * </pre>
     */
    private String	city;

    /**
     * <pre>
     * 
     * </pre>
     */
    private String	district;

    /**
     * <pre>
     * 
     * </pre>
     */
    private String	province;


    public String getAddress() {
      return this.address;
    }

    public void setAddress(String address) {
      this.address = address;
    }

    public String getCity() {
      return this.city;
    }

    public void setCity(String city) {
      this.city = city;
    }

    public String getDistrict() {
      return this.district;
    }

    public void setDistrict(String district) {
      this.district = district;
    }

    public String getProvince() {
      return this.province;
    }

    public void setProvince(String province) {
      this.province = province;
    }

}
