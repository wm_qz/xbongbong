package com.jaka.framework.core.xbongbong.api.work.input;

import com.jaka.framework.core.xbongbong.base.XBBAbstractAPIInput;

/**
 * @author ：james.liu
 * @date ：Created in 2022/8/17 16:54
 * @description：
 * @version: 1.0
 */
public class ApiWorkOrderAddInput extends XBBAbstractAPIInput {

    /**
     * 工单编号
     */
    private String serialNo;
    /**
     * 工单名称
     */
    private String text_13;
    /**
     * 服务来源
     */
    private String text_15;
    /**
     * 需求部门
     */
    private String text_17;
    /**
     * 关联客户
     */
    private String text_1;
    /**
     * 服务内容
     */
    private String text_18;
    /**
     * 期望开始时间
     */
    private String date_2;
    /**
     * 客户渠道
     */
    private String text_16;
    /**
     * 海外客户名称
     */
    private String text_23;
    /**
     * 客户联系人
     */
    private String text_2;
    /**
     * 联系方式
     */
    private String subForm_1;
    /**
     * 地址
     */
    private String address_1;
    /**
     * 关联机会
     */
    private String text_3;
    /**
     * 实际工时
     */
    private String num_4;
    /**
     * 机器人序列号
     */
    private String text_22;
    /**
     * 工单服务需求描述
     */
    private String text_5;
    /**
     * 紧急程度
     */
    private String text_6;
    /**
     * 负责人
     */
    private String ownerId;
    /**
     * 协同人
     */
    private String coUserId;
    /**
     * 工单创建人所属部门
     */
    private String departmentId;
    /**
     * 创建人
     */
    private String creatorId;
    /**
     * 日志创建人所属部门
     */
    private String text_25;
    /**
     * 创建时间
     */
    private String addTime;
    /**
     * 更新时间
     */
    private String updateTime;
    /**
     * 客户编码
     */
    private String text_19;
    /**
     * 客户名称
     */
    private String text_21;
    /**
     * 项目编码
     */
    private String text_20;
    /**
     * 工单创建人
     */
    private String text_24;
    /**
     * 工单创建人
     */
    private String text_11;
    /**
     * 工单负责人
     */
    private String status;
    /**
     *评价星级
     */
    private String num_2;
    /**
     *阶段状态
     */
    private String num_3;

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public String getText_13() {
        return text_13;
    }

    public void setText_13(String text_13) {
        this.text_13 = text_13;
    }

    public String getText_15() {
        return text_15;
    }

    public void setText_15(String text_15) {
        this.text_15 = text_15;
    }

    public String getText_17() {
        return text_17;
    }

    public void setText_17(String text_17) {
        this.text_17 = text_17;
    }

    public String getText_1() {
        return text_1;
    }

    public void setText_1(String text_1) {
        this.text_1 = text_1;
    }

    public String getText_18() {
        return text_18;
    }

    public void setText_18(String text_18) {
        this.text_18 = text_18;
    }

    public String getDate_2() {
        return date_2;
    }

    public void setDate_2(String date_2) {
        this.date_2 = date_2;
    }

    public String getText_16() {
        return text_16;
    }

    public void setText_16(String text_16) {
        this.text_16 = text_16;
    }

    public String getText_23() {
        return text_23;
    }

    public void setText_23(String text_23) {
        this.text_23 = text_23;
    }

    public String getText_2() {
        return text_2;
    }

    public void setText_2(String text_2) {
        this.text_2 = text_2;
    }

    public String getSubForm_1() {
        return subForm_1;
    }

    public void setSubForm_1(String subForm_1) {
        this.subForm_1 = subForm_1;
    }

    public String getAddress_1() {
        return address_1;
    }

    public void setAddress_1(String address_1) {
        this.address_1 = address_1;
    }

    public String getText_3() {
        return text_3;
    }

    public void setText_3(String text_3) {
        this.text_3 = text_3;
    }

    public String getNum_4() {
        return num_4;
    }

    public void setNum_4(String num_4) {
        this.num_4 = num_4;
    }

    public String getText_22() {
        return text_22;
    }

    public void setText_22(String text_22) {
        this.text_22 = text_22;
    }

    public String getText_5() {
        return text_5;
    }

    public void setText_5(String text_5) {
        this.text_5 = text_5;
    }

    public String getText_6() {
        return text_6;
    }

    public void setText_6(String text_6) {
        this.text_6 = text_6;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getCoUserId() {
        return coUserId;
    }

    public void setCoUserId(String coUserId) {
        this.coUserId = coUserId;
    }

    public String getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(String departmentId) {
        this.departmentId = departmentId;
    }

    public String getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId;
    }

    public String getText_25() {
        return text_25;
    }

    public void setText_25(String text_25) {
        this.text_25 = text_25;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getText_19() {
        return text_19;
    }

    public void setText_19(String text_19) {
        this.text_19 = text_19;
    }

    public String getText_21() {
        return text_21;
    }

    public void setText_21(String text_21) {
        this.text_21 = text_21;
    }

    public String getText_20() {
        return text_20;
    }

    public void setText_20(String text_20) {
        this.text_20 = text_20;
    }

    public String getText_24() {
        return text_24;
    }

    public void setText_24(String text_24) {
        this.text_24 = text_24;
    }

    public String getText_11() {
        return text_11;
    }

    public void setText_11(String text_11) {
        this.text_11 = text_11;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNum_2() {
        return num_2;
    }

    public void setNum_2(String num_2) {
        this.num_2 = num_2;
    }

    public String getNum_3() {
        return num_3;
    }

    public void setNum_3(String num_3) {
        this.num_3 = num_3;
    }
}
