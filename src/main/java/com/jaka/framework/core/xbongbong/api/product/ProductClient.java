package com.jaka.framework.core.xbongbong.api.product;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jaka.framework.common.core.base.IHttpUtils;
import com.jaka.framework.core.xbongbong.api.product.input.ProV2ApiProductListInput;
import com.jaka.framework.core.xbongbong.api.product.input.XBBApiProductDetailInput;
import com.jaka.framework.core.xbongbong.api.product.result.ProV2ApiProductListResult;
import com.jaka.framework.core.xbongbong.api.product.result.dto.XBBApiProductDetailResult;
import com.jaka.framework.core.xbongbong.base.XBBDefaultAPIActionProxy;
import com.jaka.framework.core.xbongbong.base.XBBRequestMappering;
import com.jaka.framework.core.xbongbong.config.XBBAPIClientConfiguration;
import com.jaka.framework.core.xbongbong.util.SignUtils;

/**
 * @author ：james.liu
 * @date ：Created in 2022/11/23 12:47
 * @description：产品管理
 * @version: 1.0
 */
public class ProductClient {

    private final IHttpUtils httpUtils;
    private final XBBAPIClientConfiguration configuration;
    private final SignUtils signUtils;

    public ProductClient(final XBBAPIClientConfiguration configuration,
                              final IHttpUtils httpUtils,
                              final SignUtils signUtils) {
        this.configuration = configuration;
        this.httpUtils = httpUtils;
        this.signUtils = signUtils;
    }

    /**
     * 产品列表
     * @param input
     * @return
     * @throws Exception
     */
    public ProV2ApiProductListResult  proV2ApiProductList(final ProV2ApiProductListInput input) throws Exception {
        input.setCmdId(XBBRequestMappering.pro_v2_api_product_list.getMapperName());
        input.setCorpid(configuration.getCorpId());
        input.setUserId(configuration.getUserId());
        JSONObject jsonObject = removeSignData(input.toJsonString());
        return XBBDefaultAPIActionProxy.getInstance().doAction(input,
                ProV2ApiProductListResult.class,
                configuration,
                httpUtils,
                signUtils,
                input.toRequestBody(jsonObject),
                jsonObject);
    }

    /**
     *
     * @param input
     * @return
     * @throws Exception
     */
    public XBBApiProductDetailResult proV2ApiProductDetail(final XBBApiProductDetailInput input) throws Exception {
        input.setCmdId(XBBRequestMappering.pro_v2_api_product_detail.getMapperName());
        input.setCorpid(configuration.getCorpId());
        input.setUserId(configuration.getUserId());
        JSONObject jsonObject = removeSignData(input.toJsonString());
        return XBBDefaultAPIActionProxy.getInstance().doAction(input,
                XBBApiProductDetailResult.class,
                configuration,
                httpUtils,
                signUtils,
                input.toRequestBody(jsonObject),
                jsonObject);
    }


    public final JSONObject removeSignData(String data) {
        JSONObject signData = JSON.parseObject(data);
        if (signData.containsKey("cmdId")) {
            signData.remove("cmdId");
        }
        if (signData.containsKey("signUtils")) {
            signData.remove("signUtils");
        }
        return (signData.isEmpty()) ? null : signData;
    }

}
