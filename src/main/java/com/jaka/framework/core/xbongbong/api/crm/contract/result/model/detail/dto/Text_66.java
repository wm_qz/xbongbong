package com.jaka.framework.core.xbongbong.api.crm.contract.result.model.detail.dto;

/**
 * <pre>
 *  Text_66
 * </pre>
 * @author toolscat.com
 * @verison $Id: Text_66 v 0.1 2022-11-15 11:01:04
 */
public class Text_66{

    /**
     * <pre>
     * 
     * </pre>
     */
    private Integer	id;

    /**
     * <pre>
     * 
     * </pre>
     */
    private String	name;


    public Integer getId() {
      return this.id;
    }

    public void setId(Integer id) {
      this.id = id;
    }

    public String getName() {
      return this.name;
    }

    public void setName(String name) {
      this.name = name;
    }

}
