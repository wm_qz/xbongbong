package com.jaka.framework.core.xbongbong.api.crm.customer.input;

import com.jaka.framework.core.xbongbong.base.XBBAbstractAPIPageInput;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/23 9:36
 * @description：客户详情接口入参
 * @version: 1.0
 */
public class XBBApiCustomerDetailInput extends XBBAbstractAPIPageInput {

    /**
     * 客户id
     */
    private Long dataId;

    public Long getDataId() {
        return dataId;
    }

    public void setDataId(Long dataId) {
        this.dataId = dataId;
    }
}
