package com.jaka.framework.core.xbongbong.api.crm.contract.result.model.detail.dto;

/**
 * @author ：james.liu
 * @date ：Created in 2022/11/22 19:32
 * @description：
 * @version: 1.0
 */
public class SubFormText_2 {

    /**
     * <pre>
     *
     * </pre>
     */
    private String	text;

    /**
     * <pre>
     *
     * </pre>
     */
    private String	value;


    public String getText() {
        return this.text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
