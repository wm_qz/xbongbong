package com.jaka.framework.core.xbongbong.api.contractOutstock.input.dto.save;

/**
 * @author ：james.liu
 * @date ：Created in 2022/12/2 14:07
 * @description：
 * @version: 1.0
 */
public class ProV2ApiContractOutStockAddArray_1 {
    /**
     * 产品名称
     */
    private Integer text_1;
    /**
     * 备注
     */
    private String text_3;
    /**
     * 仓库
     */
    private String text_6;
    /**
     * 出库数量
     */
    private Integer num_3;
    /**
     * 售价（元）
     */
    private String num_6;
    /**
     * 售价小计
     */
    private String num_5;
    /**
     * 出库成本
     */
    private Integer num_2;
    /**
     * 成本小计
     */
    private String num_7;
    /**
     * 单位
     */
    private String text_8;
    /**
     * 单价
     */
    private Integer num_1;
    /**
     * 折扣（%）
     */
    private Integer num_4;
    /**
     * 出库产品关联客户
     */
    private String text_2;
    /**
     * 批次号
     */
    private String text_5;
    /**
     * 生产日期
     */
    private String num_8;
    /**
     * 保质期
     */
    private String num_9;
    /**
     * 到期日期
     */
    private String num_40;

    private Integer productSubId;

    private Integer refProductId;

    public Integer getText_1() {
        return text_1;
    }

    public void setText_1(Integer text_1) {
        this.text_1 = text_1;
    }

    public String getText_3() {
        return text_3;
    }

    public void setText_3(String text_3) {
        this.text_3 = text_3;
    }

    public String getText_6() {
        return text_6;
    }

    public void setText_6(String text_6) {
        this.text_6 = text_6;
    }

    public Integer getNum_3() {
        return num_3;
    }

    public void setNum_3(Integer num_3) {
        this.num_3 = num_3;
    }

    public String getNum_6() {
        return num_6;
    }

    public void setNum_6(String num_6) {
        this.num_6 = num_6;
    }

    public String getNum_5() {
        return num_5;
    }

    public void setNum_5(String num_5) {
        this.num_5 = num_5;
    }

    public Integer getNum_2() {
        return num_2;
    }

    public void setNum_2(Integer num_2) {
        this.num_2 = num_2;
    }

    public String getNum_7() {
        return num_7;
    }

    public void setNum_7(String num_7) {
        this.num_7 = num_7;
    }

    public String getText_8() {
        return text_8;
    }

    public void setText_8(String text_8) {
        this.text_8 = text_8;
    }

    public Integer getNum_1() {
        return num_1;
    }

    public void setNum_1(Integer num_1) {
        this.num_1 = num_1;
    }

    public Integer getNum_4() {
        return num_4;
    }

    public void setNum_4(Integer num_4) {
        this.num_4 = num_4;
    }

    public String getText_2() {
        return text_2;
    }

    public void setText_2(String text_2) {
        this.text_2 = text_2;
    }

    public String getText_5() {
        return text_5;
    }

    public void setText_5(String text_5) {
        this.text_5 = text_5;
    }

    public String getNum_8() {
        return num_8;
    }

    public void setNum_8(String num_8) {
        this.num_8 = num_8;
    }

    public String getNum_9() {
        return num_9;
    }

    public void setNum_9(String num_9) {
        this.num_9 = num_9;
    }

    public String getNum_40() {
        return num_40;
    }

    public void setNum_40(String num_40) {
        this.num_40 = num_40;
    }

    public Integer getProductSubId() {
        return productSubId;
    }

    public void setProductSubId(Integer productSubId) {
        this.productSubId = productSubId;
    }

    public Integer getRefProductId() {
        return refProductId;
    }

    public void setRefProductId(Integer refProductId) {
        this.refProductId = refProductId;
    }
}
