package com.jaka.framework.core.xbongbong.api.paymentSheet.result;

import com.jaka.framework.core.xbongbong.api.paymentSheet.result.dto.detail.PaymentSheetDetailResult;
import com.jaka.framework.core.xbongbong.base.XBBAbstractAPIResult;

/**
 * @author ：james.liu
 * @date ：Created in 2022/11/21 14:41
 * @description：
 * @version: 1.0
 */
public class ProV2ApiPaymentSheetDetailResult extends XBBAbstractAPIResult {

    private PaymentSheetDetailResult result;

    public PaymentSheetDetailResult getResult() {
        return result;
    }

    public void setResult(PaymentSheetDetailResult result) {
        this.result = result;
    }
}
