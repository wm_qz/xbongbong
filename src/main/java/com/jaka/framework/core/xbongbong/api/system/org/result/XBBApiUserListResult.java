package com.jaka.framework.core.xbongbong.api.system.org.result;

import com.jaka.framework.core.xbongbong.base.XBBAbstractAPIResult;
import com.jaka.framework.core.xbongbong.base.XBBPageResult;

import java.util.List;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/22 13:26
 * @description：
 * @version: 1.0
 */
public class XBBApiUserListResult extends XBBAbstractAPIResult {
    /**
     * 返回信息
     */
    private ApiCustomerList result;

    public ApiCustomerList getResult() {
        return result;
    }

    public void setResult(ApiCustomerList result) {
        this.result = result;
    }

    public static class ApiCustomerList extends XBBPageResult {
        /**
         * 员工列表
         */
        private List<UserList> userList;

        public List<XBBApiUserListResult.UserList> getUserList() {
            return userList;
        }

        public void setUserList(List<XBBApiUserListResult.UserList> userList) {
            this.userList = userList;
        }
    }

    public static class UserList {
        /**
         * 员工钉钉id
         */
        private String userId;
        /**
         * 员工名
         */
        private String name;
        /**
         * 职位
         */
        private String position;
        /**
         * 头像
         */
        private String avatar;
        /**
         * 员工所在部门列表
         */
        private List<DepartmentList> departmentList;
        /**
         * 员工角色列表
         */
        private List<RoleList> roleList;

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPosition() {
            return position;
        }

        public void setPosition(String position) {
            this.position = position;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public List<XBBApiUserListResult.DepartmentList> getDepartmentList() {
            return departmentList;
        }

        public void setDepartmentList(List<XBBApiUserListResult.DepartmentList> departmentList) {
            this.departmentList = departmentList;
        }

        public List<XBBApiUserListResult.RoleList> getRoleList() {
            return roleList;
        }

        public void setRoleList(List<XBBApiUserListResult.RoleList> roleList) {
            this.roleList = roleList;
        }
    }

    public static class DepartmentList {
        /**
         * Id
         */
        private Integer id;
        /**
         * 名称
         */
        private String name;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public static class RoleList {
        /**
         * Id
         */
        private Integer id;
        /**
         * 名称
         */
        private String name;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
