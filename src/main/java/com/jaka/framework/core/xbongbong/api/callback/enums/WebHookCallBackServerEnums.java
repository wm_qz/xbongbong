package com.jaka.framework.core.xbongbong.api.callback.enums;

import com.jaka.framework.core.xbongbong.api.callback.center.BaseCenter;
import com.jaka.framework.core.xbongbong.api.callback.center.clue.ClueCenter;
import com.jaka.framework.core.xbongbong.api.callback.center.contact.ContactCenter;
import com.jaka.framework.core.xbongbong.api.callback.center.contract.ContractCenter;
import com.jaka.framework.core.xbongbong.api.callback.center.customer.CustomerCenter;
import com.jaka.framework.core.xbongbong.api.callback.center.customerCommunicate.CustomerCommunicateCenter;
import com.jaka.framework.core.xbongbong.api.callback.center.payment.PaymentCenter;
import com.jaka.framework.core.xbongbong.api.callback.center.paymentSheet.PaymentSheetCenter;
import com.jaka.framework.core.xbongbong.api.callback.center.product.ProductCenter;
import com.jaka.framework.core.xbongbong.api.callback.center.purchase.PurchaseCenter;
import com.jaka.framework.core.xbongbong.api.callback.center.quotation.QuotationCenter;
import com.jaka.framework.core.xbongbong.api.callback.center.sales.SalesCenter;
import com.jaka.framework.core.xbongbong.api.callback.center.system.SystemCenter;
import com.jaka.framework.core.xbongbong.api.callback.center.warehouse.WarehouseCenter;

import java.util.Objects;

/**
 * @author ：james.liu
 * @date ：Created in 2022/9/30 13:41
 * @description：可以执行的业务回调
 * @version: 1.0
 */
public enum WebHookCallBackServerEnums {
    /**
     * 客户
     */
    customer("customer", "客户", 1L, new CustomerCenter()),
    /**
     * 合同订单
     */
    contract("contract", "合同订单", 4232488L, new ContractCenter()),
    /**
     * 销售机会
     */
    sales_opportunity("salesOpportunity", "销售机会", 3L, new SalesCenter()),
    /**
     * 自定义
     */
    system("system", "自定义", 4L, new SystemCenter()),
    /**
     * 应收款
     */
    payment("payment", "应收款", 1680802L, new PaymentCenter()),
    /**
     * 回款单
     */
    payment_sheet("paymentSheet", "回款单", 1680811L, new PaymentSheetCenter()),
    /**
     * 产品
     */
    product("product", "产品", 7L, new ProductCenter()),
    /**
     * 销售线索
     */
    clue("clue", "销售线索", 8L, new ClueCenter()),
    /**
     * 跟进记录
     */
    customer_communicate("customerCommunicate", "跟进记录", 9L, new CustomerCommunicateCenter()),
    /**
     * 仓库
     */
    warehouse("warehouse", "仓库", 10L, new WarehouseCenter()),
    /**
     * 联系人
     */
    contact("contact", "联系人", 11L, new ContactCenter()),
    /**
     * 采购合同
     */
    purchase("purchase", "采购合同", 12L, new PurchaseCenter()),
    /**
     * 报价单
     */
    quotation("quotation", "报价单", 13L, new QuotationCenter()),
    ;

    WebHookCallBackServerEnums(String server, String desc, Long formId, BaseCenter classz) {
        this.server = server;
        this.desc = desc;
        this.formId = formId;
        this.classz = classz;
    }

    /**
     * 通过表单FormId 查询具体实现
     *
     * @param formId
     * @return
     */
    public static WebHookCallBackServerEnums get(Long formId) {
        if (Objects.isNull(formId)) {
            return null;
        }
        for (WebHookCallBackServerEnums value : values()) {
            if (Objects.equals(value.getFormId(), formId)) {
                return value;
            }
        }
        return null;
    }

    /**
     * 接口地址
     */
    private String server;
    /**
     * 描述
     */
    private String desc;
    /**
     * 对应数据的表单模板id
     */
    private Long formId;

    /**
     * Class<? extends BaseCenter> classz
     */
    private BaseCenter classz;

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Long getFormId() {
        return formId;
    }

    public void setFormId(Long formId) {
        this.formId = formId;
    }

    public BaseCenter getClassz() {
        return classz;
    }

    public void setClassz(BaseCenter classz) {
        this.classz = classz;
    }
}
