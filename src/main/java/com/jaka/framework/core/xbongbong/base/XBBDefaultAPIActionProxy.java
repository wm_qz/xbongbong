package com.jaka.framework.core.xbongbong.base;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jaka.framework.common.core.base.IHttpUtils;
import com.jaka.framework.common.core.exception.EmptyValueException;
import com.jaka.framework.common.core.exception.SignMsgFailedException;
import com.jaka.framework.common.core.exception.SignVerifyFailedException;
import com.jaka.framework.common.core.utils.HttpRequestUtils;
import com.jaka.framework.core.xbongbong.config.XBBAPIClientConfiguration;
import com.jaka.framework.core.xbongbong.util.SignUtils;
import okhttp3.RequestBody;
import org.apache.commons.lang3.StringUtils;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/22 11:39
 * @description：销帮帮代理
 * @version: 1.0
 */
public class XBBDefaultAPIActionProxy {

    private static final XBBDefaultAPIActionProxy instance = new XBBDefaultAPIActionProxy();


    private XBBDefaultAPIActionProxy() {

    }

    /**
     * API接口动作代理泛型方法
     *
     * @param input         入参基类
     * @param clz           出参基类
     * @param configuration 核心配置参数
     * @param httpUtils     http请求工具
     * @param signUtils     签名工具类
     * @param <I>           基类
     * @param <O>           基类
     * @param requestBody   request body
     * @param signData      签名
     * @return
     * @throws SignMsgFailedException    签名异常
     * @throws SignVerifyFailedException 签名验证异常
     * @throws EmptyValueException       数据Null或空
     * @throws Exception
     */
    public <I extends XBBAbstractAPIInput, O extends XBBAbstractAPIResult> O doAction(
            final I input,
            final Class<O> clz,
            final XBBAPIClientConfiguration configuration,
            final IHttpUtils httpUtils,
            final SignUtils signUtils,
            final RequestBody requestBody,
            final JSONObject signData
    ) throws SignMsgFailedException, SignVerifyFailedException, EmptyValueException, Exception {
        O r;
        String cmdId = input.getCmdId();
        //校验参数
        String sign = signUtils.encrypt(signData);
        input.checkParameter();
        if (StringUtils.isEmpty(sign)) {
            throw new EmptyValueException("xbb sign is null");
        }
        String apiUrl = configuration.getApiUrl() + cmdId;
        if (StringUtils.isEmpty(apiUrl)) {
            throw new EmptyValueException("销帮帮请求接口有误;");
        }
        final String responseBody = httpUtils.postStringResult(apiUrl, sign, requestBody);
        if (StringUtils.isEmpty(responseBody)) {
            throw new EmptyValueException("responseBody is null.");
        }
        String json = responseBody;
        r = JSON.parseObject(json, clz);
        return r;
    }


    /**
     * API接口动作代理泛型方法
     *
     * @param input         入参基类
     * @param clz           出参基类
     * @param configuration 核心配置参数
     * @param signUtils     签名工具类
     * @param <I>           基类
     * @param <O>           基类
     * @param inputJSON     JSONObjects
     * @return
     * @throws SignMsgFailedException    签名异常
     * @throws SignVerifyFailedException 签名验证异常
     * @throws EmptyValueException       数据Null或空
     * @throws Exception
     */
    public <I extends XBBAbstractAPIInput, O extends XBBAbstractAPIResult> O doAction(
            final I input,
            final Class<O> clz,
            final XBBAPIClientConfiguration configuration,
            final SignUtils signUtils,
            final JSONObject inputJSON
    ) throws SignMsgFailedException, SignVerifyFailedException, EmptyValueException, Exception {
        O r;
        String cmdId = input.getCmdId();
        //校验参数
        String sign = signUtils.encrypt(inputJSON);
        input.checkParameter();
        if (StringUtils.isEmpty(sign)) {
            throw new EmptyValueException("xbb sign is null");
        }
        String apiUrl = configuration.getApiUrl() + cmdId;
        if (StringUtils.isEmpty(apiUrl)) {
            throw new EmptyValueException("销帮帮请求接口有误!");
        }
        final String responseBody = HttpRequestUtils.post(apiUrl, JSON.toJSONString(inputJSON), sign);
        if (StringUtils.isEmpty(responseBody)) {
            throw new EmptyValueException("responseBody is null.");
        }
        String json = responseBody;
        r = JSON.parseObject(json, clz);
        return r;
    }

    public static XBBDefaultAPIActionProxy getInstance() {
        return instance;
    }


}
