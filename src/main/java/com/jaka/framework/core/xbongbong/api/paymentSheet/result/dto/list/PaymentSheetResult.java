package com.jaka.framework.core.xbongbong.api.paymentSheet.result.dto.list;

import java.util.List;

/**
 * @author ：james.liu
 * @date ：Created in 2022/12/1 9:59
 * @description：
 * @version: 1.0
 */
public class PaymentSheetResult {

    private List<PaymentSheetList> list;

    private int totalCount;

    private int totalPage;

    public List<PaymentSheetList> getList() {
        return list;
    }

    public void setList(List<PaymentSheetList> list) {
        this.list = list;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }
}
