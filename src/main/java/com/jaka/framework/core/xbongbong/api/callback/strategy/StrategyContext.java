package com.jaka.framework.core.xbongbong.api.callback.strategy;

import com.jaka.framework.core.xbongbong.api.callback.center.BaseCallBack;
import com.jaka.framework.core.xbongbong.api.callback.center.BaseCenter;
import com.jaka.framework.core.xbongbong.api.callback.model.WebHookCallBackInput;

/**
 * @author ：james.liu
 * @date ：Created in 2022/10/8 15:31
 * @description：负责和具体的策略类交互
 * @version: 1.0
 */
public class StrategyContext {

    /**
     * 回调接口策略接口
     */
    private BaseCenter baseCenter;

    /**
     * 构造方法
     *
     * @param baseCenter 执行器
     */
    public StrategyContext(BaseCenter baseCenter) {
        this.baseCenter = baseCenter;
    }

    /**
     * 具体执行方法
     *
     * @param webHookCallBackInput
     * @return
     */
    public void center(WebHookCallBackInput webHookCallBackInput, Class<? extends BaseCallBack> callback) {
        baseCenter.center(webHookCallBackInput, callback);
    }
}
