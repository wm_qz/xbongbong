package com.jaka.framework.core.xbongbong.api.crm.contacts.result;

import com.jaka.framework.core.xbongbong.base.XBBAbstractAPIResult;

import java.util.List;

/**
 * @author ：james.liu
 * @date ：Created in 2022/1/12 16:59
 * @description：联系人列表接口
 * @version: 1.0
 */
public class XBBProV2ApiContactListResult extends XBBAbstractAPIResult {

    /**
     * 返回信息
     */
    private RList result;

    public RList getResult() {
        return result;
    }

    public void setResult(RList result) {
        this.result = result;
    }

    public static class RList{

        private List<ResultList> list;

        /**
         * 总数据量
         */
        private Integer totalCount;

        /**
         * 总页码数
         */
        private Integer totalPage;

        public List<ResultList> getList() {
            return list;
        }

        public void setList(List<ResultList> list) {
            this.list = list;
        }

        public Integer getTotalCount() {
            return totalCount;
        }

        public void setTotalCount(Integer totalCount) {
            this.totalCount = totalCount;
        }

        public Integer getTotalPage() {
            return totalPage;
        }

        public void setTotalPage(Integer totalPage) {
            this.totalPage = totalPage;
        }
    }

    public static class ResultList {
        private long addTime;

        private ResultData data;

        private Long dataId;

        private Long formId;

        private Long updateTime;

        public long getAddTime() {
            return addTime;
        }

        public void setAddTime(long addTime) {
            this.addTime = addTime;
        }

        public ResultData getData() {
            return data;
        }

        public void setData(ResultData data) {
            this.data = data;
        }

        public Long getDataId() {
            return dataId;
        }

        public void setDataId(Long dataId) {
            this.dataId = dataId;
        }

        public Long getFormId() {
            return formId;
        }

        public void setFormId(Long formId) {
            this.formId = formId;
        }

        public Long getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(Long updateTime) {
            this.updateTime = updateTime;
        }
    }

    public static class ResultData {
        /**
         * 地址
         */
        private Address address_1;
        /**
         * 联系电话
         */
        private List<SubForm> subForm_1;
        /**
         * 生日
         */
        private String text_14;
        /**
         * 创建人
         */
        private String creatorId;
        /**
         * 决策关系
         */
        private String text_10;
        /**
         * 爱好
         */
        private String text_11;
        /**
         * 备注
         */
        private String text_12;
        /**
         * 姓名
         */
        private String text_1;
        /**
         * 关联客户
         */
        private String text_2;
        /**
         * 部门
         */
        private String text_3;
        /**
         * 职务
         */
        private String text_4;
        /**
         * 级别
         */
        private String text_5;
        /**
         * 邮箱
         */
        private String text_6;
        /**
         * QQ
         */
        private String text_7;
        /**
         * 邮编
         */
        private String text_8;
        /**
         * 性别
         */
        private String text_9;
        /**
         * 重要程度
         */
        private Integer num_1;
        private Integer num_2;
        /**
         * 是否主联系人
         */
        private Integer num_5;
        /**
         * 联系人职位
         */
        private String text_18;
        /**
         * 创建时间
         */
        private String addTime;
        /**
         * 更新时间
         */
        private String updateTime;

        public Integer getNum_5() {
            return num_5;
        }

        public void setNum_5(Integer num_5) {
            this.num_5 = num_5;
        }

        public String getText_18() {
            return text_18;
        }

        public void setText_18(String text_18) {
            this.text_18 = text_18;
        }

        public String getAddTime() {
            return addTime;
        }

        public void setAddTime(String addTime) {
            this.addTime = addTime;
        }

        public String getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(String updateTime) {
            this.updateTime = updateTime;
        }

        public Address getAddress_1() {
            return address_1;
        }

        public void setAddress_1(Address address_1) {
            this.address_1 = address_1;
        }

        public List<SubForm> getSubForm_1() {
            return subForm_1;
        }

        public void setSubForm_1(List<SubForm> subForm_1) {
            this.subForm_1 = subForm_1;
        }

        public String getText_14() {
            return text_14;
        }

        public void setText_14(String text_14) {
            this.text_14 = text_14;
        }

        public String getCreatorId() {
            return creatorId;
        }

        public void setCreatorId(String creatorId) {
            this.creatorId = creatorId;
        }

        public String getText_10() {
            return text_10;
        }

        public void setText_10(String text_10) {
            this.text_10 = text_10;
        }

        public String getText_11() {
            return text_11;
        }

        public void setText_11(String text_11) {
            this.text_11 = text_11;
        }

        public String getText_12() {
            return text_12;
        }

        public void setText_12(String text_12) {
            this.text_12 = text_12;
        }

        public String getText_1() {
            return text_1;
        }

        public void setText_1(String text_1) {
            this.text_1 = text_1;
        }

        public String getText_2() {
            return text_2;
        }

        public void setText_2(String text_2) {
            this.text_2 = text_2;
        }

        public String getText_3() {
            return text_3;
        }

        public void setText_3(String text_3) {
            this.text_3 = text_3;
        }

        public String getText_4() {
            return text_4;
        }

        public void setText_4(String text_4) {
            this.text_4 = text_4;
        }

        public String getText_5() {
            return text_5;
        }

        public void setText_5(String text_5) {
            this.text_5 = text_5;
        }

        public String getText_6() {
            return text_6;
        }

        public void setText_6(String text_6) {
            this.text_6 = text_6;
        }

        public String getText_7() {
            return text_7;
        }

        public void setText_7(String text_7) {
            this.text_7 = text_7;
        }

        public String getText_8() {
            return text_8;
        }

        public void setText_8(String text_8) {
            this.text_8 = text_8;
        }

        public String getText_9() {
            return text_9;
        }

        public void setText_9(String text_9) {
            this.text_9 = text_9;
        }

        public Integer getNum_1() {
            return num_1;
        }

        public void setNum_1(Integer num_1) {
            this.num_1 = num_1;
        }

        public Integer getNum_2() {
            return num_2;
        }

        public void setNum_2(Integer num_2) {
            this.num_2 = num_2;
        }
    }

    public static class SubForm {

        private String text_1;
        private String text_2;

        public String getText_1() {
            return text_1;
        }

        public void setText_1(String text_1) {
            this.text_1 = text_1;
        }

        public String getText_2() {
            return text_2;
        }

        public void setText_2(String text_2) {
            this.text_2 = text_2;
        }
    }

    public static class Address {
        private String address;
        private String province;
        private String city;
        private String district;
        private Location location;

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getProvince() {
            return province;
        }

        public void setProvince(String province) {
            this.province = province;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getDistrict() {
            return district;
        }

        public void setDistrict(String district) {
            this.district = district;
        }

        public Location getLocation() {
            return location;
        }

        public void setLocation(Location location) {
            this.location = location;
        }
    }

    public static class Location {
        private float lon;
        private float lat;
        public float getLon() {
            return lon;
        }

        public void setLon(float lon) {
            this.lon = lon;
        }

        public float getLat() {
            return lat;
        }

        public void setLat(float lat) {
            this.lat = lat;
        }
    }
}
