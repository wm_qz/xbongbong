package com.jaka.framework.core.xbongbong.api.paymentSheet.input;

import com.jaka.framework.core.xbongbong.base.XBBAbstractAPIPageInput;

/**
 * @author ：james.liu
 * @date ：Created in 2022/11/21 14:41
 * @description：
 * @version: 1.0
 */
public class ProV2ApiPaymentSheetDetailInput extends XBBAbstractAPIPageInput {

    /**
     * 表单ID
     */
    private Long dataId;

    public Long getDataId() {
        return dataId;
    }

    public void setDataId(Long dataId) {
        this.dataId = dataId;
    }
}
