package com.jaka.framework.core.xbongbong.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jaka.framework.common.core.utils.DigestUtil;
import com.jaka.framework.core.xbongbong.config.XBBAPIClientConfiguration;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/22 11:46
 * @description：签名工具类
 * @version: 1.0
 */
public class SignUtils {

    private final XBBAPIClientConfiguration configuration;

    public SignUtils(final XBBAPIClientConfiguration configuration) {
        this.configuration = configuration;
    }

    /**
     * @param data
     * @return
     */
    public String encrypt(final JSONObject data) {
        return getDataSign(data, configuration.getToken());
    }

    /**
     * 获取签名
     *
     * @param data  求参数(JSON格式)
     * @param token 令牌
     * @return
     */
    public static String getDataSign(JSONObject data, String token) {
        String dataSign = JSON.toJSONString(data) + token;
        return DigestUtil.encrypt(dataSign, "SHA-256");
    }


}
