package com.jaka.framework.core.xbongbong.api.crm;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jaka.framework.common.core.base.IHttpUtils;
import com.jaka.framework.core.xbongbong.api.crm.communicate.input.ProV2ApiCommunicateListInput;
import com.jaka.framework.core.xbongbong.api.crm.communicate.result.ProV2ApiCommunicateListResult;
import com.jaka.framework.core.xbongbong.api.crm.contacts.input.XBBProV2ApiContactListInput;
import com.jaka.framework.core.xbongbong.api.crm.contacts.result.XBBProV2ApiContactListResult;
import com.jaka.framework.core.xbongbong.api.crm.contract.input.XBBApiContractDetailInput;
import com.jaka.framework.core.xbongbong.api.crm.contract.input.XBBApiContractEditInput;
import com.jaka.framework.core.xbongbong.api.crm.contract.input.XBBApiContractListInput;
import com.jaka.framework.core.xbongbong.api.crm.contract.result.XBBApiContractDetailResult;
import com.jaka.framework.core.xbongbong.api.crm.contract.result.XBBApiContractEditResult;
import com.jaka.framework.core.xbongbong.api.crm.contract.result.XBBApiContractListResult;
import com.jaka.framework.core.xbongbong.api.crm.customer.input.*;
import com.jaka.framework.core.xbongbong.api.crm.customer.result.*;
import com.jaka.framework.core.xbongbong.api.crm.sales.input.ProV2ApiOpportunityListInput;
import com.jaka.framework.core.xbongbong.api.crm.sales.result.ProV2ApiOpportunityListResult;
import com.jaka.framework.core.xbongbong.base.XBBDefaultAPIActionProxy;
import com.jaka.framework.core.xbongbong.base.XBBRequestMappering;
import com.jaka.framework.core.xbongbong.config.XBBAPIClientConfiguration;
import com.jaka.framework.core.xbongbong.util.SignUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.Objects;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/22 16:52
 * @description：
 * @version: 1.0
 */
public class CRMClient {

    private final IHttpUtils httpUtils;
    private final XBBAPIClientConfiguration configuration;
    private final SignUtils signUtils;

    public CRMClient(final XBBAPIClientConfiguration configuration,
                     final IHttpUtils httpUtils,
                     final SignUtils signUtils) {
        this.configuration = configuration;
        this.httpUtils = httpUtils;
        this.signUtils = signUtils;
    }

    /**
     * 用户列表接口
     *
     * @param input 入参
     * @return
     * @throws Exception
     */
    public XBBApiCustomerListResult getAPICustomerList(final XBBApiCustomerListInput input) throws Exception {
        input.setCorpid(configuration.getCorpId());
        input.setUserId(configuration.getUserId());
        input.setCmdId(XBBRequestMappering.api_customer_list.getMapperName());
        JSONObject jsonObject = removeSignData(input.toJsonString());
        return XBBDefaultAPIActionProxy.getInstance().doAction(input,
                XBBApiCustomerListResult.class,
                configuration,
                httpUtils,
                signUtils,
                input.toRequestBody(jsonObject),
                jsonObject);
    }

    /**
     * 新建客户接口
     *
     * @param input 入参
     * @return
     * @throws Exception
     */
    public XBBApiCustomerAddResult apiCustomerAdd(final XBBApiCustomerAddInput input) throws Exception {
        input.setCorpid(configuration.getCorpId());
        input.setUserId(configuration.getUserId());
        input.setCmdId(XBBRequestMappering.api_customer_add.getMapperName());
        JSONObject jsonObject = removeSignData(input.toJsonString());
        return XBBDefaultAPIActionProxy.getInstance().doAction(input,
                XBBApiCustomerAddResult.class,
                configuration,
                httpUtils,
                signUtils,
                input.toRequestBody(jsonObject),
                jsonObject);
    }

    /**
     * 编辑客户接口
     *
     * @param input 入参
     * @return
     * @throws Exception
     */
    public XBBApiCustomerEditResult apiCustomerEdit(final XBBApiCustomerEditInput input) throws Exception {
        input.setCorpid(configuration.getCorpId());
        input.setUserId(configuration.getUserId());
        input.setCmdId(XBBRequestMappering.api_customer_edit.getMapperName());
        JSONObject jsonObject = removeSignData(input.toJsonString());
        return XBBDefaultAPIActionProxy.getInstance().doAction(input,
                XBBApiCustomerEditResult.class,
                configuration,
                httpUtils,
                signUtils,
                input.toRequestBody(jsonObject),
                jsonObject);
    }

    /**
     * 客户详情接口
     *
     * @param input
     * @return
     * @throws Exception
     */
    public XBBApiCustomerDetailResult apiCustomerDetail(final XBBApiCustomerDetailInput input) throws Exception {
        input.setCorpid(configuration.getCorpId());
        input.setUserId(configuration.getUserId());
        input.setCmdId(XBBRequestMappering.api_customer_detail.getMapperName());
        JSONObject jsonObject = removeSignData(input.toJsonString());
        return XBBDefaultAPIActionProxy.getInstance().doAction(input,
                XBBApiCustomerDetailResult.class,
                configuration,
                httpUtils,
                signUtils,
                input.toRequestBody(jsonObject),
                jsonObject);
    }

    /**
     * 删除客户接口
     *
     * @param input
     * @return
     * @throws Exception
     */
    public XBBApiCustomerDelResult apiCustomerDel(final XBBApiCustomerDelInput input) throws Exception {
        input.setCorpid(configuration.getCorpId());
        input.setUserId(configuration.getUserId());
        input.setCmdId(XBBRequestMappering.api_customer_del.getMapperName());
        JSONObject jsonObject = removeSignData(input.toJsonString());
        return XBBDefaultAPIActionProxy.getInstance().doAction(input,
                XBBApiCustomerDelResult.class,
                configuration,
                httpUtils,
                signUtils,
                input.toRequestBody(jsonObject),
                jsonObject);
    }

    /**
     * 客户添加协同人接口
     *
     * @param input
     * @return
     * @throws Exception
     */
    public XBBApiCustomerAddCoUserResult apiCustomerAddCoUser(final XBBApiCustomerAddCoUserInput input) throws Exception {
        input.setCorpid(configuration.getCorpId());
        input.setUserId(configuration.getUserId());
        input.setCmdId(XBBRequestMappering.api_customer_add_co_user.getMapperName());
        JSONObject jsonObject = removeSignData(input.toJsonString());
        return XBBDefaultAPIActionProxy.getInstance().doAction(input,
                XBBApiCustomerAddCoUserResult.class,
                configuration,
                httpUtils,
                signUtils,
                input.toRequestBody(jsonObject),
                jsonObject);
    }

    /**
     * 客户删除协同人接口
     *
     * @param input
     * @return
     * @throws Exception
     */
    public XBBApiCustomerDeleteCoUserResult apiCustomerDeleteCoUser(final XBBApiCustomerDeleteCoUserInput input) throws Exception {
        input.setCorpid(configuration.getCorpId());
        input.setUserId(configuration.getUserId());
        input.setCmdId(XBBRequestMappering.api_customer_delete_co_user.getMapperName());
        JSONObject jsonObject = removeSignData(input.toJsonString());
        return XBBDefaultAPIActionProxy.getInstance().doAction(input,
                XBBApiCustomerDeleteCoUserResult.class,
                configuration,
                httpUtils,
                signUtils,
                input.toRequestBody(jsonObject),
                jsonObject);
    }

    /**
     * 联系人列表接口
     *
     * @param input
     * @return
     * @throws Exception
     */
    public XBBProV2ApiContactListResult proV2ApiContactList(final XBBProV2ApiContactListInput input) throws Exception {
        input.setCorpid(configuration.getCorpId());
        input.setUserId(configuration.getUserId());
        input.setCmdId(XBBRequestMappering.pro_v2_api_contact_list.getMapperName());
        JSONObject jsonObject = removeSignData(input.toJsonString());
        return XBBDefaultAPIActionProxy.getInstance().doAction(input,
                XBBProV2ApiContactListResult.class,
                configuration,
                httpUtils,
                signUtils,
                input.toRequestBody(jsonObject),
                jsonObject);
    }

    /**
     * 销售机会列表接口
     *
     * @param input
     * @return
     * @throws Exception
     */
    public ProV2ApiOpportunityListResult proV2ApiOpportunityList(final ProV2ApiOpportunityListInput input) throws Exception {
        input.setCorpid(configuration.getCorpId());
        input.setUserId(configuration.getUserId());
        input.setCmdId(XBBRequestMappering.pro_v2_api_opportunity_list.getMapperName());
        JSONObject jsonObject = removeSignData(input.toJsonString());
        return XBBDefaultAPIActionProxy.getInstance().doAction(input,
                ProV2ApiOpportunityListResult.class,
                configuration,
                httpUtils,
                signUtils,
                input.toRequestBody(jsonObject),
                jsonObject);
    }

    /**
     * 跟进记录列表接口
     *
     * @param input
     * @return
     * @throws Exception
     */
    public ProV2ApiCommunicateListResult proV2ApiCommunicateList(final ProV2ApiCommunicateListInput input) throws Exception {
        input.setCorpid(configuration.getCorpId());
        input.setUserId(configuration.getUserId());
        input.setCmdId(XBBRequestMappering.pro_v2_api_communicate_list.getMapperName());
        JSONObject jsonObject = removeSignData(input.toJsonString());
        return XBBDefaultAPIActionProxy.getInstance().doAction(input,
                ProV2ApiCommunicateListResult.class,
                configuration,
                httpUtils,
                signUtils,
                input.toRequestBody(jsonObject),
                jsonObject);
    }


    // ---------------------------------  合同订单模块接口 start

    /**
     * 合同订单列表
     *
     * @param input
     * @return
     * @throws Exception
     */
    public XBBApiContractListResult apiContractList(final XBBApiContractListInput input) throws Exception {
        input.setCorpid(configuration.getCorpId());
        input.setUserId(configuration.getUserId());
        input.setCmdId(XBBRequestMappering.pro_v2_api_contract_list.getMapperName());
        JSONObject jsonObject = removeSignData(input.toJsonString());
        return XBBDefaultAPIActionProxy.getInstance().doAction(input,
                XBBApiContractListResult.class,
                configuration,
                httpUtils,
                signUtils,
                input.toRequestBody(jsonObject),
                jsonObject);
    }

    /**
     * 修改合同详情
     *
     * @param input
     * @return
     * @throws Exception
     */
    public XBBApiContractEditResult apiContractEdit(final XBBApiContractEditInput input) throws Exception {
        input.setCorpid(configuration.getCorpId());
        input.setUserId(configuration.getUserId());
        input.setCmdId(XBBRequestMappering.pro_v2_api_contract_edit.getMapperName());
        JSONObject jsonObject = removeSignData(input.toJsonString());
        jsonObject = removeEmpty(jsonObject);
        return XBBDefaultAPIActionProxy.getInstance().doAction(input,
                XBBApiContractEditResult.class,
                configuration,
                httpUtils,
                signUtils,
                input.toRequestBody(jsonObject),
                jsonObject);
    }

    /**
     * 合同订单详情
     *
     * @param input
     * @return
     * @throws Exception
     */
    public XBBApiContractDetailResult apiContractDetail(final XBBApiContractDetailInput input) throws Exception {
        input.setCorpid(configuration.getCorpId());
        input.setUserId(configuration.getUserId());
        input.setCmdId(XBBRequestMappering.pro_v2_api_contract_detail.getMapperName());
        JSONObject jsonObject = removeSignData(input.toJsonString());
        return XBBDefaultAPIActionProxy.getInstance().doAction(input,
                XBBApiContractDetailResult.class,
                configuration,
                httpUtils,
                signUtils,
                input.toRequestBody(jsonObject),
                jsonObject);
    }


    public final JSONObject removeSignData(String data) {
        JSONObject signData = JSON.parseObject(data);
        if (signData.containsKey("cmdId")) {
            signData.remove("cmdId");
        }
        if (signData.containsKey("signUtils")) {
            signData.remove("signUtils");
        }
        return (signData.isEmpty()) ? null : signData;
    }


    public static JSONObject removeEmpty(JSONObject json) {
        JSONObject result = new JSONObject();
        for (String key : json.keySet()) {
            Object value = json.get(key);
            if (!isLeaf(value)) {
                value = removeEmpty((JSONObject) value);
            }
            if (isNotEmpty(value)) {
                result.put(key, value);
            }
        }
        return isNotEmpty(result) ? result : null;
    }


    private static boolean isLeaf(Object value) {
        return !(value instanceof JSON) || isEmpty(value);
    }

    private static boolean isNotEmpty(Object value) {
        return !isEmpty(value) && !Objects.equals(value, 0);
    }

    private static boolean isEmpty(Object value) {
        if (value == null) {
            return true;
        }
        if (value instanceof String) {
            return StringUtils.isBlank((String) value) || "null".equalsIgnoreCase((String) value);
        }
        if (value instanceof JSONObject) {
            return ((JSONObject) value).size() <= 0;
        }
        if (value instanceof JSONArray) {
            return ((JSONArray) value).size() <= 0;
        }
        return false;
    }

}
