/**
 * Copyright 2022 bejson.com
 */
package com.jaka.framework.core.xbongbong.api.contractOutstock.result.dto.detail;

import java.util.List;

/**
 * Auto-generated: 2022-12-05 10:27:27
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class ContractOutStockDetailData {

    /**
     * 终端客户来源
     */
    private String text_18;
    /**
     * 终端客户行业
     */
    private String text_17;
    /**
     * 销售订单编号
     */
    private String text_19;
    /**
     * 出货地址
     */
    private ContractOutStockDetailAddress_2 address_2;
    /**
     * 所属部门
     */
    private ContractOutStockDetailDepartmentId departmentId;
    /**
     * 合同签订人
     */
    private ContractOutStockDetailText_14 text_14;
    /**
     * 创建人
     */
    private ContractOutStockDetailCreatorId creatorId;
    /**
     * 签约客户性质
     */
    private String text_16;
    /**
     * 签约客户来源
     */
    private String text_15;
    /**
     * 整单折扣率
     */
    private Integer num_61;
    /**
     * 负责人
     */
    private List<ContractOutStockDetailOwnerId> ownerId;
    /**
     * 到期日期
     */
    private Long num_40;
    /**
     * 优惠金额
     */
    private Integer num_62;
    /**
     * 出库日期
     */
    private Long date_1;
    /**
     * 回单/验收单收回日期
     */
    private Long date_2;
    /**
     * 签订日期
     */
    private Long date_3;
    /**
     * 客户电话
     */
    private List<ContractOutStockDetailSubForm_3> subForm_3;
    /**
     * 电话
     */
    private ContractOutStockDetailText_2 text_2;
    /**
     * 关联单据
     */
    private ContractOutStockDetailText_4 text_4;
    /**
     * 物流单号
     */
    private String text_8;
    /**
     * 验收单情况
     */
    private ContractOutStockDetailText_9 text_9;
    /**
     * 出库单回签情况
     */
    private ContractOutStockDetailText_11 text_11;
    /**
     * 签收人
     */
    private String text_29;
    /**
     * 是否关联特殊价格
     */
    private String text_28;
    /**
     * 出库金额
     */
    private Long num_38;
    /**
     * 质量保证期（单位：年）
     */
    private String text_1;
    /**
     * 其它费用
     */
    private Integer num_39;
    /**
     * 是否新老客户
     */
    private String text_25;
    /**
     * 出货产品类型
     */
    private String text_24;
    /**
     * 应用类型
     */
    private String text_21;
    /**
     * 合同模版
     */
    private String text_23;
    /**
     * 签约客户行业
     */
    private String text_22;
    /**
     * 客户名称
     */
    private ContractOutStockDetailText_66 text_66;
    /**
     * 出库单编号
     */
    private String serialNo;
    /**
     * 出库产品
     */
    private List<ContractOutStockDetailArray_1> array_1;
    /**
     * 成本小计
     */
    private Integer num_7;
    /**
     * 出库次数
     */
    private Integer num_2;
    /**
     * 合作次数
     */
    private Integer num_3;
    /**
     * 折扣（%）
     */
    private Integer num_4;


    public String getText_18() {
        return text_18;
    }

    public void setText_18(String text_18) {
        this.text_18 = text_18;
    }

    public String getText_17() {
        return text_17;
    }

    public void setText_17(String text_17) {
        this.text_17 = text_17;
    }

    public String getText_19() {
        return text_19;
    }

    public void setText_19(String text_19) {
        this.text_19 = text_19;
    }

    public ContractOutStockDetailAddress_2 getAddress_2() {
        return address_2;
    }

    public void setAddress_2(ContractOutStockDetailAddress_2 address_2) {
        this.address_2 = address_2;
    }

    public ContractOutStockDetailDepartmentId getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(ContractOutStockDetailDepartmentId departmentId) {
        this.departmentId = departmentId;
    }

    public ContractOutStockDetailText_14 getText_14() {
        return text_14;
    }

    public void setText_14(ContractOutStockDetailText_14 text_14) {
        this.text_14 = text_14;
    }

    public ContractOutStockDetailCreatorId getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(ContractOutStockDetailCreatorId creatorId) {
        this.creatorId = creatorId;
    }

    public String getText_16() {
        return text_16;
    }

    public void setText_16(String text_16) {
        this.text_16 = text_16;
    }

    public String getText_15() {
        return text_15;
    }

    public void setText_15(String text_15) {
        this.text_15 = text_15;
    }

    public Integer getNum_61() {
        return num_61;
    }

    public void setNum_61(Integer num_61) {
        this.num_61 = num_61;
    }

    public List<ContractOutStockDetailOwnerId> getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(List<ContractOutStockDetailOwnerId> ownerId) {
        this.ownerId = ownerId;
    }

    public Long getNum_40() {
        return num_40;
    }

    public void setNum_40(Long num_40) {
        this.num_40 = num_40;
    }

    public Integer getNum_62() {
        return num_62;
    }

    public void setNum_62(Integer num_62) {
        this.num_62 = num_62;
    }

    public Long getDate_1() {
        return date_1;
    }

    public void setDate_1(Long date_1) {
        this.date_1 = date_1;
    }

    public Long getDate_3() {
        return date_3;
    }

    public void setDate_3(Long date_3) {
        this.date_3 = date_3;
    }

    public List<ContractOutStockDetailSubForm_3> getSubForm_3() {
        return subForm_3;
    }

    public void setSubForm_3(List<ContractOutStockDetailSubForm_3> subForm_3) {
        this.subForm_3 = subForm_3;
    }

    public ContractOutStockDetailText_2 getText_2() {
        return text_2;
    }

    public void setText_2(ContractOutStockDetailText_2 text_2) {
        this.text_2 = text_2;
    }

    public ContractOutStockDetailText_4 getText_4() {
        return text_4;
    }

    public void setText_4(ContractOutStockDetailText_4 text_4) {
        this.text_4 = text_4;
    }

    public String getText_8() {
        return text_8;
    }

    public void setText_8(String text_8) {
        this.text_8 = text_8;
    }

    public String getText_28() {
        return text_28;
    }

    public void setText_28(String text_28) {
        this.text_28 = text_28;
    }

    public Long getNum_38() {
        return num_38;
    }

    public void setNum_38(Long num_38) {
        this.num_38 = num_38;
    }

    public String getText_1() {
        return text_1;
    }

    public void setText_1(String text_1) {
        this.text_1 = text_1;
    }

    public Integer getNum_39() {
        return num_39;
    }

    public void setNum_39(Integer num_39) {
        this.num_39 = num_39;
    }

    public String getText_25() {
        return text_25;
    }

    public void setText_25(String text_25) {
        this.text_25 = text_25;
    }

    public String getText_24() {
        return text_24;
    }

    public void setText_24(String text_24) {
        this.text_24 = text_24;
    }

    public String getText_21() {
        return text_21;
    }

    public void setText_21(String text_21) {
        this.text_21 = text_21;
    }

    public String getText_23() {
        return text_23;
    }

    public void setText_23(String text_23) {
        this.text_23 = text_23;
    }

    public String getText_22() {
        return text_22;
    }

    public void setText_22(String text_22) {
        this.text_22 = text_22;
    }

    public ContractOutStockDetailText_66 getText_66() {
        return text_66;
    }

    public void setText_66(ContractOutStockDetailText_66 text_66) {
        this.text_66 = text_66;
    }

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public List<ContractOutStockDetailArray_1> getArray_1() {
        return array_1;
    }

    public void setArray_1(List<ContractOutStockDetailArray_1> array_1) {
        this.array_1 = array_1;
    }

    public Integer getNum_7() {
        return num_7;
    }

    public void setNum_7(Integer num_7) {
        this.num_7 = num_7;
    }

    public Integer getNum_2() {
        return num_2;
    }

    public void setNum_2(Integer num_2) {
        this.num_2 = num_2;
    }

    public Integer getNum_3() {
        return num_3;
    }

    public void setNum_3(Integer num_3) {
        this.num_3 = num_3;
    }

    public Integer getNum_4() {
        return num_4;
    }

    public void setNum_4(Integer num_4) {
        this.num_4 = num_4;
    }

    public Long getDate_2() {
        return date_2;
    }

    public void setDate_2(Long date_2) {
        this.date_2 = date_2;
    }

    public ContractOutStockDetailText_9 getText_9() {
        return text_9;
    }

    public void setText_9(ContractOutStockDetailText_9 text_9) {
        this.text_9 = text_9;
    }

    public ContractOutStockDetailText_11 getText_11() {
        return text_11;
    }

    public void setText_11(ContractOutStockDetailText_11 text_11) {
        this.text_11 = text_11;
    }

    public String getText_29() {
        return text_29;
    }

    public void setText_29(String text_29) {
        this.text_29 = text_29;
    }
}