package com.jaka.framework.core.xbongbong.api.crm.sales.result;

import com.jaka.framework.core.xbongbong.base.XBBAbstractAPIResult;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author ：james.liu
 * @date ：Created in 2022/1/12 17:23
 * @description：销售机会列表接口
 * @version: 1.0
 */
@SuppressWarnings("all")
public class ProV2ApiOpportunityListResult extends XBBAbstractAPIResult {
    /**
     * 返回信息
     */
    private RList result;

    public RList getResult() {
        return result;
    }

    public void setResult(RList result) {
        this.result = result;
    }

    public static class RList {

        private List<ResiltList> list;

        /**
         * 总数据量
         */
        private Integer totalCount;

        /**
         * 总页码数
         */
        private Integer totalPage;

        public List<ResiltList> getList() {
            return list;
        }

        public void setList(List<ResiltList> list) {
            this.list = list;
        }

        public Integer getTotalCount() {
            return totalCount;
        }

        public void setTotalCount(Integer totalCount) {
            this.totalCount = totalCount;
        }

        public Integer getTotalPage() {
            return totalPage;
        }

        public void setTotalPage(Integer totalPage) {
            this.totalPage = totalPage;
        }
    }

    public static class ResiltList {
        private Long addTime;
        private Data data;
        private Long dataId;
        private Long formId;
        private Long updateTime;

        public Long getAddTime() {
            return addTime;
        }

        public void setAddTime(Long addTime) {
            this.addTime = addTime;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

        public Long getDataId() {
            return dataId;
        }

        public void setDataId(Long dataId) {
            this.dataId = dataId;
        }

        public Long getFormId() {
            return formId;
        }

        public void setFormId(Long formId) {
            this.formId = formId;
        }

        public Long getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(Long updateTime) {
            this.updateTime = updateTime;
        }
    }

    public static class Data {
        /**
         * 创建人
         */
        private String creatorId;
        /**
         * 最后跟进时间
         */
        private Integer date_2;
        /**
         * 预计结束时间
         */
        private Integer date_1;
        /**
         * 项目名称
         */
        private String text_1;
        /**
         * 规格型号
         */
        private String text_2;
        /**
         * 客户名称
         */
        private String text_3;
        /**
         *
         */
        private String text_4;
        /**
         *
         */
        private String text_5;
        /**
         * 终端用户
         */
        private String text_6;
        /**
         * 终端用户现有负责人
         */
        private String text_7;
        /**
         * 竞争对手
         */
        private String text_9;
        /**
         * 终端用户（手填）
         */
        private String text_10;
        /**
         * 是否可以选择终端用户
         */
        private String text_11;
        /**
         * 销售阶段
         */
        private String text_17;
        /**
         * 决策人
         */
        private List<Integer> text_8;
        /**
         * 关联产品
         */
        private List<Integer> array_1;
        /**
         * 成功率
         */
        private String array_2;
        /**
         * 项目编号
         */
        private String serialNo;
        /**
         * 预计金额
         */
        private BigDecimal num_1;
        /**
         * 机会成本
         */
        private BigDecimal num_2;
        /**
         * 机会毛利
         */
        private BigDecimal num_3;
        /**
         * 机会毛利率
         */
        private BigDecimal num_4;
        /**
         * 汇率
         */
        private BigDecimal num_5;
        /**
         *
         */
        private Integer num_6;
        /**
         * 重要程度
         */
        private BigDecimal num_7;
        /**
         * 是否归档
         */
        private BigDecimal num_8;
        /**
         * 预计数量
         */
        private BigDecimal num_9;
        /**
         * 客户预算
         */
        private BigDecimal num_10;
        /**
         * 负责人
         */
        private String ownerId;
        /**
         *
         */
        private Integer addTime;
        /**
         *
         */
        private Integer updateTime;
        /**
         * 协同人
         */
        private String coUserId;

        public String getCreatorId() {
            return creatorId;
        }

        public void setCreatorId(String creatorId) {
            this.creatorId = creatorId;
        }

        public Integer getDate_2() {
            return date_2;
        }

        public void setDate_2(Integer date_2) {
            this.date_2 = date_2;
        }

        public Integer getDate_1() {
            return date_1;
        }

        public void setDate_1(Integer date_1) {
            this.date_1 = date_1;
        }

        public String getText_1() {
            return text_1;
        }

        public void setText_1(String text_1) {
            this.text_1 = text_1;
        }

        public String getText_2() {
            return text_2;
        }

        public void setText_2(String text_2) {
            this.text_2 = text_2;
        }

        public String getText_3() {
            return text_3;
        }

        public void setText_3(String text_3) {
            this.text_3 = text_3;
        }

        public String getText_4() {
            return text_4;
        }

        public void setText_4(String text_4) {
            this.text_4 = text_4;
        }

        public String getText_5() {
            return text_5;
        }

        public void setText_5(String text_5) {
            this.text_5 = text_5;
        }

        public String getText_6() {
            return text_6;
        }

        public void setText_6(String text_6) {
            this.text_6 = text_6;
        }

        public String getText_7() {
            return text_7;
        }

        public void setText_7(String text_7) {
            this.text_7 = text_7;
        }

        public List<Integer> getText_8() {
            return text_8;
        }

        public void setText_8(List<Integer> text_8) {
            this.text_8 = text_8;
        }

        public String getText_9() {
            return text_9;
        }

        public void setText_9(String text_9) {
            this.text_9 = text_9;
        }

        public List<Integer> getArray_1() {
            return array_1;
        }

        public void setArray_1(List<Integer> array_1) {
            this.array_1 = array_1;
        }

        public String getSerialNo() {
            return serialNo;
        }

        public void setSerialNo(String serialNo) {
            this.serialNo = serialNo;
        }

        public BigDecimal getNum_1() {
            return num_1;
        }

        public void setNum_1(BigDecimal num_1) {
            this.num_1 = num_1;
        }

        public BigDecimal getNum_2() {
            return num_2;
        }

        public void setNum_2(BigDecimal num_2) {
            this.num_2 = num_2;
        }

        public BigDecimal getNum_3() {
            return num_3;
        }

        public void setNum_3(BigDecimal num_3) {
            this.num_3 = num_3;
        }

        public BigDecimal getNum_4() {
            return num_4;
        }

        public void setNum_4(BigDecimal num_4) {
            this.num_4 = num_4;
        }

        public BigDecimal getNum_5() {
            return num_5;
        }

        public void setNum_5(BigDecimal num_5) {
            this.num_5 = num_5;
        }

        public Integer getNum_6() {
            return num_6;
        }

        public void setNum_6(Integer num_6) {
            this.num_6 = num_6;
        }

        public BigDecimal getNum_7() {
            return num_7;
        }

        public void setNum_7(BigDecimal num_7) {
            this.num_7 = num_7;
        }

        public String getText_10() {
            return text_10;
        }

        public void setText_10(String text_10) {
            this.text_10 = text_10;
        }

        public String getText_11() {
            return text_11;
        }

        public void setText_11(String text_11) {
            this.text_11 = text_11;
        }

        public String getText_17() {
            return text_17;
        }

        public void setText_17(String text_17) {
            this.text_17 = text_17;
        }

        public String getArray_2() {
            return array_2;
        }

        public void setArray_2(String array_2) {
            this.array_2 = array_2;
        }

        public BigDecimal getNum_8() {
            return num_8;
        }

        public void setNum_8(BigDecimal num_8) {
            this.num_8 = num_8;
        }

        public BigDecimal getNum_9() {
            return num_9;
        }

        public void setNum_9(BigDecimal num_9) {
            this.num_9 = num_9;
        }

        public BigDecimal getNum_10() {
            return num_10;
        }

        public void setNum_10(BigDecimal num_10) {
            this.num_10 = num_10;
        }

        public String getOwnerId() {
            return ownerId;
        }

        public void setOwnerId(String ownerId) {
            this.ownerId = ownerId;
        }

        public Integer getAddTime() {
            return addTime;
        }

        public void setAddTime(Integer addTime) {
            this.addTime = addTime;
        }

        public Integer getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(Integer updateTime) {
            this.updateTime = updateTime;
        }

        public String getCoUserId() {
            return coUserId;
        }

        public void setCoUserId(String coUserId) {
            this.coUserId = coUserId;
        }
    }

}
