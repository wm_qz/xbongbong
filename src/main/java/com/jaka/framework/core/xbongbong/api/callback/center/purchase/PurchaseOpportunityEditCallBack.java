package com.jaka.framework.core.xbongbong.api.callback.center.purchase;

/**
 * @author ：james.liu
 * @date ：Created in 2022/9/30 14:09
 * @description：合同订单编辑
 * @version: 1.0
 */
public interface PurchaseOpportunityEditCallBack {

    /**
     * 添加用户回调
     */
    default void callback() {

    }
}
