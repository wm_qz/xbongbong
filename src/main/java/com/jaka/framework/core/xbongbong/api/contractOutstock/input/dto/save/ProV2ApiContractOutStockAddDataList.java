package com.jaka.framework.core.xbongbong.api.contractOutstock.input.dto.save;

import java.util.List;

/**
 * @author ：james.liu
 * @date ：Created in 2022/12/2 14:06
 * @description：
 * @version: 1.0
 */
public class ProV2ApiContractOutStockAddDataList {

    /**
     * 出库单编号
     */
    private String serialNo;
    /**
     * 出库日期
     */
    private Integer date_1;
    /**
     * 签订日期
     */
    private Integer date_3;
    /**
     * 客户名称
     */
    private String text_66;
    /**
     * 关联单据
     */
    private Integer text_4;

    private Integer text_2;
    /**
     * 销售订单编号
     */
    private String text_19;
    /**
     * 出库次数
     */
    private Integer num_2;
    /**
     * 合作次数
     */
    private Integer num_3;
    /**
     * 负责人
     */
    private List<String> ownerId;
    /**
     * 所属部门
     */
    private String departmentId;
    /**
     * 是否新老客户
     */
    private Integer text_25;
    /**
     * 收全款日期
     */
    private Integer date_4;
    /**
     *合同模版
     */
    private String text_23;
    /**
     * 签约客户来源
     */
    private String text_15;
    /**
     * 签约客户行业
     */
    private String text_22;
    /**
     * 签约客户性质
     */
    private String text_16;
    /**
     * 终端客户来源
     */
    private String text_18;
    /**
     * 终端客户行业
     */
    private String text_17;
    /**
     * 终端客户行业
     */
    private String text_21;
    /**
     * 质量保证期（单位：年）
     */
    private Integer text_1;
    /**
     * 合同签订人
     */
    private String text_14;
    /**
     * 是否关联特殊价格
     */
    private String text_28;
    /**
     * 关联特殊报价审批
     */
    private String text_27;
    /**
     * 物流公司
     */
    private String text_7;
    /**
     * 物流单号
     */
    private String text_8;
    /**
     * 备注
     */
    private String text_6;
    /**
     * 出货产品类型
     */
    private String text_24;
    /**
     * 产品售价合计
     */
    private Integer num_40;
    /**
     * 其它费用
     */
    private Integer num_39;
    /**
     * 出库金额
     */
    private Integer num_38;
    /**
     * 出库数量
     */
    private Integer num_4;
    /**
     * 承诺最低业绩
     */
    private Integer num_5;
    /**
     * 已提货数量
     */
    private Integer num_6;
    /**
     * 承诺完成率（%）
     */
    private Integer num_7;
    /**
     * 验收单情况
     */
    private String text_9;
    /**
     * 出库单回签情况
     */
    private String text_11;
    /**
     * 回单/验收单收回日期
     */
    private Integer date_2;
    /**
     * 开票情况
     */
    private String text_13;
    /**
     * 退换货备注
     */
    private String text_5;
    /**
     *创建人
     */
    private String creatorId;
    /**
     * 创建时间
     */
    private Integer addTime;
    /**
     * 更新时间
     */
    private Integer updateTime;
    /**
     * 整单折扣率
     */
    private String num_61;
    /**
     * 优惠金额
     */
    private String num_62;
    /**
     * 是否出过库
     */
    private Integer num_1;
    /**
     * 出库产品
     */
    private List<ProV2ApiContractOutStockAddArray_1> array_1;
    /**
     * 客户电话
     */
    private List<ProV2ApiContractOutStockSubForm_1> subForm_3;
    /**
     * 出货地址
     */
    private ProV2ApiContractOutStockAddAddress_2 address_2;

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public Integer getDate_1() {
        return date_1;
    }

    public void setDate_1(Integer date_1) {
        this.date_1 = date_1;
    }

    public Integer getDate_3() {
        return date_3;
    }

    public void setDate_3(Integer date_3) {
        this.date_3 = date_3;
    }

    public String getText_66() {
        return text_66;
    }

    public void setText_66(String text_66) {
        this.text_66 = text_66;
    }

    public Integer getText_4() {
        return text_4;
    }

    public void setText_4(Integer text_4) {
        this.text_4 = text_4;
    }

    public Integer getText_2() {
        return text_2;
    }

    public void setText_2(Integer text_2) {
        this.text_2 = text_2;
    }

    public String getText_19() {
        return text_19;
    }

    public void setText_19(String text_19) {
        this.text_19 = text_19;
    }

    public Integer getNum_2() {
        return num_2;
    }

    public void setNum_2(Integer num_2) {
        this.num_2 = num_2;
    }

    public Integer getNum_3() {
        return num_3;
    }

    public void setNum_3(Integer num_3) {
        this.num_3 = num_3;
    }

    public List<String> getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(List<String> ownerId) {
        this.ownerId = ownerId;
    }

    public String getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(String departmentId) {
        this.departmentId = departmentId;
    }

    public Integer getText_25() {
        return text_25;
    }

    public void setText_25(Integer text_25) {
        this.text_25 = text_25;
    }

    public Integer getDate_4() {
        return date_4;
    }

    public void setDate_4(Integer date_4) {
        this.date_4 = date_4;
    }

    public String getText_23() {
        return text_23;
    }

    public void setText_23(String text_23) {
        this.text_23 = text_23;
    }

    public String getText_15() {
        return text_15;
    }

    public void setText_15(String text_15) {
        this.text_15 = text_15;
    }

    public String getText_22() {
        return text_22;
    }

    public void setText_22(String text_22) {
        this.text_22 = text_22;
    }

    public String getText_16() {
        return text_16;
    }

    public void setText_16(String text_16) {
        this.text_16 = text_16;
    }

    public String getText_18() {
        return text_18;
    }

    public void setText_18(String text_18) {
        this.text_18 = text_18;
    }

    public String getText_17() {
        return text_17;
    }

    public void setText_17(String text_17) {
        this.text_17 = text_17;
    }

    public String getText_21() {
        return text_21;
    }

    public void setText_21(String text_21) {
        this.text_21 = text_21;
    }

    public Integer getText_1() {
        return text_1;
    }

    public void setText_1(Integer text_1) {
        this.text_1 = text_1;
    }

    public String getText_14() {
        return text_14;
    }

    public void setText_14(String text_14) {
        this.text_14 = text_14;
    }

    public String getText_28() {
        return text_28;
    }

    public void setText_28(String text_28) {
        this.text_28 = text_28;
    }

    public String getText_27() {
        return text_27;
    }

    public void setText_27(String text_27) {
        this.text_27 = text_27;
    }

    public String getText_7() {
        return text_7;
    }

    public void setText_7(String text_7) {
        this.text_7 = text_7;
    }

    public String getText_8() {
        return text_8;
    }

    public void setText_8(String text_8) {
        this.text_8 = text_8;
    }

    public String getText_6() {
        return text_6;
    }

    public void setText_6(String text_6) {
        this.text_6 = text_6;
    }

    public String getText_24() {
        return text_24;
    }

    public void setText_24(String text_24) {
        this.text_24 = text_24;
    }

    public Integer getNum_40() {
        return num_40;
    }

    public void setNum_40(Integer num_40) {
        this.num_40 = num_40;
    }

    public Integer getNum_39() {
        return num_39;
    }

    public void setNum_39(Integer num_39) {
        this.num_39 = num_39;
    }

    public Integer getNum_38() {
        return num_38;
    }

    public void setNum_38(Integer num_38) {
        this.num_38 = num_38;
    }

    public Integer getNum_4() {
        return num_4;
    }

    public void setNum_4(Integer num_4) {
        this.num_4 = num_4;
    }

    public Integer getNum_5() {
        return num_5;
    }

    public void setNum_5(Integer num_5) {
        this.num_5 = num_5;
    }

    public Integer getNum_6() {
        return num_6;
    }

    public void setNum_6(Integer num_6) {
        this.num_6 = num_6;
    }

    public Integer getNum_7() {
        return num_7;
    }

    public void setNum_7(Integer num_7) {
        this.num_7 = num_7;
    }

    public String getText_9() {
        return text_9;
    }

    public void setText_9(String text_9) {
        this.text_9 = text_9;
    }

    public String getText_11() {
        return text_11;
    }

    public void setText_11(String text_11) {
        this.text_11 = text_11;
    }

    public Integer getDate_2() {
        return date_2;
    }

    public void setDate_2(Integer date_2) {
        this.date_2 = date_2;
    }

    public String getText_13() {
        return text_13;
    }

    public void setText_13(String text_13) {
        this.text_13 = text_13;
    }

    public String getText_5() {
        return text_5;
    }

    public void setText_5(String text_5) {
        this.text_5 = text_5;
    }

    public String getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId;
    }

    public Integer getAddTime() {
        return addTime;
    }

    public void setAddTime(Integer addTime) {
        this.addTime = addTime;
    }

    public Integer getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Integer updateTime) {
        this.updateTime = updateTime;
    }

    public String getNum_61() {
        return num_61;
    }

    public void setNum_61(String num_61) {
        this.num_61 = num_61;
    }

    public String getNum_62() {
        return num_62;
    }

    public void setNum_62(String num_62) {
        this.num_62 = num_62;
    }

    public Integer getNum_1() {
        return num_1;
    }

    public void setNum_1(Integer num_1) {
        this.num_1 = num_1;
    }

    public List<ProV2ApiContractOutStockAddArray_1> getArray_1() {
        return array_1;
    }

    public void setArray_1(List<ProV2ApiContractOutStockAddArray_1> array_1) {
        this.array_1 = array_1;
    }

    public List<ProV2ApiContractOutStockSubForm_1> getSubForm_3() {
        return subForm_3;
    }

    public void setSubForm_3(List<ProV2ApiContractOutStockSubForm_1> subForm_3) {
        this.subForm_3 = subForm_3;
    }

    public ProV2ApiContractOutStockAddAddress_2 getAddress_2() {
        return address_2;
    }

    public void setAddress_2(ProV2ApiContractOutStockAddAddress_2 address_2) {
        this.address_2 = address_2;
    }
}
