package com.jaka.framework.core.xbongbong.api.invoice.result;

import com.jaka.framework.core.xbongbong.base.XBBAbstractAPIResult;

/**
 * @author ：james.liu
 * @date ：Created in 2022/12/13 8:47
 * @description：
 * @version: 1.0
 */
public class ProV2ApiInvoiceAddResult extends XBBAbstractAPIResult {

    private ProV2ApiInvoiceAdd result;

    public ProV2ApiInvoiceAdd getResult() {
        return result;
    }

    public void setResult(ProV2ApiInvoiceAdd result) {
        this.result = result;
    }

    public static class ProV2ApiInvoiceAdd{
        private Integer code;

        private Integer dataId;

        private String msg;

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public Integer getDataId() {
            return dataId;
        }

        public void setDataId(Integer dataId) {
            this.dataId = dataId;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }
    }
}
