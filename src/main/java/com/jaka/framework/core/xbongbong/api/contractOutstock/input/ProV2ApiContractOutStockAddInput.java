package com.jaka.framework.core.xbongbong.api.contractOutstock.input;

import com.jaka.framework.core.xbongbong.api.contractOutstock.input.dto.save.ProV2ApiContractOutStockAddDataList;
import com.jaka.framework.core.xbongbong.base.XBBAbstractAPIInput;

/**
 * @author ：james.liu
 * @date ：Created in 2022/12/2 14:00
 * @description：
 * @version: 1.0
 */
public class ProV2ApiContractOutStockAddInput extends XBBAbstractAPIInput {

    /**
     * 表单数据
     */
    private ProV2ApiContractOutStockAddDataList dataList;

    public ProV2ApiContractOutStockAddDataList getDataList() {
        return dataList;
    }

    public void setDataList(ProV2ApiContractOutStockAddDataList dataList) {
        this.dataList = dataList;
    }
}
