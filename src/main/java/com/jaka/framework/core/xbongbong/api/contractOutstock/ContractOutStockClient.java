package com.jaka.framework.core.xbongbong.api.contractOutstock;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jaka.framework.common.core.base.IHttpUtils;
import com.jaka.framework.core.xbongbong.api.contractOutstock.input.ProV2ApiContractOutStockAddInput;
import com.jaka.framework.core.xbongbong.api.contractOutstock.input.ProV2ApiContractOutStockDetailInput;
import com.jaka.framework.core.xbongbong.api.contractOutstock.input.ProV2ApiContractOutStockListInput;
import com.jaka.framework.core.xbongbong.api.contractOutstock.result.ProV2ApiContractOutStockAddResult;
import com.jaka.framework.core.xbongbong.api.contractOutstock.result.ProV2ApiContractOutStockDetailResult;
import com.jaka.framework.core.xbongbong.api.contractOutstock.result.ProV2ApiContractOutStockListResult;
import com.jaka.framework.core.xbongbong.base.XBBAbstractAPIPageInput;
import com.jaka.framework.core.xbongbong.base.XBBDefaultAPIActionProxy;
import com.jaka.framework.core.xbongbong.base.XBBRequestMappering;
import com.jaka.framework.core.xbongbong.config.XBBAPIClientConfiguration;
import com.jaka.framework.core.xbongbong.util.SignUtils;

/**
 * @author ：james.liu
 * @date ：Created in 2022/12/2 13:59
 * @description：销售出库单接口
 * @version: 1.0
 */
public class ContractOutStockClient extends XBBAbstractAPIPageInput {


    private final IHttpUtils httpUtils;
    private final XBBAPIClientConfiguration configuration;
    private final SignUtils signUtils;

    public ContractOutStockClient(final XBBAPIClientConfiguration configuration,
                                  final IHttpUtils httpUtils,
                                  final SignUtils signUtils) {
        this.configuration = configuration;
        this.httpUtils = httpUtils;
        this.signUtils = signUtils;
    }

    /**
     * 销售出库单详情接口
     *
     * @param input
     * @return
     * @throws Exception
     */
    public ProV2ApiContractOutStockDetailResult proV2ApiContractOutStockDetail(final ProV2ApiContractOutStockDetailInput input) throws Exception {
        input.setCmdId(XBBRequestMappering.pro_v2_api_contract_out_stock_detail.getMapperName());
        input.setCorpid(configuration.getCorpId());
        input.setUserId(configuration.getUserId());
        JSONObject jsonObject = removeSignData(input.toJsonString());
        return XBBDefaultAPIActionProxy.getInstance().doAction(input,
                ProV2ApiContractOutStockDetailResult.class,
                configuration,
                httpUtils,
                signUtils,
                input.toRequestBody(jsonObject),
                jsonObject);
    }


    /**
     * 保存销售出库单
     *
     * @param input
     * @return
     * @throws Exception
     */
    public ProV2ApiContractOutStockAddResult saveContractOutStock(final ProV2ApiContractOutStockAddInput input) throws Exception {
        input.setCmdId(XBBRequestMappering.pro_v2_api_contract_out_stock_add.getMapperName());
        input.setCorpid(configuration.getCorpId());
        input.setUserId(configuration.getUserId());
        JSONObject jsonObject = removeSignData(input.toJsonString());
        return XBBDefaultAPIActionProxy.getInstance().doAction(input,
                ProV2ApiContractOutStockAddResult.class,
                configuration,
                httpUtils,
                signUtils,
                input.toRequestBody(jsonObject),
                jsonObject);
    }


    /**
     * 销售出库单列表接口
     *
     * @param input
     * @return
     * @throws Exception
     */
    public ProV2ApiContractOutStockListResult proV2ApiContractOutStockList(final ProV2ApiContractOutStockListInput input) throws Exception {
        input.setCmdId(XBBRequestMappering.pro_v2_api_contract_out_stock_list.getMapperName());
        input.setCorpid(configuration.getCorpId());
        input.setUserId(configuration.getUserId());
        JSONObject jsonObject = removeSignData(input.toJsonString());
        return XBBDefaultAPIActionProxy.getInstance().doAction(input,
                ProV2ApiContractOutStockListResult.class,
                configuration,
                httpUtils,
                signUtils,
                input.toRequestBody(jsonObject),
                jsonObject);
    }


    public final JSONObject removeSignData(String data) {
        JSONObject signData = JSON.parseObject(data);
        if (signData.containsKey("cmdId")) {
            signData.remove("cmdId");
        }
        if (signData.containsKey("signUtils")) {
            signData.remove("signUtils");
        }
        return (signData.isEmpty()) ? null : signData;
    }

}
