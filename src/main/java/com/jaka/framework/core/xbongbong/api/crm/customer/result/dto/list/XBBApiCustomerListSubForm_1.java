package com.jaka.framework.core.xbongbong.api.crm.customer.result.dto.list;

/**
 * @author ：james.liu
 * @date ：Created in 2022/12/19 9:41
 * @description：
 * @version: 1.0
 */
public class XBBApiCustomerListSubForm_1 {

    private XBBApiCustomerListSubFormText_1 text_1;

    private String text_2;

    public XBBApiCustomerListSubFormText_1 getText_1() {
        return text_1;
    }

    public void setText_1(XBBApiCustomerListSubFormText_1 text_1) {
        this.text_1 = text_1;
    }

    public String getText_2() {
        return text_2;
    }

    public void setText_2(String text_2) {
        this.text_2 = text_2;
    }
}
