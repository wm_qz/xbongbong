package com.jaka.framework.core.xbongbong.api.paymentSheet;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jaka.framework.common.core.base.IHttpUtils;
import com.jaka.framework.core.xbongbong.api.paymentSheet.input.ProV2ApiPaymentSheetDetailInput;
import com.jaka.framework.core.xbongbong.api.paymentSheet.input.ProV2ApiPaymentSheetListInput;
import com.jaka.framework.core.xbongbong.api.paymentSheet.result.ProV2ApiPaymentSheetDetailResult;
import com.jaka.framework.core.xbongbong.api.paymentSheet.result.ProV2ApiPaymentSheetListResult;
import com.jaka.framework.core.xbongbong.base.XBBDefaultAPIActionProxy;
import com.jaka.framework.core.xbongbong.base.XBBRequestMappering;
import com.jaka.framework.core.xbongbong.config.XBBAPIClientConfiguration;
import com.jaka.framework.core.xbongbong.util.SignUtils;

/**
 * @author ：james.liu
 * @date ：Created in 2022/11/21 14:41
 * @description：
 * @version: 1.0
 */
public class PaymentSheetClient {

    private final IHttpUtils httpUtils;
    private final XBBAPIClientConfiguration configuration;
    private final SignUtils signUtils;

    public PaymentSheetClient(final XBBAPIClientConfiguration configuration,
                              final IHttpUtils httpUtils,
                              final SignUtils signUtils) {
        this.configuration = configuration;
        this.httpUtils = httpUtils;
        this.signUtils = signUtils;
    }

    /**
     * 回款单列表
     *
     * @param input
     * @return
     * @throws Exception
     */
    public ProV2ApiPaymentSheetListResult apiPaymentSheetList(final ProV2ApiPaymentSheetListInput input) throws Exception {
        input.setCmdId(XBBRequestMappering.pro_v2_api_payment_sheet_detail.getMapperName());
        input.setCorpid(configuration.getCorpId());
        input.setUserId(configuration.getUserId());
        JSONObject jsonObject = removeSignData(input.toJsonString());
        return XBBDefaultAPIActionProxy.getInstance().doAction(input,
                ProV2ApiPaymentSheetListResult.class,
                configuration,
                httpUtils,
                signUtils,
                input.toRequestBody(jsonObject),
                jsonObject);
    }


    /**
     * 回款单详情
     *
     * @param input
     * @return
     * @throws Exception
     */
    public ProV2ApiPaymentSheetDetailResult apiPaymentSheetDetail(final ProV2ApiPaymentSheetDetailInput input) throws Exception {
        input.setCmdId(XBBRequestMappering.pro_v2_api_payment_sheet_detail.getMapperName());
        input.setCorpid(configuration.getCorpId());
        input.setUserId(configuration.getUserId());
        JSONObject jsonObject = removeSignData(input.toJsonString());
        return XBBDefaultAPIActionProxy.getInstance().doAction(input,
                ProV2ApiPaymentSheetDetailResult.class,
                configuration,
                httpUtils,
                signUtils,
                input.toRequestBody(jsonObject),
                jsonObject);
    }


    public final JSONObject removeSignData(String data) {
        JSONObject signData = JSON.parseObject(data);
        if (signData.containsKey("cmdId")) {
            signData.remove("cmdId");
        }
        if (signData.containsKey("signUtils")) {
            signData.remove("signUtils");
        }
        return (signData.isEmpty()) ? null : signData;
    }

}
