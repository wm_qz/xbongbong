package com.jaka.framework.core.xbongbong.api.crm.customer.result;

import com.jaka.framework.core.xbongbong.base.XBBAbstractAPIResult;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/23 10:06
 * @description：删除客户接口出参
 * @version: 1.0
 */
public class XBBApiCustomerDelResult extends XBBAbstractAPIResult {
    /**
     * 返回信息
     */
    private ApiCustomerDel result;

    public ApiCustomerDel getResult() {
        return result;
    }

    public void setResult(ApiCustomerDel result) {
        this.result = result;
    }

    public static class ApiCustomerDel {
        /**
         * 不允许删除的提示
         */
        private String errorDataMemo;

        public String getErrorDataMemo() {
            return errorDataMemo;
        }

        public void setErrorDataMemo(String errorDataMemo) {
            this.errorDataMemo = errorDataMemo;
        }
    }

}
