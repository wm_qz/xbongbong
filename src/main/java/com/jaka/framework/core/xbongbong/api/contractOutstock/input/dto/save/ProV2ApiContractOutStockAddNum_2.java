package com.jaka.framework.core.xbongbong.api.contractOutstock.input.dto.save;

/**
 * @author ：james.liu
 * @date ：Created in 2022/12/2 14:07
 * @description：
 * @version: 1.0
 */
public class ProV2ApiContractOutStockAddNum_2 {

    private Integer num_2;

    public Integer getNum_2() {
        return num_2;
    }

    public void setNum_2(Integer num_2) {
        this.num_2 = num_2;
    }
}
