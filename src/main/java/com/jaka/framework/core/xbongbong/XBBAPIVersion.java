package com.jaka.framework.core.xbongbong;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/22 14:31
 * @description：销帮帮版本
 * @version: 1.0
 */
final class XBBAPIVersion {

    public static final String version = "V1.0.0";
    public static final String author = "james.liu";
    public static final String createDate = "2021-12-22";
    public static final String updateDate = "2021-12-22";

    /**
     *
     */
    public XBBAPIVersion() {

    }
}
