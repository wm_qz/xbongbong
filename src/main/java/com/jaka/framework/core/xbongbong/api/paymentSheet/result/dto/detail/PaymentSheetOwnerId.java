package com.jaka.framework.core.xbongbong.api.paymentSheet.result.dto.detail;

/**
 * @author ：james.liu
 * @date ：Created in 2022/12/1 11:07
 * @description：
 * @version: 1.0
 */
public class PaymentSheetOwnerId {

    private String avatar;

    private String id;

    private String name;

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
