package com.jaka.framework.core.xbongbong.api.invoice.input.dto.add;

/**
 * @author ：james.liu
 * @date ：Created in 2022/12/13 8:53
 * @description：
 * @version: 1.0
 */
public class ProV2ApiInvoiceAddSubForm_1 {

    private ProV2ApiInvoiceAddText_1 text_1;

    private String text_2;

    private String text_3;

    private String text_4;

    private String text_5;

    private String text_6;

    private String text_7;

    public ProV2ApiInvoiceAddText_1 getText_1() {
        return text_1;
    }

    public void setText_1(ProV2ApiInvoiceAddText_1 text_1) {
        this.text_1 = text_1;
    }

    public String getText_2() {
        return text_2;
    }

    public void setText_2(String text_2) {
        this.text_2 = text_2;
    }

    public String getText_3() {
        return text_3;
    }

    public void setText_3(String text_3) {
        this.text_3 = text_3;
    }

    public String getText_4() {
        return text_4;
    }

    public void setText_4(String text_4) {
        this.text_4 = text_4;
    }

    public String getText_5() {
        return text_5;
    }

    public void setText_5(String text_5) {
        this.text_5 = text_5;
    }

    public String getText_6() {
        return text_6;
    }

    public void setText_6(String text_6) {
        this.text_6 = text_6;
    }

    public String getText_7() {
        return text_7;
    }

    public void setText_7(String text_7) {
        this.text_7 = text_7;
    }
}
