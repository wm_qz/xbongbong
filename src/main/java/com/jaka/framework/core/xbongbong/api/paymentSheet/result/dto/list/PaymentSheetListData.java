package com.jaka.framework.core.xbongbong.api.paymentSheet.result.dto.list;

import java.util.List;

/**
 * @author ：james.liu
 * @date ：Created in 2022/11/21 14:48
 * @description：
 * @version: 1.0
 */
public class PaymentSheetListData {

    private String file_1;

    private String text_14;

    private String creatorId;

    private String coUserId;

    private String text_10;

    private String ownerId;

    private String serialNo;

    private int date_1;

    private List<String> array_1;

    private String text_3;

    private List<Integer> text_5;

    private int num_7;

    private List<Integer> text_4;

    private int num_1;

    private String text_7;

    private String text_6;

    public String getFile_1() {
        return file_1;
    }

    public void setFile_1(String file_1) {
        this.file_1 = file_1;
    }

    public String getText_14() {
        return text_14;
    }

    public void setText_14(String text_14) {
        this.text_14 = text_14;
    }

    public String getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId;
    }

    public String getCoUserId() {
        return coUserId;
    }

    public void setCoUserId(String coUserId) {
        this.coUserId = coUserId;
    }

    public String getText_10() {
        return text_10;
    }

    public void setText_10(String text_10) {
        this.text_10 = text_10;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public int getDate_1() {
        return date_1;
    }

    public void setDate_1(int date_1) {
        this.date_1 = date_1;
    }

    public List<String> getArray_1() {
        return array_1;
    }

    public void setArray_1(List<String> array_1) {
        this.array_1 = array_1;
    }

    public String getText_3() {
        return text_3;
    }

    public void setText_3(String text_3) {
        this.text_3 = text_3;
    }

    public List<Integer> getText_5() {
        return text_5;
    }

    public void setText_5(List<Integer> text_5) {
        this.text_5 = text_5;
    }

    public int getNum_7() {
        return num_7;
    }

    public void setNum_7(int num_7) {
        this.num_7 = num_7;
    }

    public List<Integer> getText_4() {
        return text_4;
    }

    public void setText_4(List<Integer> text_4) {
        this.text_4 = text_4;
    }

    public int getNum_1() {
        return num_1;
    }

    public void setNum_1(int num_1) {
        this.num_1 = num_1;
    }

    public String getText_7() {
        return text_7;
    }

    public void setText_7(String text_7) {
        this.text_7 = text_7;
    }

    public String getText_6() {
        return text_6;
    }

    public void setText_6(String text_6) {
        this.text_6 = text_6;
    }


}
