package com.jaka.framework.core.xbongbong.api.callback.support;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * @author ：james.liu
 * @date ：Created in 2022/10/8 17:06
 * @description：上下文工具
 * @version: 1.0
 */
public class CallBackContext implements ApplicationContextAware {
    /**
     * 上下文
     */
    private static ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        CallBackContext.applicationContext = applicationContext;
    }

    /**
     * 获取上下文
     *
     * @return 返回上下文工具
     */
    public static ApplicationContext getContext() {
        return CallBackContext.applicationContext;
    }
}
