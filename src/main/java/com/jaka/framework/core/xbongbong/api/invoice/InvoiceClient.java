package com.jaka.framework.core.xbongbong.api.invoice;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jaka.framework.common.core.base.IHttpUtils;
import com.jaka.framework.core.xbongbong.api.invoice.input.ProV2ApiInvoiceAddInput;
import com.jaka.framework.core.xbongbong.api.invoice.result.ProV2ApiInvoiceAddResult;
import com.jaka.framework.core.xbongbong.base.XBBDefaultAPIActionProxy;
import com.jaka.framework.core.xbongbong.base.XBBRequestMappering;
import com.jaka.framework.core.xbongbong.config.XBBAPIClientConfiguration;
import com.jaka.framework.core.xbongbong.util.SignUtils;

/**
 * @author ：james.liu
 * @date ：Created in 2022/12/13 8:45
 * @description：销项发票
 * @version: 1.0
 */
public class InvoiceClient {

    private final IHttpUtils httpUtils;
    private final XBBAPIClientConfiguration configuration;
    private final SignUtils signUtils;

    public InvoiceClient(final XBBAPIClientConfiguration configuration,
                         final IHttpUtils httpUtils,
                         final SignUtils signUtils) {
        this.configuration = configuration;
        this.httpUtils = httpUtils;
        this.signUtils = signUtils;
    }

    /**
     * 新建销项合同
     *
     * @param input
     * @return
     * @throws Exception
     */
    public ProV2ApiInvoiceAddResult proV2ApiInvoiceAdd(final ProV2ApiInvoiceAddInput input) throws Exception {
        input.setCmdId(XBBRequestMappering.pro_v2_api_invoice_add.getMapperName());
        input.setCorpid(configuration.getCorpId());
        input.setUserId(configuration.getUserId());
        JSONObject jsonObject = removeSignData(input.toJsonString());
        return XBBDefaultAPIActionProxy.getInstance().doAction(input,
                ProV2ApiInvoiceAddResult.class,
                configuration,
                httpUtils,
                signUtils,
                input.toRequestBody(jsonObject),
                jsonObject);
    }


    public final JSONObject removeSignData(String data) {
        JSONObject signData = JSON.parseObject(data);
        if (signData.containsKey("cmdId")) {
            signData.remove("cmdId");
        }
        if (signData.containsKey("signUtils")) {
            signData.remove("signUtils");
        }
        return (signData.isEmpty()) ? null : signData;
    }


}
