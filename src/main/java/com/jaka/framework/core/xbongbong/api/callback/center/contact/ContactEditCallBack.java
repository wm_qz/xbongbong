package com.jaka.framework.core.xbongbong.api.callback.center.contact;

import com.jaka.framework.core.xbongbong.api.callback.center.BaseCenter;

/**
 * @author ：james.liu
 * @date ：Created in 2022/9/30 14:09
 * @description：合同订单编辑
 * @version: 1.0
 */
public interface ContactEditCallBack {

    default void callback() {

    }
}
