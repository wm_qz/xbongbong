package com.jaka.framework.core.xbongbong.api.callback.enums;

import org.apache.commons.lang3.StringUtils;

/**
 * @author ：james.liu
 * @date ：Created in 2022/9/30 13:29
 * @description：
 * @version: 1.0
 */
public enum OperateEnums {
    /**
     * 新增
     */
    operate_new("new", "新增"),
    /**
     * 编辑
     */
    operate_edit("edit", "编辑"),
    /**
     * 删除
     */
    operate_delete("delete", "删除"),
    /**
     * 归档
     */
    operate_archived("archived", "归档"),
    /**
     * 取消归档
     */
    operate_cancel_archived("cancelArchived", "取消归档"),
    ;

    OperateEnums(String value, String desc) {
        this.value = value;
        this.desc = desc;
    }


    private String value;

    private String desc;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public static OperateEnums getValue(String value){
        if(StringUtils.isBlank(value)){
            return null;
        }
        for (OperateEnums operateEnums : values()) {
            if(StringUtils.equals(operateEnums.getValue(),value)){
                return operateEnums;
            }
        }
        return null;
    }
}
