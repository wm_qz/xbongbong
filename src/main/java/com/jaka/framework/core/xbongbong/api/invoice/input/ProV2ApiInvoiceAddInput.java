package com.jaka.framework.core.xbongbong.api.invoice.input;

import com.jaka.framework.core.xbongbong.api.invoice.input.dto.add.ProV2ApiInvoiceAddDataList;
import com.jaka.framework.core.xbongbong.base.XBBAbstractAPIInput;

/**
 * @author ：james.liu
 * @date ：Created in 2022/12/13 8:49
 * @description：
 * @version: 1.0
 */
public class ProV2ApiInvoiceAddInput extends XBBAbstractAPIInput {

    private ProV2ApiInvoiceAddDataList dataList;

    public ProV2ApiInvoiceAddDataList getDataList() {
        return dataList;
    }

    public void setDataList(ProV2ApiInvoiceAddDataList dataList) {
        this.dataList = dataList;
    }
}
