package com.jaka.framework.core.xbongbong.api.product.input;

import com.jaka.framework.core.xbongbong.base.XBBAbstractAPIInput;

/**
 * @author ：james.liu
 * @date ：Created in 2022/11/2 13:44
 * @description：产品详情接口
 * @version: 1.0
 */
public class XBBApiProductDetailInput extends XBBAbstractAPIInput {

    /**
     * 合同订单id
     */
    private Long dataId;

    public Long getDataId() {
        return dataId;
    }

    public void setDataId(Long dataId) {
        this.dataId = dataId;
    }
}
