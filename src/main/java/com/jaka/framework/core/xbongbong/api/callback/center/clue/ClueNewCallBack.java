package com.jaka.framework.core.xbongbong.api.callback.center.clue;

/**
 * @author ：james.liu
 * @date ：Created in 2022/9/30 14:09
 * @description：合同订单添加
 * @version: 1.0
 */
public interface ClueNewCallBack {

    default void callback() {

    }
}