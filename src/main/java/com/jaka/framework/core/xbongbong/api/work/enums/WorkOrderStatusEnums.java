package com.jaka.framework.core.xbongbong.api.work.enums;

/**
 * @author ：james.liu
 * @date ：Created in 2022/8/17 17:27
 * @description：
 * @version: 1.0
 */
public enum WorkOrderStatusEnums {

    WORK_ORDER_STATUS_1("待接收", "1"),
    CUSTOMER_CHANNEL_2("进行中", "2"),
    CUSTOMER_CHANNEL_3("已完成", "3"),
    CUSTOMER_CHANNEL_4("已关闭", "4"),
    ;

    WorkOrderStatusEnums(String text, String value) {
        this.text = text;
        this.value = value;
    }

    private String text;

    private String value;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
