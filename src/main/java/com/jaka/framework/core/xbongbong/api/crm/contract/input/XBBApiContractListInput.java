package com.jaka.framework.core.xbongbong.api.crm.contract.input;

import com.jaka.framework.core.xbongbong.base.XBBAbstractAPIInput;

import java.util.List;

/**
 * @author ：james.liu
 * @date ：Created in 2022/11/2 13:44
 * @description：合同订单列表接口
 * @version: 1.0
 */
public class XBBApiContractListInput extends XBBAbstractAPIInput {

    /**
     * 表单id
     */
    private Long formId;
    /**
     * 条件集合
     */
    private List<ConditionsList> conditions;

    public Long getFormId() {
        return formId;
    }

    public void setFormId(Long formId) {
        this.formId = formId;
    }

    public List<ConditionsList> getConditions() {
        return conditions;
    }

    public void setConditions(List<ConditionsList> conditions) {
        this.conditions = conditions;
    }

    public static class ConditionsList {

        private String attr;

        private String symbol;

        private List<String> value;

        public String getAttr() {
            return attr;
        }

        public void setAttr(String attr) {
            this.attr = attr;
        }

        public String getSymbol() {
            return symbol;
        }

        public void setSymbol(String symbol) {
            this.symbol = symbol;
        }

        public List<String> getValue() {
            return value;
        }

        public void setValue(List<String> value) {
            this.value = value;
        }
    }

}
