package com.jaka.framework.core.xbongbong.api.crm.contract.result;

import com.jaka.framework.core.xbongbong.api.crm.contract.result.model.list.ContractListResult;
import com.jaka.framework.core.xbongbong.base.XBBAbstractAPIResult;

/**
 * @author ：james.liu
 * @date ：Created in 2022/11/2 13:44
 * @description：合同订单列表接口
 * @version: 1.0
 */
public class XBBApiContractListResult extends XBBAbstractAPIResult {

    /**
     * 合同订单列表接口
     */
    private ContractListResult result;

    public ContractListResult getResult() {
        return result;
    }

    public void setResult(ContractListResult result) {
        this.result = result;
    }
}
