package com.jaka.framework.core.xbongbong.api.product.result;

import com.jaka.framework.core.xbongbong.api.product.result.dto.ProductResult;
import com.jaka.framework.core.xbongbong.base.XBBAbstractAPIPageInput;
import com.jaka.framework.core.xbongbong.base.XBBAbstractAPIResult;

import java.util.List;

/**
 * @author ：james.liu
 * @date ：Created in 2022/11/23 12:47
 * @description：产品管理产品列表
 * @version: 1.0
 */
public class ProV2ApiProductListResult extends XBBAbstractAPIResult {

    private ProductResult result;

    public ProductResult getResult() {
        return result;
    }

    public void setResult(ProductResult result) {
        this.result = result;
    }
}
