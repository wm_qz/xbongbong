package com.jaka.framework.core.xbongbong.api.product.result.dto;

import java.util.List;

/**
 * @author ：james.liu
 * @date ：Created in 2022/11/23 13:50
 * @description：
 * @version: 1.0
 */
public class ProductResult {

    private List<ProductListResult> list;

    private int totalCount;

    private int totalPage;

    public List<ProductListResult> getList() {
        return list;
    }

    public void setList(List<ProductListResult> list) {
        this.list = list;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }
}
