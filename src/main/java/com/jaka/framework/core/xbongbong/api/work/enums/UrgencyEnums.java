package com.jaka.framework.core.xbongbong.api.work.enums;

/**
 * @author ：james.liu
 * @date ：Created in 2022/8/17 17:27
 * @description：
 * @version: 1.0
 */
public enum UrgencyEnums {

    URGENCY_1("普通", "0"),
    URGENCY_2("紧急", "1"),
    URGENCY_3("非常紧急", "2"),
    ;

    UrgencyEnums(String text, String value) {
        this.text = text;
        this.value = value;
    }

    private String text;

    private String value;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
