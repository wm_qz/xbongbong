package com.jaka.framework.core.xbongbong.api.crm.customer.input;

import com.jaka.framework.core.xbongbong.base.XBBAbstractAPIInput;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/23 9:09
 * @description：新建客户接口入参；新建客户接口，若没有指定负责人，则会到公海池
 * @version: 1.0
 */
public class XBBApiCustomerEditInput extends XBBAbstractAPIInput {

    /**
     * 表单模板id表单模板id
     */
    private Long dataId;
    /**
     * 表单数据,其中字段对应通过表单字段解释获得
     */
    private ApiCustomerEdit dataList;

    public Long getDataId() {
        return dataId;
    }

    public void setDataId(Long dataId) {
        this.dataId = dataId;
    }

    public ApiCustomerEdit getDataList() {
        return dataList;
    }

    public void setDataList(ApiCustomerEdit dataList) {
        this.dataList = dataList;
    }

    /**
     * 修改用户对象
     */
    public static class ApiCustomerEdit extends XBBApiCustomerAddInput.ApiCustomerAdd {

    }


}
