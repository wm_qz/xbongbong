package com.jaka.framework.core.xbongbong.api.crm.customer.result.dto.list;

/**
 * @author ：james.liu
 * @date ：Created in 2022/12/19 9:41
 * @description：
 * @version: 1.0
 */
public class XBBApiCustomerListText_5 {

    private boolean checked;

    private String text;

    private String value;

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
