package com.jaka.framework.core.xbongbong.api.crm.contract.result.model.edit;

/**
 * @author ：james.liu
 * @date ：Created in 2022/11/18 18:05
 * @description：
 * @version: 1.0
 */
public class ContractEditResult {

    /**
     *  状态码 1 表示成功
     */
    private Integer code;
    /**
     *
     */
    private Long dataId;
    /**
     *
     */
    private String msg;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Long getDataId() {
        return dataId;
    }

    public void setDataId(Long dataId) {
        this.dataId = dataId;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
