package com.jaka.framework.core.xbongbong.base;

import com.alibaba.fastjson.JSONObject;
import com.jaka.framework.common.core.annotation.SystemCallMethod;
import com.jaka.framework.common.core.exception.EmptyValueException;
import com.jaka.framework.common.core.model.AbstractToJsonString;
import com.jaka.framework.common.core.model.BeanUtils;
import com.jaka.framework.common.core.utils.BasicDataConversionUtils;
import com.jaka.framework.core.xbongbong.config.XBBAPIInterfaceMapping;
import com.jaka.framework.core.xbongbong.util.SignUtils;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/22 11:40
 * @description：销帮帮入参基类
 * @version: 1.0
 */
public class XBBAbstractAPIInput implements AbstractToJsonString {
    /**
     * 公司id
     */
    private String corpid;
    /**
     * 操作人id
     */
    private String userId;
    /**
     * 描述：接口名称
     */
    private String CmdId = null;

    /**
     * 描述：签名工具，自动注入
     */
    private transient SignUtils signUtils = null;


    /**
     * 通过内省方式获取 ，JavaBean属性
     *
     * @param key
     * @return
     */
    private final Object get(final String key) {
        Object r = null;
        try {
            r = BeanUtils.getProperty(key, this);
        } catch (Exception e) {
        }
        return r;
    }

    /**
     * 校验必填 参数 是否 合法，系统自动调用
     *
     * @throws EmptyValueException 校验失败
     */
    public final void checkParameter() throws EmptyValueException {
        List<String> requiredList = XBBAPIInterfaceMapping.getRequiredListInput(getCmdId());
        if (requiredList == null) {
            return;
        }
        String value;
        for (String key : requiredList) {
            value = BasicDataConversionUtils.toString(get(key), null);
            if (StringUtils.isEmpty(value)) {
                throw new EmptyValueException(key + " is null");
            }
        }
    }

    /**
     * 描述：转换RequestBody对象
     */
    public final RequestBody toRequestBody(JSONObject src) {
        MediaType mediaType = MediaType.parse("application/json;charset=UTF-8");
        final RequestBody requestBody = RequestBody.create(mediaType, src.toJSONString());
        return requestBody;
    }

    /**
     * 描述：签名工具，自动注入
     *
     * @param signUtils
     */
    @SystemCallMethod
    public final void autoSetSignUtils(SignUtils signUtils) {
        this.signUtils = signUtils;
    }


    public String getCmdId() {
        return CmdId;
    }

    public void setCmdId(String cmdId) {
        CmdId = cmdId;
    }

    public SignUtils getSignUtils() {
        return signUtils;
    }

    public void setSignUtils(SignUtils signUtils) {
        this.signUtils = signUtils;
    }

    public String getCorpid() {
        return corpid;
    }

    public void setCorpid(String corpid) {
        this.corpid = corpid;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
