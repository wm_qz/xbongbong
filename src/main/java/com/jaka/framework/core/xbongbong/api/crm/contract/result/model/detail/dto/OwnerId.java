package com.jaka.framework.core.xbongbong.api.crm.contract.result.model.detail.dto;

/**
 * <pre>
 *  OwnerId
 * </pre>
 * @author toolscat.com
 * @verison $Id: OwnerId v 0.1 2022-11-15 11:01:04
 */
public class OwnerId{

    /**
     * <pre>
     * 
     * </pre>
     */
    private String	avatar;

    /**
     * <pre>
     * 
     * </pre>
     */
    private String	id;

    /**
     * <pre>
     * 姚正兴
     * </pre>
     */
    private String	name;


    public String getAvatar() {
      return this.avatar;
    }

    public void setAvatar(String avatar) {
      this.avatar = avatar;
    }

    public String getId() {
      return this.id;
    }

    public void setId(String id) {
      this.id = id;
    }

    public String getName() {
      return this.name;
    }

    public void setName(String name) {
      this.name = name;
    }

}
