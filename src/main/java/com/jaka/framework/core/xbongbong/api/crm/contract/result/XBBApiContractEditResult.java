package com.jaka.framework.core.xbongbong.api.crm.contract.result;

import com.jaka.framework.core.xbongbong.api.crm.contract.result.model.edit.ContractEditResult;
import com.jaka.framework.core.xbongbong.base.XBBAbstractAPIResult;

/**
 * @author ：james.liu
 * @date ：Created in 2022/11/18 18:04
 * @description：编辑合同订单
 * @version: 1.0
 */
public class XBBApiContractEditResult extends XBBAbstractAPIResult {

    private ContractEditResult result;

    public ContractEditResult getResult() {
        return result;
    }

    public void setResult(ContractEditResult result) {
        this.result = result;
    }
}
