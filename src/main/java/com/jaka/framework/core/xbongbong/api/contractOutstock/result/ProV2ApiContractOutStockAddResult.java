package com.jaka.framework.core.xbongbong.api.contractOutstock.result;

import com.jaka.framework.core.xbongbong.base.XBBAbstractAPIResult;

/**
 * @author ：james.liu
 * @date ：Created in 2022/12/2 14:46
 * @description：
 * @version: 1.0
 */
public class ProV2ApiContractOutStockAddResult extends XBBAbstractAPIResult {

    private ProV2ApiContractOutStockAdd result;

    public ProV2ApiContractOutStockAdd getResult() {
        return result;
    }

    public void setResult(ProV2ApiContractOutStockAdd result) {
        this.result = result;
    }

    public static class ProV2ApiContractOutStockAdd {

        private Integer code;

        private Integer dataId;

        private Integer formDataId;

        private String msg;

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public Integer getDataId() {
            return dataId;
        }

        public void setDataId(Integer dataId) {
            this.dataId = dataId;
        }

        public Integer getFormDataId() {
            return formDataId;
        }

        public void setFormDataId(Integer formDataId) {
            this.formDataId = formDataId;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }
    }
}
