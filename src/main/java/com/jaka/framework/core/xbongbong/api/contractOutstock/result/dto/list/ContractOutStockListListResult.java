package com.jaka.framework.core.xbongbong.api.contractOutstock.result.dto.list;

import java.util.List;

/**
 * @author ：james.liu
 * @date ：Created in 2022/12/5 9:59
 * @description：
 * @version: 1.0
 */
public class ContractOutStockListListResult {

    private List<ContractOutStockListList> list;
    private int totalCount;
    private int totalPage;

    public List<ContractOutStockListList> getList() {
        return list;
    }

    public void setList(List<ContractOutStockListList> list) {
        this.list = list;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }
}
