package com.jaka.framework.core.xbongbong.api.work.result;

import com.jaka.framework.core.xbongbong.base.XBBAbstractAPIResult;

/**
 * @author ：james.liu
 * @date ：Created in 2022/8/17 17:15
 * @description：
 * @version: 1.0
 */
public class ApiWorkOrderAddResult extends XBBAbstractAPIResult {

    private ApiWorkOrderAdd result;

    public ApiWorkOrderAdd getResult() {
        return result;
    }

    public void setResult(ApiWorkOrderAdd result) {
        this.result = result;
    }

    public static class ApiWorkOrderAdd{
        private String formDataId;

        public String getFormDataId() {
            return formDataId;
        }

        public void setFormDataId(String formDataId) {
            this.formDataId = formDataId;
        }
    }
}
