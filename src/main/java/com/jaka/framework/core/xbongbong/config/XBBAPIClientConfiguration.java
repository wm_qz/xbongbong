package com.jaka.framework.core.xbongbong.config;

import com.jaka.framework.core.xbongbong.api.XBBAPIClient;
import okhttp3.OkHttpClient;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.security.cert.CertificateException;
import java.util.concurrent.TimeUnit;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/21 11:03
 * @description：销帮帮接口配置类
 * @version: 1.0
 */
public abstract class XBBAPIClientConfiguration {

    /**
     * 客户端使用相关配置直接配置相关参数
     */
    protected void setClientConfiguration() {
    }

    /**
     * 销帮帮接口根域名
     */
    protected static final String XBB_API_ROOT = "http://proapi.xbongbong.com";
    /**
     * 本公司corpid。接口基础参数，接口请求必传
     * 管理员账号登录销帮帮WEB版后台后，访问https://pfweb.xbongbong.com/#/apiToken/index查看
     */
    protected String corpId = "";
    /**
     * 本公司访问接口的token,该值相当于密钥，请妥善保管，不要泄露，可以下列url中重置token
     * 管理员账号登录销帮帮WEB版后台后，访问https://pfweb.xbongbong.com/#/apiToken/index查看
     */
    protected String token = "";
    /**
     * 接口操作人userId,接口基础参数。不传默认取超管角色
     */
    protected String userId = "";
    /**
     * 描述：全局的http请求工具
     */
    protected volatile OkHttpClient okHttpClient = null;

    /**
     * 描述：API接口请求地址<br/>
     */
    protected transient String apiUrl = XBB_API_ROOT;

    /**
     * 描述：连接超时时间（默认：15s）
     */
    protected int connectTimeout = 15000;
    /**
     * 描述：读取数据超时时间（默认：5s）
     */
    protected int readTimeout = 10000;
    /**
     * 描述：写入数据超时时间（默认：5s）
     */
    protected int writeTimeout = 5000;

    /**
     * 构建一个API客户端
     * @return
     */
    public abstract XBBAPIClient build();

    public String getCorpId() {
        return corpId;
    }

    public XBBAPIClientConfiguration setCorpId(String corpId) {
        this.corpId = corpId;
        return this;
    }

    public String getToken() {
        return token;
    }

    public XBBAPIClientConfiguration setToken(String token) {
        this.token = token;
        return this;
    }

    public String getUserId() {
        return userId;
    }

    public XBBAPIClientConfiguration setUserId(String userId) {
        this.userId = userId;
        return this;
    }

    public OkHttpClient getOkHttpClient() {
        return okHttpClient;
    }

    public XBBAPIClientConfiguration setOkHttpClient(OkHttpClient okHttpClient) {
        this.okHttpClient = okHttpClient;
        return this;
    }

    public String getApiUrl() {
        return apiUrl;
    }

    public XBBAPIClientConfiguration setApiUrl(String apiUrl) {
        this.apiUrl = apiUrl;
        return this;
    }

    public int getConnectTimeout() {
        return connectTimeout;
    }

    public XBBAPIClientConfiguration setConnectTimeout(int connectTimeout) {
        this.connectTimeout = connectTimeout;
        return this;
    }

    public int getReadTimeout() {
        return readTimeout;
    }

    public XBBAPIClientConfiguration setReadTimeout(int readTimeout) {
        this.readTimeout = readTimeout;
        return this;
    }

    public int getWriteTimeout() {
        return writeTimeout;
    }

    public XBBAPIClientConfiguration setWriteTimeout(int writeTimeout) {
        this.writeTimeout = writeTimeout;
        return this;
    }

    /**
     * 描述：当 OkHttpClient 为NULL 是时候 自动根据当前配置  设置全局的http请求工具（设置）
     */
    public synchronized void autoSetOkHttpClientIfOkHttpClientIsNull() {
        if (this.okHttpClient == null) {
            try {
                final TrustManager[] trustAllCerts = new TrustManager[]{
                        new X509TrustManager() {
                            @Override
                            public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                            }

                            @Override
                            public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                            }

                            @Override
                            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                                return new java.security.cert.X509Certificate[]{};
                            }
                        }
                };

                // Install the all-trusting trust manager
                final SSLContext sslContext = SSLContext.getInstance("SSL");
                sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
                // Create an ssl socket factory with our all-trusting manager
                final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

                this.okHttpClient = new OkHttpClient.Builder().
                        connectTimeout(connectTimeout, TimeUnit.MILLISECONDS).
                        readTimeout(readTimeout, TimeUnit.MILLISECONDS).
                        writeTimeout(writeTimeout, TimeUnit.MILLISECONDS).
                        build();

            } catch (Exception e) {
                ///
            }

        }
    }
}
