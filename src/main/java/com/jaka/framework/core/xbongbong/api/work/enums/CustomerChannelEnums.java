package com.jaka.framework.core.xbongbong.api.work.enums;

/**
 * @author ：james.liu
 * @date ：Created in 2022/8/17 17:27
 * @description：
 * @version: 1.0
 */
public enum CustomerChannelEnums {

    CUSTOMER_CHANNEL_1("微信群或个人电话", "899a3641-c6c6-0a2a-b849-c22053cd989b"),
    CUSTOMER_CHANNEL_2("智能客服", "d6ecf4bc-9688-65b1-6a80-523401cdb96d"),
    CUSTOMER_CHANNEL_3("400电话", "05b9bd7c-82b6-804f-7a00-718e0097d750"),
    CUSTOMER_CHANNEL_4("小程序", "e2d9ee15-8ec5-29ab-24dd-31e9de1cf74d"),
    ;

    CustomerChannelEnums(String text, String value) {
        this.text = text;
        this.value = value;
    }

    private String text;

    private String value;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
