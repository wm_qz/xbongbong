package com.jaka.framework.core.xbongbong.base;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/22 17:44
 * @description：
 * @version: 1.0
 */
public class XBBPageResult {
    /**
     * 总数据量
     */
    private Integer totalCount;
    /**
     * 总页码数
     */
    private Integer totalPage;

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public Integer getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(Integer totalPage) {
        this.totalPage = totalPage;
    }
}
