package com.jaka.framework.core.xbongbong.api.crm.contract.result.model.list;

import java.util.List;

public class ContractListData {
    private int num_9;

    private String text_13;

    private String text_10;

    private String text_12;

    private String text_11;

    private String text_3;

    private String text_2;

    private String text_4;

    private String text_7;

    private String text_6;

    private String text_9;

    private String text_8;

    private int num_10;

    private String text_1;

    private String serialNo;

    private double num_25;

    private int num_26;

    private int num_27;

    private int num_28;

    private String creatorId;

    private int date_2;

    private int date_1;

    private int num_38;

    private String text_66;

    private int num_31;

    private List<Integer> array_4;

    private int num_5;

    private int num_6;

    private int num_7;

    private int num_8;

    private int num_1;

    private double num_2;

    private double num_3;

    private double num_4;

    public void setNum_9(int num_9) {
        this.num_9 = num_9;
    }

    public int getNum_9() {
        return this.num_9;
    }

    public void setText_13(String text_13) {
        this.text_13 = text_13;
    }

    public String getText_13() {
        return this.text_13;
    }

    public void setText_10(String text_10) {
        this.text_10 = text_10;
    }

    public String getText_10() {
        return this.text_10;
    }

    public void setText_12(String text_12) {
        this.text_12 = text_12;
    }

    public String getText_12() {
        return this.text_12;
    }

    public void setText_11(String text_11) {
        this.text_11 = text_11;
    }

    public String getText_11() {
        return this.text_11;
    }

    public void setText_3(String text_3) {
        this.text_3 = text_3;
    }

    public String getText_3() {
        return this.text_3;
    }

    public void setText_2(String text_2) {
        this.text_2 = text_2;
    }

    public String getText_2() {
        return this.text_2;
    }

    public void setText_4(String text_4) {
        this.text_4 = text_4;
    }

    public String getText_4() {
        return this.text_4;
    }

    public void setText_7(String text_7) {
        this.text_7 = text_7;
    }

    public String getText_7() {
        return this.text_7;
    }

    public void setText_6(String text_6) {
        this.text_6 = text_6;
    }

    public String getText_6() {
        return this.text_6;
    }

    public void setText_9(String text_9) {
        this.text_9 = text_9;
    }

    public String getText_9() {
        return this.text_9;
    }

    public void setText_8(String text_8) {
        this.text_8 = text_8;
    }

    public String getText_8() {
        return this.text_8;
    }

    public void setNum_10(int num_10) {
        this.num_10 = num_10;
    }

    public int getNum_10() {
        return this.num_10;
    }

    public void setText_1(String text_1) {
        this.text_1 = text_1;
    }

    public String getText_1() {
        return this.text_1;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public String getSerialNo() {
        return this.serialNo;
    }

    public void setNum_25(double num_25) {
        this.num_25 = num_25;
    }

    public double getNum_25() {
        return this.num_25;
    }

    public void setNum_26(int num_26) {
        this.num_26 = num_26;
    }

    public int getNum_26() {
        return this.num_26;
    }

    public void setNum_27(int num_27) {
        this.num_27 = num_27;
    }

    public int getNum_27() {
        return this.num_27;
    }

    public void setNum_28(int num_28) {
        this.num_28 = num_28;
    }

    public int getNum_28() {
        return this.num_28;
    }

    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId;
    }

    public String getCreatorId() {
        return this.creatorId;
    }

    public void setDate_2(int date_2) {
        this.date_2 = date_2;
    }

    public int getDate_2() {
        return this.date_2;
    }

    public void setDate_1(int date_1) {
        this.date_1 = date_1;
    }

    public int getDate_1() {
        return this.date_1;
    }

    public void setNum_38(int num_38) {
        this.num_38 = num_38;
    }

    public int getNum_38() {
        return this.num_38;
    }

    public void setText_66(String text_66) {
        this.text_66 = text_66;
    }

    public String getText_66() {
        return this.text_66;
    }

    public void setNum_31(int num_31) {
        this.num_31 = num_31;
    }

    public int getNum_31() {
        return this.num_31;
    }

    public void setArray_4(List<Integer> array_4) {
        this.array_4 = array_4;
    }

    public List<Integer> getArray_4() {
        return this.array_4;
    }

    public void setNum_5(int num_5) {
        this.num_5 = num_5;
    }

    public int getNum_5() {
        return this.num_5;
    }

    public void setNum_6(int num_6) {
        this.num_6 = num_6;
    }

    public int getNum_6() {
        return this.num_6;
    }

    public void setNum_7(int num_7) {
        this.num_7 = num_7;
    }

    public int getNum_7() {
        return this.num_7;
    }

    public void setNum_8(int num_8) {
        this.num_8 = num_8;
    }

    public int getNum_8() {
        return this.num_8;
    }

    public void setNum_1(int num_1) {
        this.num_1 = num_1;
    }

    public int getNum_1() {
        return this.num_1;
    }

    public void setNum_2(double num_2) {
        this.num_2 = num_2;
    }

    public double getNum_2() {
        return this.num_2;
    }

    public void setNum_3(double num_3) {
        this.num_3 = num_3;
    }

    public double getNum_3() {
        return this.num_3;
    }

    public void setNum_4(double num_4) {
        this.num_4 = num_4;
    }

    public double getNum_4() {
        return this.num_4;
    }
}