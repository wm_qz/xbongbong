package com.jaka.framework.core.xbongbong.api.crm.contract.input.model.edit;

public class Array_4 {
    private int num_3;

    private int num_4;

    private double num_6;

    private int text_1;

    private String text_3;

    public void setNum_3(int num_3) {
        this.num_3 = num_3;
    }

    public int getNum_3() {
        return this.num_3;
    }

    public void setNum_4(int num_4) {
        this.num_4 = num_4;
    }

    public int getNum_4() {
        return this.num_4;
    }

    public void setNum_6(double num_6) {
        this.num_6 = num_6;
    }

    public double getNum_6() {
        return this.num_6;
    }

    public void setText_1(int text_1) {
        this.text_1 = text_1;
    }

    public int getText_1() {
        return this.text_1;
    }

    public void setText_3(String text_3) {
        this.text_3 = text_3;
    }

    public String getText_3() {
        return this.text_3;
    }
}