package com.jaka.framework.core.xbongbong.api.market.result;

import com.jaka.framework.core.xbongbong.base.XBBAbstractAPIResult;
import com.jaka.framework.core.xbongbong.base.XBBPageResult;

import java.util.List;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/22 17:31
 * @description：市场活动列表接口出参
 * @version: 1.0
 */
public class XBBApiMarketActivityListResult extends XBBAbstractAPIResult {

    private ApiMarketActivity result;

    public static class ApiMarketActivity {
        /**
         * 客户列表
         */
        private List<ApiMarketActivityList> list;

        public List<ApiMarketActivityList> getList() {
            return list;
        }

        public void setList(List<ApiMarketActivityList> list) {
            this.list = list;
        }
    }

    public static class ApiMarketActivityList extends XBBPageResult {

        private Long addTime;

        private Long dataId;

        private Long formId;

        private Long updateTime;

        private List<MarketActivityData> data;

        public Long getAddTime() {
            return addTime;
        }

        public void setAddTime(Long addTime) {
            this.addTime = addTime;
        }

        public Long getDataId() {
            return dataId;
        }

        public void setDataId(Long dataId) {
            this.dataId = dataId;
        }

        public Long getFormId() {
            return formId;
        }

        public void setFormId(Long formId) {
            this.formId = formId;
        }

        public Long getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(Long updateTime) {
            this.updateTime = updateTime;
        }

        public List<MarketActivityData> getData() {
            return data;
        }

        public void setData(List<MarketActivityData> data) {
            this.data = data;
        }
    }

    public static class MarketActivityData {
        private String creatorId;
        private Long date_1;
        private Long date_2;
        private Long num_1;
        private Long num_2;
        private Long num_3;
        private Long num_4;
        private String text_1;
        private String text_2;
        private String text_3;
        private String text_4;
        private String text_5;
        private String text_6;

        public String getCreatorId() {
            return creatorId;
        }

        public void setCreatorId(String creatorId) {
            this.creatorId = creatorId;
        }

        public Long getDate_1() {
            return date_1;
        }

        public void setDate_1(Long date_1) {
            this.date_1 = date_1;
        }

        public Long getDate_2() {
            return date_2;
        }

        public void setDate_2(Long date_2) {
            this.date_2 = date_2;
        }

        public Long getNum_1() {
            return num_1;
        }

        public void setNum_1(Long num_1) {
            this.num_1 = num_1;
        }

        public Long getNum_2() {
            return num_2;
        }

        public void setNum_2(Long num_2) {
            this.num_2 = num_2;
        }

        public Long getNum_3() {
            return num_3;
        }

        public void setNum_3(Long num_3) {
            this.num_3 = num_3;
        }

        public Long getNum_4() {
            return num_4;
        }

        public void setNum_4(Long num_4) {
            this.num_4 = num_4;
        }

        public String getText_1() {
            return text_1;
        }

        public void setText_1(String text_1) {
            this.text_1 = text_1;
        }

        public String getText_2() {
            return text_2;
        }

        public void setText_2(String text_2) {
            this.text_2 = text_2;
        }

        public String getText_3() {
            return text_3;
        }

        public void setText_3(String text_3) {
            this.text_3 = text_3;
        }

        public String getText_4() {
            return text_4;
        }

        public void setText_4(String text_4) {
            this.text_4 = text_4;
        }

        public String getText_5() {
            return text_5;
        }

        public void setText_5(String text_5) {
            this.text_5 = text_5;
        }

        public String getText_6() {
            return text_6;
        }

        public void setText_6(String text_6) {
            this.text_6 = text_6;
        }
    }

    public ApiMarketActivity getResult() {
        return result;
    }

    public void setResult(ApiMarketActivity result) {
        this.result = result;
    }
}
