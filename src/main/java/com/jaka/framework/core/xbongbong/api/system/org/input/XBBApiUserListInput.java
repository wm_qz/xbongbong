package com.jaka.framework.core.xbongbong.api.system.org.input;

import com.jaka.framework.core.xbongbong.base.XBBAbstractAPIPageInput;

import java.util.List;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/22 13:09
 * @description：
 * @version: 1.0
 */
public class XBBApiUserListInput extends XBBAbstractAPIPageInput {
    /**
     * 员工姓名模糊查询
     */
    private String nameLike;
    /**
     * 根据userId集合筛选
     */
    private List<String> userIdIn;

    public String getNameLike() {
        return nameLike;
    }

    public void setNameLike(String nameLike) {
        this.nameLike = nameLike;
    }

    public List<String> getUserIdIn() {
        return userIdIn;
    }

    public void setUserIdIn(List<String> userIdIn) {
        this.userIdIn = userIdIn;
    }


}
