package com.jaka.framework.core.xbongbong.api.market.result;

import com.jaka.framework.core.xbongbong.base.XBBAbstractAPIResult;

import java.util.List;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/22 18:03
 * @description：
 * @version: 1.0
 */
public class XBBApiMarketActivityDetailResult extends XBBAbstractAPIResult {
    /**
     * 返回信息
     */
    private ApiMarketActivityDetail result;

    public ApiMarketActivityDetail getResult() {
        return result;
    }

    public void setResult(ApiMarketActivityDetail result) {
        this.result = result;
    }

    private static class ApiMarketActivityDetail{

        /**
         * 新建时间
         */
        private Long addTime;
        /**
         * 记录主键
         */
        private Long dataId;
        /**
         * 表单id
         */
        private Long formId;
        /**
         * 更新时间
         */
        private Long updateTime;

        private ApiMarketActivityDetailData data;

        public Long getAddTime() {
            return addTime;
        }

        public void setAddTime(Long addTime) {
            this.addTime = addTime;
        }

        public Long getDataId() {
            return dataId;
        }

        public void setDataId(Long dataId) {
            this.dataId = dataId;
        }

        public Long getFormId() {
            return formId;
        }

        public void setFormId(Long formId) {
            this.formId = formId;
        }

        public Long getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(Long updateTime) {
            this.updateTime = updateTime;
        }

        public ApiMarketActivityDetailData getData() {
            return data;
        }

        public void setData(ApiMarketActivityDetailData data) {
            this.data = data;
        }
    }


    public static class ApiMarketActivityDetailData {
        private Long date_1;
        private Long date_2;
        private Long num_1;
        private Long num_2;
        private Long num_3;
        private Long num_4;
        private Address address_1;
        private CoUserId coUserId;
        private CreatorId creatorId;
        private List<OwnerId> ownerId;
        private String text_1;
        private Text2 text_2;
        private Text3 text_3;
        private String text_4;
        private String text_5;

        public Long getDate_1() {
            return date_1;
        }

        public void setDate_1(Long date_1) {
            this.date_1 = date_1;
        }

        public Long getDate_2() {
            return date_2;
        }

        public void setDate_2(Long date_2) {
            this.date_2 = date_2;
        }

        public Long getNum_1() {
            return num_1;
        }

        public void setNum_1(Long num_1) {
            this.num_1 = num_1;
        }

        public Long getNum_2() {
            return num_2;
        }

        public void setNum_2(Long num_2) {
            this.num_2 = num_2;
        }

        public Long getNum_3() {
            return num_3;
        }

        public void setNum_3(Long num_3) {
            this.num_3 = num_3;
        }

        public Long getNum_4() {
            return num_4;
        }

        public void setNum_4(Long num_4) {
            this.num_4 = num_4;
        }

        public Address getAddress_1() {
            return address_1;
        }

        public void setAddress_1(Address address_1) {
            this.address_1 = address_1;
        }

        public CoUserId getCoUserId() {
            return coUserId;
        }

        public void setCoUserId(CoUserId coUserId) {
            this.coUserId = coUserId;
        }

        public CreatorId getCreatorId() {
            return creatorId;
        }

        public void setCreatorId(CreatorId creatorId) {
            this.creatorId = creatorId;
        }

        public List<OwnerId> getOwnerId() {
            return ownerId;
        }

        public void setOwnerId(List<OwnerId> ownerId) {
            this.ownerId = ownerId;
        }

        public String getText_1() {
            return text_1;
        }

        public void setText_1(String text_1) {
            this.text_1 = text_1;
        }

        public Text2 getText_2() {
            return text_2;
        }

        public void setText_2(Text2 text_2) {
            this.text_2 = text_2;
        }

        public Text3 getText_3() {
            return text_3;
        }

        public void setText_3(Text3 text_3) {
            this.text_3 = text_3;
        }

        public String getText_4() {
            return text_4;
        }

        public void setText_4(String text_4) {
            this.text_4 = text_4;
        }

        public String getText_5() {
            return text_5;
        }

        public void setText_5(String text_5) {
            this.text_5 = text_5;
        }
    }

    public static class Address {
        private String address;
        private String city;
        private String district;
        private String province;
        private Location location;

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getDistrict() {
            return district;
        }

        public void setDistrict(String district) {
            this.district = district;
        }

        public String getProvince() {
            return province;
        }

        public void setProvince(String province) {
            this.province = province;
        }

        public Location getLocation() {
            return location;
        }

        public void setLocation(Location location) {
            this.location = location;
        }
    }

    public static class Location {
        private float lat;
        private float lon;

        public float getLat() {
            return lat;
        }

        public void setLat(float lat) {
            this.lat = lat;
        }

        public float getLon() {
            return lon;
        }

        public void setLon(float lon) {
            this.lon = lon;
        }
    }

    public static class CoUserId {
        private String avatar;
        private String id;
        private String name;

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public static class CreatorId {
        private String id;
        private String name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public static class OwnerId {
        private String avatar;
        private String id;
        private String name;

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    private static class Text2 extends TextBase {

    }

    private static class Text3 extends TextBase {

    }

    public static class TextBase {
        private String text;

        private String value;

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }


}
