package com.jaka.framework.core.xbongbong.api.work.enums;

/**
 * @author ：james.liu
 * @date ：Created in 2022/8/17 17:27
 * @description：
 * @version: 1.0
 */
public enum ServiceContentEnums {

    SERVICE_CONTENT_1("调试陪产", "90ea3f16-5f5e-6245-2aa3-c293bfd6a08b"),
    SERVICE_CONTENT_2("客户培训", "0ce79579-32d4-04ef-8a10-d093d0c00da3"),
    SERVICE_CONTENT_3("客户原因维修", "899a3641-c6c6-0a2a-b849-c22053cd989b"),
    SERVICE_CONTENT_4("拜访洽谈", "d6ecf4bc-9688-65b1-6a80-523401cdb96d"),
    SERVICE_CONTENT_5("方案支持", "05b9bd7c-82b6-804f-7a00-718e0097d750"),
    SERVICE_CONTENT_6("技术接待", "acaa9d23-1a0e-4bae-3c30-075957e544c6"),
    SERVICE_CONTENT_7("仿真模拟", "2c5b705f-d2b6-adb1-f681-fe40efb66c2f"),
    SERVICE_CONTENT_8("节卡内部的新人培训", "d30ccfc2-53bd-9c58-0aa8-362d3cdf6ed0"),
    SERVICE_CONTENT_9("节卡--市场推广", "18e1b9ad-34f6-0c5a-bf22-941de57f93b2"),
    SERVICE_CONTENT_10("节卡--产品文档", "8692781e-398a-3ec2-3586-95bad8c2e26d"),
    SERVICE_CONTENT_11("节卡--研发&测试", "d43fb0a8-361d-ad57-128e-5e92478ebb2e"),
    SERVICE_CONTENT_12("节卡--生产&调试", "06e4a1fc-638d-022f-887c-a946e6776c95"),
    SERVICE_CONTENT_13("节卡质量问题处理", "eb7c061c-62d0-7bf4-30e3-4983300de4c3"),

    ;

    ServiceContentEnums(String text, String value) {
        this.text = text;
        this.value = value;
    }

    private String text;

    private String value;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
