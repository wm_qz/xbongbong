/**
 * Copyright 2022 bejson.com
 */
package com.jaka.framework.core.xbongbong.api.contractOutstock.result.dto.detail;

/**
 * Auto-generated: 2022-12-05 10:27:27
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class ContractOutStockDetailText_4 {

    private String name;
    private long id;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

}