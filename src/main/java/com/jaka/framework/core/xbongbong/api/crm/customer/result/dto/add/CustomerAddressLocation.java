package com.jaka.framework.core.xbongbong.api.crm.customer.result.dto.add;

/**
 * @author ：james.liu
 * @date ：Created in 2022/11/24 13:22
 * @description：
 * @version: 1.0
 */
public class CustomerAddressLocation {

    private double lon;

    private double lat;

    public void setLon(double lon){
        this.lon = lon;
    }
    public double getLon(){
        return this.lon;
    }
    public void setLat(double lat){
        this.lat = lat;
    }
    public double getLat(){
        return this.lat;
    }
}
