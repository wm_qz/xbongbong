package com.jaka.framework.core.xbongbong.api.system.from.result.detail;

/**
 * @author ：james.liu
 * @date ：Created in 2022/11/30 9:39
 * @description：
 * @version: 1.0
 */
public class NumericalLimits {

    private Long max;

    private Long min;

    public Long getMax() {
        return max;
    }

    public void setMax(Long max) {
        this.max = max;
    }

    public Long getMin() {
        return min;
    }

    public void setMin(Long min) {
        this.min = min;
    }
}
