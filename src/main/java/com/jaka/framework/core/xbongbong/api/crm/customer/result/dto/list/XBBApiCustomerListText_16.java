package com.jaka.framework.core.xbongbong.api.crm.customer.result.dto.list;

/**
 * @author ：james.liu
 * @date ：Created in 2022/12/19 9:41
 * @description：
 * @version: 1.0
 */
public class XBBApiCustomerListText_16 {

    private String avatar;

    private String id;

    private String name;

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
