package com.jaka.framework.core.xbongbong.api.crm.customer.result.dto.add;

/**
 * @author ：james.liu
 * @date ：Created in 2022/11/24 13:16
 * @description：
 * @version: 1.0
 */
public class CustomerText_50 {

    private boolean checked;

    private String text;

    private String value;

    public void setChecked(boolean checked){
        this.checked = checked;
    }
    public boolean getChecked(){
        return this.checked;
    }
    public void setText(String text){
        this.text = text;
    }
    public String getText(){
        return this.text;
    }
    public void setValue(String value){
        this.value = value;
    }
    public String getValue(){
        return this.value;
    }

}
