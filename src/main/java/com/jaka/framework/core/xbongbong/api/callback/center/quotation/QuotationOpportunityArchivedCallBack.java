package com.jaka.framework.core.xbongbong.api.callback.center.quotation;

/**
 * @author ：james.liu
 * @date ：Created in 2022/9/30 14:09
 * @description：报价单归档
 * @version: 1.0
 */
public interface QuotationOpportunityArchivedCallBack {

    /**
     * 添加用户回调
     */
    default void callback() {

    }
}
