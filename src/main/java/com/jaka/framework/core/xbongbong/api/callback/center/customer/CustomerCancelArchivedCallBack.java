package com.jaka.framework.core.xbongbong.api.callback.center.customer;

import com.jaka.framework.core.xbongbong.api.callback.center.BaseCenter;

/**
 * @author ：james.liu
 * @date ：Created in 2022/9/30 14:05
 * @description：客户取消归档回调
 * @version: 1.0
 */
public interface CustomerCancelArchivedCallBack  {

    default void callback() {

    }
}