package com.jaka.framework.core.xbongbong.api.callback.center;

import com.jaka.framework.core.xbongbong.api.callback.model.WebHookCallBackInput;

/**
 * @author ：james.liu
 * @date ：Created in 2022/9/30 15:48
 * @description：
 * @version: 1.0
 */
public interface BaseCenter {

    /**
     * 决策中心
     *
     * @param webHookCallBackInput
     */
    void center(WebHookCallBackInput webHookCallBackInput, Class<? extends BaseCallBack> callback);

}
