package com.jaka.framework.core.xbongbong.api.crm.contract.result.model.detail.dto;

import java.util.List;

/**
 * <pre>
 *  Data
 * </pre>
 *
 * @author toolscat.com
 * @verison $Id: Data v 0.1 2022-11-15 11:01:04
 */
public class Data {

    /**
     * <pre>
     * address_1
     * </pre>
     */
    private Address_1 address_1;

    /**
     * <pre>
     * array_4
     * </pre>
     */
    private List<Array_4> array_4;
    /**
     *
     */
    private List<ContractDetailArray_5> array_5;
    /**
     *
     */
    private List<ContractDetailArray_6> array_6;
    /**
     *
     */
    private List<ContractDetailArray_7> array_7;

    /**
     * <pre>
     * coUserId
     * </pre>
     */
    private List<String> coUserId;

    /**
     * <pre>
     * creatorId
     * </pre>
     */
    private CreatorId creatorId;

    /**
     * <pre>
     *
     * </pre>
     */
    private Integer date_1;
    /**
     *
     */
    private Integer date_2;

    /**
     * <pre>
     *
     * </pre>
     */
    private Integer date_3;

    /**
     * <pre>
     * file_1
     * </pre>
     */
    private List<File_1> file_1;

    /**
     * <pre>
     *
     * </pre>
     */
    private Integer num_1;

    /**
     * <pre>
     *
     * </pre>
     */
    private Integer num_11;

    /**
     * <pre>
     *
     * </pre>
     */
    private Integer num_12;

    /**
     * <pre>
     *
     * </pre>
     */
    private Integer num_13;

    /**
     * <pre>
     *
     * </pre>
     */
    private Integer num_17;

    /**
     * <pre>
     *
     * </pre>
     */
    private Integer num_19;
    /**
     * <pre>
     * num_2
     * </pre>
     */
    private Integer num_2;
    /**
     * <pre>
     *
     * </pre>
     */
    private Integer num_20;

    /**
     * <pre>
     *
     * </pre>
     */
    private Integer num_21;

    /**
     * <pre>
     *
     * </pre>
     */
    private Integer num_24;

    /**
     * <pre>
     *
     * </pre>
     */
    private Integer num_26;

    /**
     * <pre>
     *
     * </pre>
     */
    private Integer num_27;

    /**
     * <pre>
     *
     * </pre>
     */
    private Integer num_28;

    /**
     * <pre>
     *
     * </pre>
     */
    private Integer num_38;

    /**
     * <pre>
     *
     * </pre>
     */
    private Integer num_61;

    /**
     * <pre>
     * ownerId
     * </pre>
     */
    private List<OwnerId> ownerId;

    /**
     * <pre>
     *
     * </pre>
     */
    private String serialNo;

    /**
     * <pre>
     * subForm_1
     * </pre>
     */
    private List<SubForm_1> subForm_1;

    /**
     * <pre>
     * 海立项目合同
     * </pre>
     */
    private String text_1;

    /**
     * <pre>
     *
     * </pre>
     */
    private Integer text_14;

    /**
     * <pre>
     * text_15
     * </pre>
     */
    private Text_15 text_15;

    /**
     * <pre>
     * text_16
     * </pre>
     */
    private Text_16 text_16;

    /**
     * <pre>
     * text_2
     * </pre>
     */
    private Text_2 text_2;

    /**
     * <pre>
     * text_23
     * </pre>
     */
    private Text_23 text_23;

    /**
     * <pre>
     *
     * </pre>
     */
    private String text_24;

    /**
     * <pre>
     * text_25
     * </pre>
     */
    private Text_25 text_25;

    /**
     * <pre>
     * 自主开发
     * </pre>
     */
    private String text_27;

    /**
     * <pre>
     * 自主开发
     * </pre>
     */
    private String text_28;

    /**
     * <pre>
     * text_29
     * </pre>
     */
    private Text_29 text_29;

    /**
     * <pre>
     *
     * </pre>
     */
    private String text_37;

    /**
     * <pre>
     *
     * </pre>
     */
    private String text_39;

    /**
     * <pre>
     *
     * </pre>
     */
    private String text_40;

    /**
     * <pre>
     * 系统集成商
     * </pre>
     */
    private String text_41;

    /**
     * <pre>
     * 海立马瑞利(无锡)汽车热管理系统有限公司襄阳分公司
     * </pre>
     */
    private String text_42;

    /**
     * <pre>
     * 来电咨询
     * </pre>
     */
    private String text_43;

    /**
     * <pre>
     * 螺丝锁付
     * </pre>
     */
    private String text_44;

    /**
     * <pre>
     * text_45
     * </pre>
     */
    private Text_45 text_45;

    /**
     * <pre>
     * 汽车及相关
     * </pre>
     */
    private String text_46;

    /**
     * <pre>
     * text_47
     * </pre>
     */
    private Text_47 text_47;
    /**
     * <pre>
     * text_49
     * </pre>
     */
    private Text_49 text_49;


    /**
     * <pre>
     * text_6
     * </pre>
     */
    private Text_6 text_6;

    /**
     * <pre>
     * text_66
     * </pre>
     */
    private Text_66 text_66;

    /**
     * <pre>
     * text_7
     * </pre>
     */
    private Text_7 text_7;

    /**
     * <pre>
     * text_8
     * </pre>
     */
    private Text_8 text_8;

    /**
     * <pre>
     * text_9
     * </pre>
     */
    private Text_9 text_9;
    /**
     * 收入条款
     */
    private Data_Text_51 text_51;

    /**
     * 事业部
     */
    private Data_Text_50 text_50;

    public Address_1 getAddress_1() {
        return address_1;
    }

    public void setAddress_1(Address_1 address_1) {
        this.address_1 = address_1;
    }

    public List<Array_4> getArray_4() {
        return array_4;
    }

    public void setArray_4(List<Array_4> array_4) {
        this.array_4 = array_4;
    }

    public List<ContractDetailArray_5> getArray_5() {
        return array_5;
    }

    public void setArray_5(List<ContractDetailArray_5> array_5) {
        this.array_5 = array_5;
    }

    public List<ContractDetailArray_6> getArray_6() {
        return array_6;
    }

    public void setArray_6(List<ContractDetailArray_6> array_6) {
        this.array_6 = array_6;
    }

    public List<ContractDetailArray_7> getArray_7() {
        return array_7;
    }

    public void setArray_7(List<ContractDetailArray_7> array_7) {
        this.array_7 = array_7;
    }

    public List<String> getCoUserId() {
        return coUserId;
    }

    public void setCoUserId(List<String> coUserId) {
        this.coUserId = coUserId;
    }

    public CreatorId getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(CreatorId creatorId) {
        this.creatorId = creatorId;
    }

    public Integer getDate_1() {
        return date_1;
    }

    public void setDate_1(Integer date_1) {
        this.date_1 = date_1;
    }

    public Integer getDate_3() {
        return date_3;
    }

    public void setDate_3(Integer date_3) {
        this.date_3 = date_3;
    }

    public List<File_1> getFile_1() {
        return file_1;
    }

    public void setFile_1(List<File_1> file_1) {
        this.file_1 = file_1;
    }

    public Integer getNum_1() {
        return num_1;
    }

    public void setNum_1(Integer num_1) {
        this.num_1 = num_1;
    }

    public Integer getNum_11() {
        return num_11;
    }

    public void setNum_11(Integer num_11) {
        this.num_11 = num_11;
    }

    public Integer getNum_12() {
        return num_12;
    }

    public void setNum_12(Integer num_12) {
        this.num_12 = num_12;
    }

    public Integer getNum_13() {
        return num_13;
    }

    public void setNum_13(Integer num_13) {
        this.num_13 = num_13;
    }

    public Integer getNum_17() {
        return num_17;
    }

    public void setNum_17(Integer num_17) {
        this.num_17 = num_17;
    }

    public Integer getNum_19() {
        return num_19;
    }

    public void setNum_19(Integer num_19) {
        this.num_19 = num_19;
    }

    public Integer getNum_20() {
        return num_20;
    }

    public void setNum_20(Integer num_20) {
        this.num_20 = num_20;
    }

    public Integer getNum_21() {
        return num_21;
    }

    public void setNum_21(Integer num_21) {
        this.num_21 = num_21;
    }

    public Integer getNum_24() {
        return num_24;
    }

    public void setNum_24(Integer num_24) {
        this.num_24 = num_24;
    }

    public Integer getNum_26() {
        return num_26;
    }

    public void setNum_26(Integer num_26) {
        this.num_26 = num_26;
    }

    public Integer getNum_27() {
        return num_27;
    }

    public void setNum_27(Integer num_27) {
        this.num_27 = num_27;
    }

    public Integer getNum_28() {
        return num_28;
    }

    public void setNum_28(Integer num_28) {
        this.num_28 = num_28;
    }

    public Integer getNum_38() {
        return num_38;
    }

    public void setNum_38(Integer num_38) {
        this.num_38 = num_38;
    }

    public Integer getNum_61() {
        return num_61;
    }

    public void setNum_61(Integer num_61) {
        this.num_61 = num_61;
    }

    public List<OwnerId> getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(List<OwnerId> ownerId) {
        this.ownerId = ownerId;
    }

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public List<SubForm_1> getSubForm_1() {
        return subForm_1;
    }

    public void setSubForm_1(List<SubForm_1> subForm_1) {
        this.subForm_1 = subForm_1;
    }

    public String getText_1() {
        return text_1;
    }

    public void setText_1(String text_1) {
        this.text_1 = text_1;
    }

    public Integer getText_14() {
        return text_14;
    }

    public void setText_14(Integer text_14) {
        this.text_14 = text_14;
    }

    public Text_15 getText_15() {
        return text_15;
    }

    public void setText_15(Text_15 text_15) {
        this.text_15 = text_15;
    }

    public Text_16 getText_16() {
        return text_16;
    }

    public void setText_16(Text_16 text_16) {
        this.text_16 = text_16;
    }

    public Text_2 getText_2() {
        return text_2;
    }

    public void setText_2(Text_2 text_2) {
        this.text_2 = text_2;
    }

    public Text_23 getText_23() {
        return text_23;
    }

    public void setText_23(Text_23 text_23) {
        this.text_23 = text_23;
    }

    public String getText_24() {
        return text_24;
    }

    public void setText_24(String text_24) {
        this.text_24 = text_24;
    }

    public Text_25 getText_25() {
        return text_25;
    }

    public void setText_25(Text_25 text_25) {
        this.text_25 = text_25;
    }

    public String getText_27() {
        return text_27;
    }

    public void setText_27(String text_27) {
        this.text_27 = text_27;
    }

    public Text_29 getText_29() {
        return text_29;
    }

    public void setText_29(Text_29 text_29) {
        this.text_29 = text_29;
    }

    public String getText_37() {
        return text_37;
    }

    public void setText_37(String text_37) {
        this.text_37 = text_37;
    }

    public String getText_39() {
        return text_39;
    }

    public void setText_39(String text_39) {
        this.text_39 = text_39;
    }

    public String getText_40() {
        return text_40;
    }

    public void setText_40(String text_40) {
        this.text_40 = text_40;
    }

    public String getText_41() {
        return text_41;
    }

    public void setText_41(String text_41) {
        this.text_41 = text_41;
    }

    public String getText_42() {
        return text_42;
    }

    public void setText_42(String text_42) {
        this.text_42 = text_42;
    }

    public String getText_43() {
        return text_43;
    }

    public void setText_43(String text_43) {
        this.text_43 = text_43;
    }

    public String getText_44() {
        return text_44;
    }

    public void setText_44(String text_44) {
        this.text_44 = text_44;
    }

    public Text_45 getText_45() {
        return text_45;
    }

    public void setText_45(Text_45 text_45) {
        this.text_45 = text_45;
    }

    public String getText_46() {
        return text_46;
    }

    public void setText_46(String text_46) {
        this.text_46 = text_46;
    }

    public Text_47 getText_47() {
        return text_47;
    }

    public void setText_47(Text_47 text_47) {
        this.text_47 = text_47;
    }

    public Text_6 getText_6() {
        return text_6;
    }

    public void setText_6(Text_6 text_6) {
        this.text_6 = text_6;
    }

    public Text_66 getText_66() {
        return text_66;
    }

    public void setText_66(Text_66 text_66) {
        this.text_66 = text_66;
    }

    public Text_7 getText_7() {
        return text_7;
    }

    public void setText_7(Text_7 text_7) {
        this.text_7 = text_7;
    }

    public Text_8 getText_8() {
        return text_8;
    }

    public void setText_8(Text_8 text_8) {
        this.text_8 = text_8;
    }

    public Integer getDate_2() {
        return date_2;
    }

    public void setDate_2(Integer date_2) {
        this.date_2 = date_2;
    }

    public Text_49 getText_49() {
        return text_49;
    }

    public void setText_49(Text_49 text_49) {
        this.text_49 = text_49;
    }

    public Text_9 getText_9() {
        return text_9;
    }

    public void setText_9(Text_9 text_9) {
        this.text_9 = text_9;
    }

    public Integer getNum_2() {
        return num_2;
    }

    public void setNum_2(Integer num_2) {
        this.num_2 = num_2;
    }

    public String getText_28() {
        return text_28;
    }

    public void setText_28(String text_28) {
        this.text_28 = text_28;
    }

    public Data_Text_51 getText_51() {
        return text_51;
    }

    public void setText_51(Data_Text_51 text_51) {
        this.text_51 = text_51;
    }

    public Data_Text_50 getText_50() {
        return text_50;
    }

    public void setText_50(Data_Text_50 text_50) {
        this.text_50 = text_50;
    }
}
