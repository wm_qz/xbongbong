package com.jaka.framework.core.xbongbong.api.crm.customer.result.dto.add;

/**
 * @author ：james.liu
 * @date ：Created in 2022/11/24 13:18
 * @description：
 * @version: 1.0
 */
public class CustomerAddress_1 {

    private String address;

    private String province;

    private String city;

    private String district;

    private CustomerAddressLocation location;

    public void setAddress(String address){
        this.address = address;
    }
    public String getAddress(){
        return this.address;
    }
    public void setProvince(String province){
        this.province = province;
    }
    public String getProvince(){
        return this.province;
    }
    public void setCity(String city){
        this.city = city;
    }
    public String getCity(){
        return this.city;
    }
    public void setDistrict(String district){
        this.district = district;
    }
    public String getDistrict(){
        return this.district;
    }
    public void setLocation(CustomerAddressLocation location){
        this.location = location;
    }
    public CustomerAddressLocation getLocation(){
        return this.location;
    }

}
