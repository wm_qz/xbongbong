package com.jaka.framework.core.xbongbong.api.crm.customer.result;


import com.jaka.framework.core.xbongbong.base.XBBAbstractAPIResult;
import com.jaka.framework.core.xbongbong.base.XBBPageResult;

import java.util.List;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/22 16:19
 * @description：客户列表接口出参
 * @version: 1.0
 */
public class XBBApiCustomerListResult extends XBBAbstractAPIResult {
    /**
     * 返回信息
     */
    private ApiCustomerList result;

    public ApiCustomerList getResult() {
        return result;
    }

    public void setResult(ApiCustomerList result) {
        this.result = result;
    }

    public static class ApiCustomerList extends XBBPageResult {
        /**
         * 客户列表
         */
        private List<CustomerList> list;

        public List<CustomerList> getList() {
            return list;
        }

        public void setList(List<CustomerList> list) {
            this.list = list;
        }
    }

    public static class CustomerList {

        private Long addTime;

        private Long dataId;

        private Long formId;

        private Long updateTime;

        private List<CustomerData> data;

        public Long getAddTime() {
            return addTime;
        }

        public void setAddTime(Long addTime) {
            this.addTime = addTime;
        }

        public Long getDataId() {
            return dataId;
        }

        public void setDataId(Long dataId) {
            this.dataId = dataId;
        }

        public Long getFormId() {
            return formId;
        }

        public void setFormId(Long formId) {
            this.formId = formId;
        }

        public Long getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(Long updateTime) {
            this.updateTime = updateTime;
        }

        public List<CustomerData> getData() {
            return data;
        }

        public void setData(List<CustomerData> data) {
            this.data = data;
        }
    }

    public static class CustomerData {
        /**
         * 最后跟进时间
         */
        private Integer date_2;
        /**
         * 分配时间
         */
        private Long date_4;
        /**
         *
         */
        private Integer num_1;
        /**
         * 是否公海
         */
        private Integer num_3;
        /**
         * 是否归档
         */
        private Integer num_4;
        /**
         * 客户名称
         */
        private String text_1;
        /**
         * 客户编号
         */
        private String text_2;
        /**
         * 客户类型
         */
        private String text_3;
        /**
         * 客户状态
         */
        private String text_4;
        /**
         * 客户性质
         */
        private String text_5;
        /**
         * 客户等级
         */
        private String text_6;
        /**
         * 行业类型
         */
        private String text_7;
        /**
         *
         */
        private String text_8;
        /**
         * 关联销售线索
         */
        private String text_81;
        /**
         * 客户来源
         */
        private String text_9;
        /**
         *
         */
        private String text_10;
        /**
         * 客户简介
         */
        private String text_11;
        /**
         * 备注
         */
        private String text_13;
        /**
         * 创建人
         */
        private String text_16;
        /**
         * 上级客户
         */
        private String text_17;
        /**
         * 客户阶段
         */
        private String text_18;
        /**
         * 海外客户地址
         */
        private String text_38;
        /**
         * 注册资金
         */
        private String text_47;
        /**
         * 爱好
         */
        private String text_26;
        /**
         * 创建时间
         */
        private String addTime;
        /**
         * 更新时间
         */
        private String updateTime;
        /**
         * 国内客户地址
         */
        private Address address_1;
        /**
         * 法定代表人
         */
        private String text_42;
        /**
         * 税号
         */
        private String text_44;
        /**
         * 开户银行
         */
        private String text_45;
        /**
         * 银行账号
         */
        private String text_46;
        /**
         * 负责人
         */
        private String ownerId;
        /**
         * 客户编号
         */
        private String text_27;
        /**
         * 成员单选（同负责人）
         */
        private String text_40;
        /**
         * 首次是否联系 是 ： 8995e541-00a0-3df0-27a6-0f2bffb69946  否 4c82aecc-ab60-6091-bf82-08d172bfb231
         */
        private String text_41;
        /**
         * 首次反馈
         */
        private String text_43;
        /**
         *QQ
         */
        private String text_24;
        /**
         *微信
         */
        private String text_25;
        /**
         * 客户区域
         */
        private String text_39;

        public String getText_39() {
            return text_39;
        }

        public void setText_39(String text_39) {
            this.text_39 = text_39;
        }

        public String getText_18() {
            return text_18;
        }

        public void setText_18(String text_18) {
            this.text_18 = text_18;
        }

        public String getText_42() {
            return text_42;
        }

        public void setText_42(String text_42) {
            this.text_42 = text_42;
        }

        public String getText_44() {
            return text_44;
        }

        public void setText_44(String text_44) {
            this.text_44 = text_44;
        }

        public String getText_45() {
            return text_45;
        }

        public void setText_45(String text_45) {
            this.text_45 = text_45;
        }

        public String getText_46() {
            return text_46;
        }

        public void setText_46(String text_46) {
            this.text_46 = text_46;
        }

        public String getOwnerId() {
            return ownerId;
        }

        public void setOwnerId(String ownerId) {
            this.ownerId = ownerId;
        }

        public String getText_27() {
            return text_27;
        }

        public void setText_27(String text_27) {
            this.text_27 = text_27;
        }

        public String getText_40() {
            return text_40;
        }

        public void setText_40(String text_40) {
            this.text_40 = text_40;
        }

        public String getText_41() {
            return text_41;
        }

        public void setText_41(String text_41) {
            this.text_41 = text_41;
        }

        public String getText_43() {
            return text_43;
        }

        public void setText_43(String text_43) {
            this.text_43 = text_43;
        }

        public String getText_24() {
            return text_24;
        }

        public void setText_24(String text_24) {
            this.text_24 = text_24;
        }

        public String getText_25() {
            return text_25;
        }

        public void setText_25(String text_25) {
            this.text_25 = text_25;
        }

        public String getText_81() {
            return text_81;
        }

        public void setText_81(String text_81) {
            this.text_81 = text_81;
        }

        public String getText_38() {
            return text_38;
        }

        public void setText_38(String text_38) {
            this.text_38 = text_38;
        }

        public String getText_47() {
            return text_47;
        }

        public void setText_47(String text_47) {
            this.text_47 = text_47;
        }

        public String getText_26() {
            return text_26;
        }

        public void setText_26(String text_26) {
            this.text_26 = text_26;
        }

        public String getAddTime() {
            return addTime;
        }

        public void setAddTime(String addTime) {
            this.addTime = addTime;
        }

        public String getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(String updateTime) {
            this.updateTime = updateTime;
        }

        public Integer getDate_2() {
            return date_2;
        }

        public void setDate_2(Integer date_2) {
            this.date_2 = date_2;
        }

        public Long getDate_4() {
            return date_4;
        }

        public void setDate_4(Long date_4) {
            this.date_4 = date_4;
        }

        public Integer getNum_1() {
            return num_1;
        }

        public void setNum_1(Integer num_1) {
            this.num_1 = num_1;
        }

        public Integer getNum_3() {
            return num_3;
        }

        public void setNum_3(Integer num_3) {
            this.num_3 = num_3;
        }

        public Integer getNum_4() {
            return num_4;
        }

        public void setNum_4(Integer num_4) {
            this.num_4 = num_4;
        }

        public String getText_1() {
            return text_1;
        }

        public void setText_1(String text_1) {
            this.text_1 = text_1;
        }

        public String getText_2() {
            return text_2;
        }

        public void setText_2(String text_2) {
            this.text_2 = text_2;
        }

        public String getText_3() {
            return text_3;
        }

        public void setText_3(String text_3) {
            this.text_3 = text_3;
        }

        public String getText_4() {
            return text_4;
        }

        public void setText_4(String text_4) {
            this.text_4 = text_4;
        }

        public String getText_5() {
            return text_5;
        }

        public void setText_5(String text_5) {
            this.text_5 = text_5;
        }

        public String getText_6() {
            return text_6;
        }

        public void setText_6(String text_6) {
            this.text_6 = text_6;
        }

        public String getText_7() {
            return text_7;
        }

        public void setText_7(String text_7) {
            this.text_7 = text_7;
        }

        public String getText_8() {
            return text_8;
        }

        public void setText_8(String text_8) {
            this.text_8 = text_8;
        }

        public String getText_9() {
            return text_9;
        }

        public void setText_9(String text_9) {
            this.text_9 = text_9;
        }

        public String getText_10() {
            return text_10;
        }

        public void setText_10(String text_10) {
            this.text_10 = text_10;
        }

        public String getText_11() {
            return text_11;
        }

        public void setText_11(String text_11) {
            this.text_11 = text_11;
        }

        public String getText_13() {
            return text_13;
        }

        public void setText_13(String text_13) {
            this.text_13 = text_13;
        }

        public String getText_16() {
            return text_16;
        }

        public void setText_16(String text_16) {
            this.text_16 = text_16;
        }

        public String getText_17() {
            return text_17;
        }

        public void setText_17(String text_17) {
            this.text_17 = text_17;
        }

        public Address getAddress_1() {
            return address_1;
        }

        public void setAddress_1(Address address_1) {
            this.address_1 = address_1;
        }
    }

    public static class Address {
        private String address;
        private String city;
        private String district;
        private String province;
        private Location location;

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getDistrict() {
            return district;
        }

        public void setDistrict(String district) {
            this.district = district;
        }

        public String getProvince() {
            return province;
        }

        public void setProvince(String province) {
            this.province = province;
        }

        public Location getLocation() {
            return location;
        }

        public void setLocation(Location location) {
            this.location = location;
        }
    }

    public static class Location {
        private float lat;
        private float lon;

        public float getLat() {
            return lat;
        }

        public void setLat(float lat) {
            this.lat = lat;
        }

        public float getLon() {
            return lon;
        }

        public void setLon(float lon) {
            this.lon = lon;
        }
    }
}
