package com.jaka.framework.core.xbongbong.api.callback;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.jaka.framework.core.xbongbong.api.callback.center.BaseCallBack;
import com.jaka.framework.core.xbongbong.api.callback.enums.WebHookCallBackServerEnums;
import com.jaka.framework.core.xbongbong.api.callback.model.WebHookCallBackInput;
import com.jaka.framework.core.xbongbong.api.callback.strategy.StrategyContext;
import org.apache.commons.lang3.StringUtils;

import java.util.Objects;


/**
 * @author ：james.liu
 * @date ：Created in 2022/9/30 13:54
 * @description：回调客户端
 * @version: 1.0
 */
public class WebHookCallBackClient {

    public WebHookCallBackClient() {

    }

    /**
     * 回调 逻辑处理
     */
    public void callback(String input, Class<? extends BaseCallBack> callback) {
        if (StringUtils.isBlank(input)) {
            return;
        }
        WebHookCallBackInput webHookCallBackInput = JSONObject.parseObject(input, new TypeReference<WebHookCallBackInput>() {
        });
        if (Objects.isNull(webHookCallBackInput)) {
            return;
        }
        Long formId = webHookCallBackInput.getFormId();
        WebHookCallBackServerEnums webHookCallBackServerEnums = WebHookCallBackServerEnums.get(formId);
        if (Objects.isNull(webHookCallBackServerEnums)) {
            return;
        }
        StrategyContext strategyContext = new StrategyContext(webHookCallBackServerEnums.getClassz());
        strategyContext.center(webHookCallBackInput, callback);
    }

    /**
     * 回调 逻辑处理
     * @param webHookCallBackInput
     * @param callback
     */
    public void callback(WebHookCallBackInput webHookCallBackInput, Class<? extends BaseCallBack> callback) {
        if (Objects.isNull(webHookCallBackInput)) {
            return;
        }
        Long formId = webHookCallBackInput.getFormId();
        WebHookCallBackServerEnums webHookCallBackServerEnums = WebHookCallBackServerEnums.get(formId);
        if (Objects.isNull(webHookCallBackServerEnums)) {
            return;
        }
        StrategyContext strategyContext = new StrategyContext(webHookCallBackServerEnums.getClassz());
        strategyContext.center(webHookCallBackInput, callback);
    }

}
