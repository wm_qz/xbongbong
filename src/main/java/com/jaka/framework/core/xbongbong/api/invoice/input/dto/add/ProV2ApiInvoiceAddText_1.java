package com.jaka.framework.core.xbongbong.api.invoice.input.dto.add;

/**
 * @author ：james.liu
 * @date ：Created in 2022/12/13 8:54
 * @description：
 * @version: 1.0
 */
public class ProV2ApiInvoiceAddText_1 {

    private String text;

    private String value;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
