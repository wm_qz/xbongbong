package com.jaka.framework.core.xbongbong.api.crm.customer.result.dto.add;

/**
 * @author ：james.liu
 * @date ：Created in 2022/11/24 13:18
 * @description：
 * @version: 1.0
 */
public class DepartmentId {

    private int id;

    private String name;

    public void setId(int id){
        this.id = id;
    }
    public int getId(){
        return this.id;
    }
    public void setName(String name){
        this.name = name;
    }
    public String getName(){
        return this.name;
    }

}
