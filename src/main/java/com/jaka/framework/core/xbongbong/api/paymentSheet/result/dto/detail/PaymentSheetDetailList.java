package com.jaka.framework.core.xbongbong.api.paymentSheet.result.dto.detail;

/**
 * @author ：james.liu
 * @date ：Created in 2022/11/21 14:48
 * @description：
 * @version: 1.0
 */
public class PaymentSheetDetailList {

    private int addTime;

    private int alone;

    private PaymentSheetDetailData data;

    private int dataId;

    private int formId;

    private int updateTime;

    private String uuid;

    public int getAddTime() {
        return addTime;
    }

    public void setAddTime(int addTime) {
        this.addTime = addTime;
    }

    public int getAlone() {
        return alone;
    }

    public void setAlone(int alone) {
        this.alone = alone;
    }

    public PaymentSheetDetailData getData() {
        return data;
    }

    public void setData(PaymentSheetDetailData data) {
        this.data = data;
    }

    public int getDataId() {
        return dataId;
    }

    public void setDataId(int dataId) {
        this.dataId = dataId;
    }

    public int getFormId() {
        return formId;
    }

    public void setFormId(int formId) {
        this.formId = formId;
    }

    public int getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(int updateTime) {
        this.updateTime = updateTime;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
