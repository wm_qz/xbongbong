package com.jaka.framework.core.xbongbong.api.work;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jaka.framework.common.core.base.IHttpUtils;
import com.jaka.framework.core.xbongbong.api.work.input.ApiWorkOrderAddInput;
import com.jaka.framework.core.xbongbong.api.work.input.ApiWorkOrderTemplateDetailInput;
import com.jaka.framework.core.xbongbong.api.work.input.ApiWorkOrderTemplateListInput;
import com.jaka.framework.core.xbongbong.api.work.result.ApiWorkOrderAddResult;
import com.jaka.framework.core.xbongbong.api.work.result.ApiWorkOrderTemplateDetailResult;
import com.jaka.framework.core.xbongbong.api.work.result.ApiWorkOrderTemplateListResult;
import com.jaka.framework.core.xbongbong.base.XBBDefaultAPIActionProxy;
import com.jaka.framework.core.xbongbong.base.XBBRequestMappering;
import com.jaka.framework.core.xbongbong.config.XBBAPIClientConfiguration;
import com.jaka.framework.core.xbongbong.util.SignUtils;

/**
 * @author ：james.liu
 * @date ：Created in 2022/8/17 9:02
 * @description：
 * @version: 1.0
 */
public class WorkOrderClient {

    private final IHttpUtils httpUtils;
    private final XBBAPIClientConfiguration configuration;
    private final SignUtils signUtils;

    public WorkOrderClient(final XBBAPIClientConfiguration configuration,
                           final IHttpUtils httpUtils,
                           final SignUtils signUtils) {
        this.httpUtils = httpUtils;
        this.configuration = configuration;
        this.signUtils = signUtils;
    }


    public ApiWorkOrderAddResult apiWorkOrderAdd(final ApiWorkOrderAddInput input) throws Exception {
        input.setCmdId(XBBRequestMappering.api_work_order_add.getMapperName());
        input.setCorpid(configuration.getCorpId());
        input.setUserId(configuration.getUserId());
        JSONObject jsonObject = removeSignData(input.toJsonString());
        return XBBDefaultAPIActionProxy.getInstance().doAction(input,
                ApiWorkOrderAddResult.class,
                configuration,
                signUtils,
                jsonObject);
    }

    /**
     * 工单模板列表接口
     *
     * @param input
     * @return
     * @throws Exception
     */
    public ApiWorkOrderTemplateListResult workOrderTemplateList(final ApiWorkOrderTemplateListInput input) throws Exception {
        input.setCmdId(XBBRequestMappering.api_work_order_temlate_list.getMapperName());
        input.setCorpid(configuration.getCorpId());
        input.setUserId(configuration.getUserId());
        JSONObject jsonObject = removeSignData(input.toJsonString());
        return XBBDefaultAPIActionProxy.getInstance().doAction(input,
                ApiWorkOrderTemplateListResult.class,
                configuration,
                httpUtils,
                signUtils,
                input.toRequestBody(jsonObject),
                jsonObject);
    }

    /**
     * @param input
     * @return
     * @throws Exception
     */
    public ApiWorkOrderTemplateDetailResult workOrderTemplateDetail(final ApiWorkOrderTemplateDetailInput input) throws Exception {
        input.setCmdId(XBBRequestMappering.api_work_order_temlate_detail.getMapperName());
        input.setCorpid(configuration.getCorpId());
        input.setUserId(configuration.getUserId());
        JSONObject jsonObject = removeSignData(input.toJsonString());
        return XBBDefaultAPIActionProxy.getInstance().doAction(input,
                ApiWorkOrderTemplateDetailResult.class,
                configuration,
                httpUtils,
                signUtils,
                input.toRequestBody(jsonObject),
                jsonObject);
    }


    public final JSONObject removeSignData(String data) {
        JSONObject signData = JSON.parseObject(data);
        if (signData.containsKey("cmdId")) {
            signData.remove("cmdId");
        }
        if (signData.containsKey("signUtils")) {
            signData.remove("signUtils");
        }
        return (signData.isEmpty()) ? null : signData;
    }


}
