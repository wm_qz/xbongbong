package com.jaka.framework.core.xbongbong.api.crm.customer.input;

import com.jaka.framework.core.xbongbong.base.XBBAbstractAPIInput;

import java.util.List;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/23 9:09
 * @description：新建客户接口入参；新建客户接口，若没有指定负责人，则会到公海池
 * @version: 1.0
 */
public class XBBApiCustomerAddInput extends XBBAbstractAPIInput {

    /**
     * 表单模板id表单模板id
     */
    private Long formId;
    /**
     * 表单数据,其中字段对应通过表单字段解释获得
     */
    private ApiCustomerAdd dataList;

    public Long getFormId() {
        return formId;
    }

    public void setFormId(Long formId) {
        this.formId = formId;
    }

    public ApiCustomerAdd getDataList() {
        return dataList;
    }

    public void setDataList(ApiCustomerAdd dataList) {
        this.dataList = dataList;
    }

    /**
     *
     */
    public static class ApiCustomerAdd {
        private String text_1;
        private String text_2;
        private String text_3;
        private String text_4;
        private String text_5;
        private String text_6;
        private String text_7;
        private String text_8;
        private String text_9;
        private String text_10;
        private String text_11;
        private String text_13;
        private String text_16;
        private String text_17;
        private String text_18;
        /**
         * 地址
         */
        private Address address_1;
        private SubForm subForm_1;
        private List<String> ownerId;

        public String getText_1() {
            return text_1;
        }

        public void setText_1(String text_1) {
            this.text_1 = text_1;
        }

        public String getText_2() {
            return text_2;
        }

        public void setText_2(String text_2) {
            this.text_2 = text_2;
        }

        public String getText_3() {
            return text_3;
        }

        public void setText_3(String text_3) {
            this.text_3 = text_3;
        }

        public String getText_4() {
            return text_4;
        }

        public void setText_4(String text_4) {
            this.text_4 = text_4;
        }

        public String getText_5() {
            return text_5;
        }

        public void setText_5(String text_5) {
            this.text_5 = text_5;
        }

        public String getText_6() {
            return text_6;
        }

        public void setText_6(String text_6) {
            this.text_6 = text_6;
        }

        public String getText_7() {
            return text_7;
        }

        public void setText_7(String text_7) {
            this.text_7 = text_7;
        }

        public String getText_8() {
            return text_8;
        }

        public void setText_8(String text_8) {
            this.text_8 = text_8;
        }

        public String getText_9() {
            return text_9;
        }

        public void setText_9(String text_9) {
            this.text_9 = text_9;
        }

        public String getText_10() {
            return text_10;
        }

        public void setText_10(String text_10) {
            this.text_10 = text_10;
        }

        public String getText_11() {
            return text_11;
        }

        public void setText_11(String text_11) {
            this.text_11 = text_11;
        }

        public String getText_13() {
            return text_13;
        }

        public void setText_13(String text_13) {
            this.text_13 = text_13;
        }

        public String getText_16() {
            return text_16;
        }

        public void setText_16(String text_16) {
            this.text_16 = text_16;
        }

        public String getText_17() {
            return text_17;
        }

        public void setText_17(String text_17) {
            this.text_17 = text_17;
        }

        public String getText_18() {
            return text_18;
        }

        public void setText_18(String text_18) {
            this.text_18 = text_18;
        }

        public Address getAddress_1() {
            return address_1;
        }

        public void setAddress_1(Address address_1) {
            this.address_1 = address_1;
        }

        public SubForm getSubForm_1() {
            return subForm_1;
        }

        public void setSubForm_1(SubForm subForm_1) {
            this.subForm_1 = subForm_1;
        }

        public List<String> getOwnerId() {
            return ownerId;
        }

        public void setOwnerId(List<String> ownerId) {
            this.ownerId = ownerId;
        }
    }


    public static class SubForm {
        /**
         *
         */
        private String text_1;
        /**
         *
         */
        private String text_2;

        public String getText_1() {
            return text_1;
        }

        public void setText_1(String text_1) {
            this.text_1 = text_1;
        }

        public String getText_2() {
            return text_2;
        }

        public void setText_2(String text_2) {
            this.text_2 = text_2;
        }
    }

    public static class Address {
        /**
         * 城市
         */
        private String city;
        /**
         * 地址
         */
        private String address;
        /**
         * 区
         */
        private String district;
        /**
         * 省
         */
        private String province;
        /**
         * 经纬度
         */
        private Location location;

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getDistrict() {
            return district;
        }

        public void setDistrict(String district) {
            this.district = district;
        }

        public String getProvince() {
            return province;
        }

        public void setProvince(String province) {
            this.province = province;
        }

        public Location getLocation() {
            return location;
        }

        public void setLocation(Location location) {
            this.location = location;
        }
    }

    /**
     * 经纬度
     */
    public static class Location {

        private float lon;
        private float lat;

        public float getLon() {
            return lon;
        }

        public void setLon(float lon) {
            this.lon = lon;
        }

        public float getLat() {
            return lat;
        }

        public void setLat(float lat) {
            this.lat = lat;
        }
    }

}
