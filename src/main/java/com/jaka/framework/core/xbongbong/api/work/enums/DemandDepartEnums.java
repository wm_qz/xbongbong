package com.jaka.framework.core.xbongbong.api.work.enums;

/**
 * @author ：james.liu
 * @date ：Created in 2022/8/17 17:27
 * @description：
 * @version: 1.0
 */
public enum DemandDepartEnums {

    DEPART_1("市场", "90ea3f16-5f5e-6245-2aa3-c293bfd6a08b"),
    DEPART_2("产品", "899a3641-c6c6-0a2a-b849-c22053cd989b"),
    DEPART_3("研发", "d6ecf4bc-9688-65b1-6a80-523401cdb96d"),
    DEPART_4("生产", "0fbb8f5a-ec6a-2544-c60f-68a48bcee28a"),
    DEPART_5("客户服务中心内部", "05b9bd7c-82b6-804f-7a00-718e0097d750"),

    ;

    DemandDepartEnums(String text, String value) {
        this.text = text;
        this.value = value;
    }

    private String text;

    private String value;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
