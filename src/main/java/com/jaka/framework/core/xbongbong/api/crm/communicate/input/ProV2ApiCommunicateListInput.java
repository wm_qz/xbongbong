package com.jaka.framework.core.xbongbong.api.crm.communicate.input;

import com.jaka.framework.core.xbongbong.base.XBBAbstractAPIPageInput;

import java.util.List;

/**
 * @author ：james.liu
 * @date ：Created in 2022/1/28 16:16
 * @description：
 * @version: 1.0
 */
public class ProV2ApiCommunicateListInput extends XBBAbstractAPIPageInput {

    /**
     * 条件集合
     */
    private List<Conditions> conditions;

    public List<Conditions> getConditions() {
        return conditions;
    }

    public void setConditions(List<Conditions> conditions) {
        this.conditions = conditions;
    }

    public static class Conditions {

        private String attr;

        private String symbol;

        private List<String> value;

        public String getAttr() {
            return attr;
        }

        public void setAttr(String attr) {
            this.attr = attr;
        }

        public String getSymbol() {
            return symbol;
        }

        public void setSymbol(String symbol) {
            this.symbol = symbol;
        }

        public List<String> getValue() {
            return value;
        }

        public void setValue(List<String> value) {
            this.value = value;
        }
    }
}
