package com.jaka.framework.core.xbongbong.api.invoice.input.dto.add;

/**
 * @author ：james.liu
 * @date ：Created in 2022/12/13 8:53
 * @description：
 * @version: 1.0
 */
public class ProV2ApiInvoiceAddSubForm_2 {

    private ProV2ApiInvoiceAddAddress_1 address_1;

    private String text_1;

    private String text_2;

    private String text_3;

    private String text_4;

    public ProV2ApiInvoiceAddAddress_1 getAddress_1() {
        return address_1;
    }

    public void setAddress_1(ProV2ApiInvoiceAddAddress_1 address_1) {
        this.address_1 = address_1;
    }

    public String getText_1() {
        return text_1;
    }

    public void setText_1(String text_1) {
        this.text_1 = text_1;
    }

    public String getText_2() {
        return text_2;
    }

    public void setText_2(String text_2) {
        this.text_2 = text_2;
    }

    public String getText_3() {
        return text_3;
    }

    public void setText_3(String text_3) {
        this.text_3 = text_3;
    }

    public String getText_4() {
        return text_4;
    }

    public void setText_4(String text_4) {
        this.text_4 = text_4;
    }
}
