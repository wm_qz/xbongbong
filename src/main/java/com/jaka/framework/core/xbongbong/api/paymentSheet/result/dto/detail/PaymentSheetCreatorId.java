package com.jaka.framework.core.xbongbong.api.paymentSheet.result.dto.detail;

/**
 * @author ：james.liu
 * @date ：Created in 2022/12/1 11:07
 * @description：
 * @version: 1.0
 */
public class PaymentSheetCreatorId {

    private String text;

    private String value;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
