package com.jaka.framework.core.xbongbong.api.paymentSheet.result.dto.detail;

/**
 * @author ：james.liu
 * @date ：Created in 2022/12/1 11:07
 * @description：
 * @version: 1.0
 */
public class PaymentSheetText_4 {

    private String name;

    private int id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
