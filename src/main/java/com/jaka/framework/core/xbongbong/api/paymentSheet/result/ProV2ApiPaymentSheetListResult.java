package com.jaka.framework.core.xbongbong.api.paymentSheet.result;

import com.jaka.framework.core.xbongbong.api.paymentSheet.result.dto.list.PaymentSheetResult;
import com.jaka.framework.core.xbongbong.base.XBBAbstractAPIResult;

/**
 * @author ：james.liu
 * @date ：Created in 2022/11/21 14:41
 * @description：
 * @version: 1.0
 */
public class ProV2ApiPaymentSheetListResult extends XBBAbstractAPIResult {

    private PaymentSheetResult result;

    public PaymentSheetResult getResult() {
        return result;
    }

    public void setResult(PaymentSheetResult result) {
        this.result = result;
    }
}
