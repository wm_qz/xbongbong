package com.jaka.framework.core.xbongbong.api.crm.customer.result;

import com.jaka.framework.core.xbongbong.base.XBBAbstractAPIResult;

import java.util.List;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/23 10:13
 * @description：
 * @version: 1.0
 */
public class XBBApiCustomerDeleteCoUserResult extends XBBAbstractAPIResult {

    /**
     * 返回信息
     */
    private ApiCustomerDeleteCoUser result;

    public ApiCustomerDeleteCoUser getResult() {
        return result;
    }

    public void setResult(ApiCustomerDeleteCoUser result) {
        this.result = result;
    }

    public static class ApiCustomerDeleteCoUser {
        /**
         * 异常客户名
         */
        private List<Object> messageList;

        public List<Object> getMessageList() {
            return messageList;
        }

        public void setMessageList(List<Object> messageList) {
            this.messageList = messageList;
        }
    }

}
