package com.jaka.framework.core.xbongbong.api.crm.customer.result;

import com.jaka.framework.core.xbongbong.base.XBBAbstractAPIResult;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/23 9:21
 * @description：新建客户接口出参；新建客户接口，若没有指定负责人，则会到公海池
 * @version: 1.0
 */
public class XBBApiCustomerAddResult extends XBBAbstractAPIResult {

    /**
     * 返回信息
     */
    private ApiCustomerAdd result;

    public ApiCustomerAdd getResult() {
        return result;
    }

    public void setResult(ApiCustomerAdd result) {
        this.result = result;
    }

    public static class ApiCustomerAdd {
        /**
         *
         */
        private Integer code;

        private Long formDataId;

        private String msg;

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public Long getFormDataId() {
            return formDataId;
        }

        public void setFormDataId(Long formDataId) {
            this.formDataId = formDataId;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }
    }
}
