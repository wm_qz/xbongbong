package com.jaka.framework.core.xbongbong.api.crm.contract.result.model.detail.dto;

/**
 * <pre>
 *  Text_2
 * </pre>
 * @author toolscat.com
 * @verison $Id: Text_2 v 0.1 2022-11-15 11:01:04
 */
public class Text_2{

    private Integer id;

    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
