package com.jaka.framework.core.xbongbong.api.callback.center.customerCommunicate;

import com.jaka.framework.core.xbongbong.api.callback.center.BaseCenter;

/**
 * @author ：james.liu
 * @date ：Created in 2022/9/30 14:05
 * @description：新建跟进记录
 * @version: 1.0
 */
public interface CustomerCommunicateNewCallBack {

    default void callback() {

    }
}