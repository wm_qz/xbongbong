package com.jaka.framework.core.xbongbong.api.crm.customer.result.dto.add;

import com.jaka.framework.core.xbongbong.api.crm.customer.result.dto.add.*;

import java.util.List;

/**
 * @author ：james.liu
 * @date ：Created in 2022/11/24 13:12
 * @description：
 * @version: 1.0
 */
public class XBBApiCustomerDetailData {

    private int num_9;

    private CustomerText_18 text_18;

    private CustomerAddress_1 address_1;

    private DepartmentId departmentId;

    private String text_13;

    private CustomerText_16 text_16;

    private List<CustomerOwnerId> ownerId;

    private CustomerText_50 text_50;

    private CustomerText_5 text_5;

    private CustomerText_4 text_4;

    private CustomerText_7 text_7;

    private CustomerText_6 text_6;

    private CustomerText_9 text_9;

    private int num_10;

    private List<String> file_1;

    private String text_1;

    private List<String> file_2;

    private String text_47;

    private String text_46;

    private List<String> coUserId;

    private String text_43;

    private String text_42;

    private String text_44;

    private CustomerText_41 text_41;

    private CustomerText_40 text_40;

    private List<CustomerText_81> text_81;

    private CustomerText_39 text_39;

    private int date_2;

    private List<CustomerSubForm_1> subForm_1;

    private List<CustomerArray_6> array_6;

    private String text_27;

    private List<CustomerArray_3> array_3;

    private List<CustomerArray_2> array_2;

    private List<CustomerArray_5> array_5;

    private List<CustomerArray_4> array_4;

    private int num_7;

    private int num_8;

    private int num_3;

    private int num_4;

    public int getNum_9() {
        return num_9;
    }

    public void setNum_9(int num_9) {
        this.num_9 = num_9;
    }

    public CustomerText_18 getText_18() {
        return text_18;
    }

    public void setText_18(CustomerText_18 text_18) {
        this.text_18 = text_18;
    }

    public CustomerAddress_1 getAddress_1() {
        return address_1;
    }

    public void setAddress_1(CustomerAddress_1 address_1) {
        this.address_1 = address_1;
    }

    public DepartmentId getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(DepartmentId departmentId) {
        this.departmentId = departmentId;
    }

    public String getText_13() {
        return text_13;
    }

    public void setText_13(String text_13) {
        this.text_13 = text_13;
    }

    public CustomerText_16 getText_16() {
        return text_16;
    }

    public void setText_16(CustomerText_16 text_16) {
        this.text_16 = text_16;
    }

    public List<CustomerOwnerId> getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(List<CustomerOwnerId> ownerId) {
        this.ownerId = ownerId;
    }

    public CustomerText_50 getText_50() {
        return text_50;
    }

    public void setText_50(CustomerText_50 text_50) {
        this.text_50 = text_50;
    }

    public CustomerText_5 getText_5() {
        return text_5;
    }

    public void setText_5(CustomerText_5 text_5) {
        this.text_5 = text_5;
    }

    public CustomerText_4 getText_4() {
        return text_4;
    }

    public void setText_4(CustomerText_4 text_4) {
        this.text_4 = text_4;
    }

    public CustomerText_7 getText_7() {
        return text_7;
    }

    public void setText_7(CustomerText_7 text_7) {
        this.text_7 = text_7;
    }

    public CustomerText_6 getText_6() {
        return text_6;
    }

    public void setText_6(CustomerText_6 text_6) {
        this.text_6 = text_6;
    }

    public CustomerText_9 getText_9() {
        return text_9;
    }

    public void setText_9(CustomerText_9 text_9) {
        this.text_9 = text_9;
    }

    public int getNum_10() {
        return num_10;
    }

    public void setNum_10(int num_10) {
        this.num_10 = num_10;
    }

    public List<String> getFile_1() {
        return file_1;
    }

    public void setFile_1(List<String> file_1) {
        this.file_1 = file_1;
    }

    public String getText_1() {
        return text_1;
    }

    public void setText_1(String text_1) {
        this.text_1 = text_1;
    }

    public List<String> getFile_2() {
        return file_2;
    }

    public void setFile_2(List<String> file_2) {
        this.file_2 = file_2;
    }

    public String getText_47() {
        return text_47;
    }

    public void setText_47(String text_47) {
        this.text_47 = text_47;
    }

    public String getText_46() {
        return text_46;
    }

    public void setText_46(String text_46) {
        this.text_46 = text_46;
    }

    public List<String> getCoUserId() {
        return coUserId;
    }

    public void setCoUserId(List<String> coUserId) {
        this.coUserId = coUserId;
    }

    public String getText_43() {
        return text_43;
    }

    public void setText_43(String text_43) {
        this.text_43 = text_43;
    }

    public String getText_42() {
        return text_42;
    }

    public void setText_42(String text_42) {
        this.text_42 = text_42;
    }

    public String getText_44() {
        return text_44;
    }

    public void setText_44(String text_44) {
        this.text_44 = text_44;
    }

    public CustomerText_41 getText_41() {
        return text_41;
    }

    public void setText_41(CustomerText_41 text_41) {
        this.text_41 = text_41;
    }

    public CustomerText_40 getText_40() {
        return text_40;
    }

    public void setText_40(CustomerText_40 text_40) {
        this.text_40 = text_40;
    }

    public List<CustomerText_81> getText_81() {
        return text_81;
    }

    public void setText_81(List<CustomerText_81> text_81) {
        this.text_81 = text_81;
    }

    public CustomerText_39 getText_39() {
        return text_39;
    }

    public void setText_39(CustomerText_39 text_39) {
        this.text_39 = text_39;
    }

    public int getDate_2() {
        return date_2;
    }

    public void setDate_2(int date_2) {
        this.date_2 = date_2;
    }

    public List<CustomerSubForm_1> getSubForm_1() {
        return subForm_1;
    }

    public void setSubForm_1(List<CustomerSubForm_1> subForm_1) {
        this.subForm_1 = subForm_1;
    }

    public List<CustomerArray_6> getArray_6() {
        return array_6;
    }

    public void setArray_6(List<CustomerArray_6> array_6) {
        this.array_6 = array_6;
    }

    public String getText_27() {
        return text_27;
    }

    public void setText_27(String text_27) {
        this.text_27 = text_27;
    }

    public List<CustomerArray_3> getArray_3() {
        return array_3;
    }

    public void setArray_3(List<CustomerArray_3> array_3) {
        this.array_3 = array_3;
    }

    public List<CustomerArray_2> getArray_2() {
        return array_2;
    }

    public void setArray_2(List<CustomerArray_2> array_2) {
        this.array_2 = array_2;
    }

    public List<CustomerArray_5> getArray_5() {
        return array_5;
    }

    public void setArray_5(List<CustomerArray_5> array_5) {
        this.array_5 = array_5;
    }

    public List<CustomerArray_4> getArray_4() {
        return array_4;
    }

    public void setArray_4(List<CustomerArray_4> array_4) {
        this.array_4 = array_4;
    }

    public int getNum_7() {
        return num_7;
    }

    public void setNum_7(int num_7) {
        this.num_7 = num_7;
    }

    public int getNum_8() {
        return num_8;
    }

    public void setNum_8(int num_8) {
        this.num_8 = num_8;
    }

    public int getNum_3() {
        return num_3;
    }

    public void setNum_3(int num_3) {
        this.num_3 = num_3;
    }

    public int getNum_4() {
        return num_4;
    }

    public void setNum_4(int num_4) {
        this.num_4 = num_4;
    }
}
