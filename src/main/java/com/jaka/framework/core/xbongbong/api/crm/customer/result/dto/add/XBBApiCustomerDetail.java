package com.jaka.framework.core.xbongbong.api.crm.customer.result.dto.add;

/**
 * @author ：james.liu
 * @date ：Created in 2022/11/24 13:12
 * @description：
 * @version: 1.0
 */
public class XBBApiCustomerDetail {
    /**
     * 新建时间
     */
    private Long addTime;
    /**
     * 记录主键
     */
    private Long dataId;
    /**
     * 表单id
     */
    private Long formId;
    /**
     * 更新时间
     */
    private Long updateTime;
    /**
     * 客户详情信息,字段含义可通过‘表单模板字段解释接口’查看
     */
    private XBBApiCustomerDetailData data;

    public Long getAddTime() {
        return addTime;
    }

    public void setAddTime(Long addTime) {
        this.addTime = addTime;
    }

    public Long getDataId() {
        return dataId;
    }

    public void setDataId(Long dataId) {
        this.dataId = dataId;
    }

    public Long getFormId() {
        return formId;
    }

    public void setFormId(Long formId) {
        this.formId = formId;
    }

    public Long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Long updateTime) {
        this.updateTime = updateTime;
    }

    public XBBApiCustomerDetailData getData() {
        return data;
    }

    public void setData(XBBApiCustomerDetailData data) {
        this.data = data;
    }
}
