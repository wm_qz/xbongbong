package com.jaka.framework.core.xbongbong.api.crm.customer.result;

import com.jaka.framework.core.xbongbong.api.crm.customer.result.dto.add.XBBApiCustomerDetail;
import com.jaka.framework.core.xbongbong.base.XBBAbstractAPIResult;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/23 9:37
 * @description：客户详情接口出参
 * @version: 1.0
 */
public class XBBApiCustomerDetailResult extends XBBAbstractAPIResult {

    /**
     *
     */
    private XBBApiCustomerDetail result;

    public XBBApiCustomerDetail getResult() {
        return result;
    }

    public void setResult(XBBApiCustomerDetail result) {
        this.result = result;
    }

}
