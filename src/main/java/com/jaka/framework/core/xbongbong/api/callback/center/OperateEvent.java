package com.jaka.framework.core.xbongbong.api.callback.center;

import com.jaka.framework.core.xbongbong.api.callback.model.WebHookCallBackInput;
import com.jaka.framework.core.xbongbong.api.callback.support.CallBackContext;
import org.springframework.context.ApplicationContext;

import java.util.Objects;

/**
 * @author ：james.liu
 * @date ：Created in 2022/10/8 11:43
 * @description：操作类型
 * @version: 1.0
 */
public class OperateEvent {

    public void center(WebHookCallBackInput webHookCallBackInput, Class<? extends BaseCallBack> callback) {
        if (Objects.isNull(callback)) {
            return;
        }
        ApplicationContext context = CallBackContext.getContext();
        BaseCallBack bean = context.getBean(callback);
        bean.callback(webHookCallBackInput);
    }
}
