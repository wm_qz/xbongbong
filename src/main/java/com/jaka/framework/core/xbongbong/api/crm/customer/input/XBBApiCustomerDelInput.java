package com.jaka.framework.core.xbongbong.api.crm.customer.input;

import com.jaka.framework.core.xbongbong.base.XBBAbstractAPIInput;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/23 10:05
 * @description：删除客户接口入参
 * @version: 1.0
 */
public class XBBApiCustomerDelInput extends XBBAbstractAPIInput {

    /**
     * 表单数据id
     */
    private Long dataId;

    public Long getDataId() {
        return dataId;
    }

    public void setDataId(Long dataId) {
        this.dataId = dataId;
    }
}
