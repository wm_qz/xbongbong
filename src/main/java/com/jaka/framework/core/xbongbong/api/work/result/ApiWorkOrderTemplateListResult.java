package com.jaka.framework.core.xbongbong.api.work.result;

import com.jaka.framework.core.xbongbong.base.XBBAbstractAPIResult;

import java.util.List;

/**
 * @author ：james.liu
 * @date ：Created in 2022/8/17 9:01
 * @description：
 * @version: 1.0
 */
public class ApiWorkOrderTemplateListResult extends XBBAbstractAPIResult {

    private ApiWorkOrderTemplate result;

    public ApiWorkOrderTemplate getResult() {
        return result;
    }

    public void setResult(ApiWorkOrderTemplate result) {
        this.result = result;
    }

    public static class ApiWorkOrderTemplate{

        private List<ApiWorkOrderTemplateList> list;

        public List<ApiWorkOrderTemplateList> getList() {
            return list;
        }

        public void setList(List<ApiWorkOrderTemplateList> list) {
            this.list = list;
        }
    }


    public static class ApiWorkOrderTemplateList{

        private Long formId;

        private Integer isEnable;

        private String name;

        public Long getFormId() {
            return formId;
        }

        public void setFormId(Long formId) {
            this.formId = formId;
        }

        public Integer getIsEnable() {
            return isEnable;
        }

        public void setIsEnable(Integer isEnable) {
            this.isEnable = isEnable;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
