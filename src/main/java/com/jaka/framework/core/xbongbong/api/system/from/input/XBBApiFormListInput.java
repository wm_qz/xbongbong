package com.jaka.framework.core.xbongbong.api.system.from.input;

import com.jaka.framework.core.xbongbong.base.XBBAbstractAPIInput;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/22 18:39
 * @description：表单模板列表接口
 * @version: 1.0
 */
public class XBBApiFormListInput extends XBBAbstractAPIInput {

    /**
     * 模板名称模糊查询
     */
    private String name;
    /**
     * 表单类型，
     * 1系统表单，
     * 2自定义表单
     */
    private Integer saasMark;
    /**
     * 表单业务类型,客户：
     * 100，合同订单：
     * 201，退货退款：
     * 202，销售机会：
     * 301，联系人：
     * 401，跟进记录：
     * 501，回款计划：
     * 701，回款单：
     * 702，销项发票：
     * 901，供应商：
     * 1001，采购合同：
     * 1101，采购入库单：
     * 1404，其他入库单：
     * 1406，销售出库单：
     * 1504，其他出库单：
     * 1506，调拨单：
     * 1601，盘点单：
     * 1701，产品：
     * 2401；线索：
     * 8000；市场活动：
     * 8100；仓库：
     * 1801；自定义表单：不传
     */
    private Integer businessType;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSaasMark() {
        return saasMark;
    }

    public void setSaasMark(Integer saasMark) {
        this.saasMark = saasMark;
    }

    public Integer getBusinessType() {
        return businessType;
    }

    public void setBusinessType(Integer businessType) {
        this.businessType = businessType;
    }
}
