package com.jaka.framework.core.xbongbong.api.crm.contract.result;

import com.jaka.framework.core.xbongbong.api.crm.contract.result.model.detail.ContractDetailResult;
import com.jaka.framework.core.xbongbong.base.XBBAbstractAPIResult;

/**
 * @author ：james.liu
 * @date ：Created in 2022/11/2 13:44
 * @description：合同订单详情接口
 * @version: 1.0
 */
public class XBBApiContractDetailResult extends XBBAbstractAPIResult {

    /**
     * 合同订单详情接口
     */
    private ContractDetailResult result;

    public ContractDetailResult getResult() {
        return result;
    }

    public void setResult(ContractDetailResult result) {
        this.result = result;
    }
}
