package com.jaka.framework.core.xbongbong.invoice;

import com.jaka.framework.core.xbongbong.XBBAPIClientBuilder;
import com.jaka.framework.core.xbongbong.api.invoice.input.ProV2ApiInvoiceAddInput;
import com.jaka.framework.core.xbongbong.api.invoice.input.dto.add.ProV2ApiInvoiceAddDataList;
import com.jaka.framework.core.xbongbong.api.invoice.result.ProV2ApiInvoiceAddResult;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ：james.liu
 * @date ：Created in 2022/12/16 13:49
 * @description：
 * @version: 1.0
 */
public class TestInvoiceClient {

    /**
     * 新建销项发票
     */
    @Test
    public void test_pro_v2_api_invoice_add() throws Exception {
        ProV2ApiInvoiceAddInput input = new ProV2ApiInvoiceAddInput();
        ProV2ApiInvoiceAddDataList dataList = new ProV2ApiInvoiceAddDataList();
        dataList.setSerialNo("");
        List<String> ownerIds = new ArrayList<>();
        ownerIds.add("16483553554452228");
        dataList.setOwnerId(ownerIds);
        dataList.setText_2("1234567890wertyuiop");
        //{"id":92199928,"name":"常州节卡智能装备有限公司"}
        dataList.setText_3(92199928);
//        dataList.setText_4();
        dataList.setText_61("回款单");
        dataList.setNum_2(1);
//        dataList.setText_5();
        dataList.setDate_1(1669858021);
//        dataList.setText_6();
//        dataList.setText_7();
//        dataList.setSubForm_1();
//        dataList.setSubForm_2();
        input.setDataList(dataList);
        ProV2ApiInvoiceAddResult result =
                XBBAPIClientBuilder.getInstance().build().getInvoiceClient().proV2ApiInvoiceAdd(input);
        System.out.println(result.toJsonString());
    }
}
