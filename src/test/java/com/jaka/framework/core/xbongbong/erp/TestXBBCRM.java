package com.jaka.framework.core.xbongbong.erp;

import com.jaka.framework.common.core.annotation.IsTested;
import com.jaka.framework.core.xbongbong.XBBAPIClientBuilder;
import com.jaka.framework.core.xbongbong.api.crm.contacts.input.XBBProV2ApiContactListInput;
import com.jaka.framework.core.xbongbong.api.crm.contacts.result.XBBProV2ApiContactListResult;
import com.jaka.framework.core.xbongbong.api.crm.contract.input.XBBApiContractDetailInput;
import com.jaka.framework.core.xbongbong.api.crm.contract.input.XBBApiContractEditInput;
import com.jaka.framework.core.xbongbong.api.crm.contract.input.model.edit.DataList;
import com.jaka.framework.core.xbongbong.api.crm.contract.result.XBBApiContractDetailResult;
import com.jaka.framework.core.xbongbong.api.crm.contract.result.XBBApiContractEditResult;
import com.jaka.framework.core.xbongbong.api.crm.customer.input.XBBApiCustomerListInput;
import com.jaka.framework.core.xbongbong.api.crm.customer.result.XBBApiCustomerListResult;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/22 16:46
 * @description：CRM模块测试类
 * @version: 1.0
 */
public class TestXBBCRM {

    /**
     * 客户列表接口 测试类
     *
     * @throws Exception
     */
    @Test
    @IsTested
    public void test_xbb_api_customer_list() throws Exception {
        XBBApiCustomerListInput input = new XBBApiCustomerListInput();
        input.setPage(1);
        input.setPageSize(100);
        input.setFormId(null);
//        List<XBBApiCustomerListInput.ConditionsList> conditionsLists = new ArrayList<>();
//        XBBApiCustomerListInput.ConditionsList conditionsList = new XBBApiCustomerListInput.ConditionsList();
//        conditionsList.setAttr("text_1");
//        conditionsList.setSymbol("equal");
//        List<String> value = new ArrayList<>();
//        value.add("深圳市易天自动化设备股份有限公司");
//        conditionsList.setValue(value);
//        conditionsLists.add(conditionsList);
//        input.setConditions(conditionsLists);
        XBBApiCustomerListResult apiCustomerList = XBBAPIClientBuilder.getInstance().build().getCrmClient().getAPICustomerList(input);
        System.out.println(apiCustomerList.toJsonString());
    }

    @Test
    public void test_xbb_pro_v2_api_contact_list() throws Exception {
        XBBProV2ApiContactListInput input = new XBBProV2ApiContactListInput();
        input.setPage(1);
        input.setPageSize(100);
        XBBProV2ApiContactListResult result = XBBAPIClientBuilder.getInstance().build().getCrmClient().proV2ApiContactList(input);
        System.out.println(result.toJsonString());
    }

    @Test
    public void test_xbb_pro_v2_api_contact_edit() throws Exception {
        XBBApiContractEditInput editInput = new XBBApiContractEditInput();
        editInput.setDataId(null);
        DataList dataList = new DataList();
        dataList.setText_24("XSDD003750");
//        String text2 = crmContract.getText2();
//        if(Objects.isNull(text2)){
//            return;
//        }
//        Text_2 text_2 = JSON.parseObject(text2, Text_2.class);
//        dataList.setSerialNo("JKSD-CS-20221121012");
//        dataList.setText_1("常州节卡测试合同订单");
//        dataList.setText_2(92199928);
//        dataList.setText_66(439933);
        editInput.setDataList(dataList);
        XBBApiContractEditResult xbbApiContractEditResult = XBBAPIClientBuilder.getInstance().build().getCrmClient().apiContractEdit(editInput);
        System.out.println(xbbApiContractEditResult.toJsonString());
    }

    @Test
    public void test_contract_detail() throws Exception {
        XBBApiContractDetailInput input = new XBBApiContractDetailInput();
        input.setDataId(null);
        XBBApiContractDetailResult xbbApiContractDetailResult = XBBAPIClientBuilder.getInstance().build().getCrmClient().apiContractDetail(input);
        System.out.println(xbbApiContractDetailResult.toJsonString());
    }
}
