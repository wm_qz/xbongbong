package com.jaka.framework.core.xbongbong.system.from;

import com.jaka.framework.common.core.annotation.IsTested;
import com.jaka.framework.core.xbongbong.XBBAPIClientBuilder;
import com.jaka.framework.core.xbongbong.api.system.from.input.XBBApiFormGetInput;
import com.jaka.framework.core.xbongbong.api.system.from.input.XBBApiFormListInput;
import com.jaka.framework.core.xbongbong.api.system.from.result.XBBApiFormGetResult;
import com.jaka.framework.core.xbongbong.api.system.from.result.XBBApiFormListResult;
import com.jaka.framework.core.xbongbong.em.SaasMarkEnums;
import org.junit.Test;

/**
 * @author ：james.liu
 * @date ：Created in 2022/1/13 11:42
 * @description：
 * @version: 1.0
 */
public class TestFromt {

    @Test
    @IsTested
    public void system_api_form_list() throws Exception {
        XBBApiFormListInput input = new XBBApiFormListInput();
        input.setSaasMark(SaasMarkEnums.system_form.getSaasMark());
        input.setBusinessType(100);
        XBBApiFormListResult result = XBBAPIClientBuilder.getInstance().build().getSystemClient().apiFormList(input);
        System.out.println(result.toJsonString());
    }

    /**
     * @throws Exception
     */
    @Test
    public void test_xbb_system_api_from_get() throws Exception {
        XBBApiFormGetInput input = new XBBApiFormGetInput();
        input.setFormId(null);
        XBBApiFormGetResult result = XBBAPIClientBuilder.getInstance().build().getSystemClient().getAPIFomGet(input);
        System.out.println(result.toJsonString());


    }
}
