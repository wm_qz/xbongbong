package com.jaka.framework.core.xbongbong.merket;

import com.jaka.framework.common.core.annotation.IsTested;
import com.jaka.framework.core.xbongbong.XBBAPIClientBuilder;
import com.jaka.framework.core.xbongbong.api.market.input.ProV2ApiClueListInput;
import com.jaka.framework.core.xbongbong.api.market.input.XBBApiMarketActivityDetailInput;
import com.jaka.framework.core.xbongbong.api.market.input.XBBApiMarketActivityListInput;
import com.jaka.framework.core.xbongbong.api.market.result.ProV2ApiClueListResult;
import com.jaka.framework.core.xbongbong.api.market.result.XBBApiMarketActivityDetailResult;
import com.jaka.framework.core.xbongbong.api.market.result.XBBApiMarketActivityListResult;
import org.junit.Test;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/22 17:54
 * @description：市场活动模块测试类
 * @version: 1.0
 */
public class TestXBBMerket {

    @Test
    @IsTested
    public void test_api_market_activity_list() throws Exception {
        XBBApiMarketActivityListInput input = new XBBApiMarketActivityListInput();
        input.setFormId(null);
        XBBApiMarketActivityListResult result = XBBAPIClientBuilder.getInstance().build().getMarketClient().apiMarketActivityList(input);
        System.out.println(result.toJsonString());
    }

    @Test
    @IsTested
    public void test_api_market_activity_detail() throws Exception {
        XBBApiMarketActivityDetailInput input = new XBBApiMarketActivityDetailInput();
        input.setDataId(null);
        XBBApiMarketActivityDetailResult result = XBBAPIClientBuilder.getInstance().build().getMarketClient().apiMarketActivityDetail(input);
        System.out.println(result.toJsonString());
    }

    @Test
    public void test_xbb_pro_v2_api_clue_list() throws Exception {
        ProV2ApiClueListInput input = new ProV2ApiClueListInput();
        input.setFormId(null);
        ProV2ApiClueListResult result = XBBAPIClientBuilder.getInstance().build().getMarketClient().proV2ApiClueList(input);
        System.out.println(result.toJsonString());
    }
}
