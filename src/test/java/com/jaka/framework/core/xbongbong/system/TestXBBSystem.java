package com.jaka.framework.core.xbongbong.system;

import com.alibaba.fastjson.JSONObject;
import com.jaka.framework.common.core.annotation.IsTested;
import com.jaka.framework.core.xbongbong.XBBAPIClientBuilder;
import com.jaka.framework.core.xbongbong.api.system.from.input.XBBApiFormGetInput;
import com.jaka.framework.core.xbongbong.api.system.from.result.XBBApiFormGetResult;
import com.jaka.framework.core.xbongbong.api.system.org.input.XBBApiDepartmentListInput;
import com.jaka.framework.core.xbongbong.api.system.org.input.XBBApiUserListInput;
import com.jaka.framework.core.xbongbong.api.system.org.result.XBBApiDepartmentListResult;
import com.jaka.framework.core.xbongbong.api.system.org.result.XBBApiUserListResult;
import com.jaka.framework.core.xbongbong.util.SignUtils;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ：james.liu
 * @date ：Created in 2021/12/22 14:35
 * @description：系统模块测试类
 * @version: 1.0
 */
public class TestXBBSystem {

    @Test
    @IsTested
    public void system_api_user_list() throws Exception {
        XBBApiUserListInput input = new XBBApiUserListInput();
        List<String> strList = new ArrayList<>();
        strList.add("1722360622859454");
        strList.add("194252543730925128");
        input.setUserIdIn(strList);
        XBBApiUserListResult apiUsetList = XBBAPIClientBuilder.getInstance().build().getSystemClient().getAPIUsetList(input);
        System.out.println(apiUsetList.toJsonString());
    }

    @Test
    @IsTested
    public void system_api_form_get() throws Exception {
        XBBApiFormGetInput input = new XBBApiFormGetInput();
        input.setFormId(null);
        XBBApiFormGetResult apiFomGet = XBBAPIClientBuilder.getInstance().build().getSystemClient().getAPIFomGet(input);
        System.out.println(apiFomGet.toJsonString());
    }

    @Test
    @IsTested
    public void system_api_department_list() throws Exception {
        XBBApiDepartmentListInput input = new XBBApiDepartmentListInput();
        XBBApiDepartmentListResult result = XBBAPIClientBuilder.getInstance().build().getSystemClient().apiDepartMentList(input);
        System.out.println(result.toJsonString());
    }


    @Test
    public void test_desc() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("corpid", "dingc2edbdcd0ad63e51");
        String dataSign = SignUtils.getDataSign(jsonObject, "7ff6521adcdc5b9b0db1335c5bcd543e");
        System.out.println(dataSign);
    }
}
