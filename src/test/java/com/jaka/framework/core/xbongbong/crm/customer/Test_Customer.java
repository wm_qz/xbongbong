package com.jaka.framework.core.xbongbong.crm.customer;

import com.jaka.framework.core.xbongbong.XBBAPIClientBuilder;
import com.jaka.framework.core.xbongbong.api.crm.customer.input.XBBApiCustomerDetailInput;
import com.jaka.framework.core.xbongbong.api.crm.customer.input.XBBApiCustomerListInput;
import com.jaka.framework.core.xbongbong.api.crm.customer.result.XBBApiCustomerDetailResult;
import com.jaka.framework.core.xbongbong.api.crm.customer.result.XBBApiCustomerListResult;
import org.junit.Test;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ：james.liu
 * @date ：Created in 2022/1/13 11:21
 * @description：
 * @version: 1.0
 */
public class Test_Customer {

    @Test
    public void test_xbb_pro_v2_api_contact_list() throws Exception {
        XBBApiCustomerListInput input = new XBBApiCustomerListInput();
        input.setPage(23);
        input.setPageSize(100);
        input.setFormId(null);
//        List<XBBApiCustomerListInput.ConditionsList> conditionsLists = new ArrayList<>();
//        XBBApiCustomerListInput.ConditionsList conditionsList = new XBBApiCustomerListInput.ConditionsList();
//        conditionsList.setAttr("text_1");
//        conditionsList.setSymbol("equal");
//        List<String> value = new ArrayList<>();
//        value.add("广州英凡电气科技有限公司");
//        conditionsList.setValue(value);
//        conditionsLists.add(conditionsList);
//        input.setConditions(conditionsLists);
        XBBApiCustomerListResult apiCustomerList = XBBAPIClientBuilder.getInstance().build().getCrmClient().getAPICustomerList(input);
        System.out.println(apiCustomerList.toJsonString());
    }

    @Test
    public void test_xbb_pro_v2_api_customer_detail() throws Exception {
        XBBApiCustomerDetailInput input = new XBBApiCustomerDetailInput();
        input.setDataId(null);
        XBBApiCustomerDetailResult result = XBBAPIClientBuilder.getInstance().build().getCrmClient().apiCustomerDetail(input);
        System.out.println(result.toJsonString());

    }


}
