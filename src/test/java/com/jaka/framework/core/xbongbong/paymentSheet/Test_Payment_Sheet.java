package com.jaka.framework.core.xbongbong.paymentSheet;

import com.jaka.framework.core.xbongbong.XBBAPIClientBuilder;
import com.jaka.framework.core.xbongbong.api.crm.contract.input.XBBApiContractListInput;
import com.jaka.framework.core.xbongbong.api.paymentSheet.input.ProV2ApiPaymentSheetDetailInput;
import com.jaka.framework.core.xbongbong.api.paymentSheet.input.ProV2ApiPaymentSheetListInput;
import com.jaka.framework.core.xbongbong.api.paymentSheet.result.ProV2ApiPaymentSheetDetailResult;
import com.jaka.framework.core.xbongbong.api.paymentSheet.result.ProV2ApiPaymentSheetListResult;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ：james.liu
 * @date ：Created in 2022/11/21 14:54
 * @description：
 * @version: 1.0
 */
public class Test_Payment_Sheet {

    @Test
    public void test_payment_sheet_list() throws Exception {
        ProV2ApiPaymentSheetListInput input = new ProV2ApiPaymentSheetListInput();
        List<ProV2ApiPaymentSheetListInput.Conditions> conditionsLists = new ArrayList<>();
        ProV2ApiPaymentSheetListInput.Conditions conditionsList = new ProV2ApiPaymentSheetListInput.Conditions();
        conditionsList.setAttr("serialNo");
        conditionsList.setSymbol("equal");
        List<String> value = new ArrayList<>();
        value.add("RMO.20221130008");
        conditionsList.setValue(value);
        conditionsLists.add(conditionsList);
        input.setConditions(conditionsLists);
        ProV2ApiPaymentSheetListResult result =
                XBBAPIClientBuilder.getInstance().build().getPaymentSheetClient().apiPaymentSheetList(input);
        System.out.println(result.toJsonString());
    }

    @Test
    public void test_payment_sheet_detail() throws Exception {
        ProV2ApiPaymentSheetDetailInput input = new ProV2ApiPaymentSheetDetailInput();
        input.setDataId(null);
        ProV2ApiPaymentSheetDetailResult result =
                XBBAPIClientBuilder.getInstance().build().getPaymentSheetClient().apiPaymentSheetDetail(input);
        System.out.println(result.toJsonString());

    }
}
