package com.jaka.framework.core.xbongbong.callback;

import com.jaka.framework.core.xbongbong.api.callback.WebHookCallBackClient;
import org.junit.Test;

/**
 * @author ：james.liu
 * @date ：Created in 2022/10/8 13:06
 * @description：
 * @version: 1.0
 */
public class TestCallBack {

    @Test
    public void test_call_back() {
        String callback = "{ \"corpid\":\"XXX\", \"dataId\":4232488L, \"formId\":4232488, \"operate\":\"edit\",\n" +
                "        \"saasMark\":1, \"type\":\"contract\" }";
        WebHookCallBackClient client = new WebHookCallBackClient();
        client.callback(callback, null);
    }
}
