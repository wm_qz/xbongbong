package com.jaka.framework.core.xbongbong.product;

import com.jaka.framework.core.xbongbong.XBBAPIClientBuilder;
import com.jaka.framework.core.xbongbong.api.product.input.ProV2ApiProductListInput;
import com.jaka.framework.core.xbongbong.api.product.input.XBBApiProductDetailInput;
import com.jaka.framework.core.xbongbong.api.product.result.ProV2ApiProductListResult;
import com.jaka.framework.core.xbongbong.api.product.result.dto.XBBApiProductDetailResult;
import org.junit.Test;

/**
 * @author ：james.liu
 * @date ：Created in 2022/11/29 15:33
 * @description：
 * @version: 1.0
 */
public class Test_Product {

    @Test
    public void test_xbb_api_product_list() throws Exception {
        ProV2ApiProductListInput input = new ProV2ApiProductListInput();
        input.setPageSize(100);
        ProV2ApiProductListResult proV2ApiProductListResult = XBBAPIClientBuilder.getInstance().build().getProductClient().proV2ApiProductList(input);
        System.out.println(proV2ApiProductListResult.toJsonString());

    }

    @Test
    public void test_xbb_api_product_detail() throws Exception {
        XBBApiProductDetailInput input = new XBBApiProductDetailInput();
        input.setDataId(null);
        XBBApiProductDetailResult proV2ApiProductListResult =
                XBBAPIClientBuilder.getInstance().build().getProductClient().proV2ApiProductDetail(input);
        System.out.println(proV2ApiProductListResult.toJsonString());

    }
}
