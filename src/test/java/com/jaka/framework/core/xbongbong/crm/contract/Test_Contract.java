package com.jaka.framework.core.xbongbong.crm.contract;

import com.jaka.framework.core.xbongbong.XBBAPIClientBuilder;
import com.jaka.framework.core.xbongbong.api.crm.contract.input.XBBApiContractDetailInput;
import com.jaka.framework.core.xbongbong.api.crm.contract.input.XBBApiContractListInput;
import com.jaka.framework.core.xbongbong.api.crm.contract.result.XBBApiContractDetailResult;
import com.jaka.framework.core.xbongbong.api.crm.contract.result.XBBApiContractListResult;
import org.junit.Test;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.jaka.framework.common.core.utils.DateUtils.getNowStartDateTime;

/**
 * @author ：james.liu
 * @date ：Created in 2022/11/2 17:03
 * @description：
 * @version: 1.0
 */
public class Test_Contract {

    @Test
    public void test_contract_list() throws Exception {
        XBBApiContractListInput input = new XBBApiContractListInput();
        input.setFormId(null);
        List<XBBApiContractListInput.ConditionsList> conditionsLists = new ArrayList<>();
        XBBApiContractListInput.ConditionsList conditionsList = new XBBApiContractListInput.ConditionsList();
        conditionsList.setAttr("serialNo");
        conditionsList.setSymbol("equal");
        List<String> value = new ArrayList<>();
        value.add("JKSD-20221129007");
        conditionsList.setValue(value);
        conditionsLists.add(conditionsList);
        input.setConditions(conditionsLists);
        XBBApiContractListResult xbbApiContractListResult = XBBAPIClientBuilder.getInstance().build().getCrmClient().apiContractList(input);
        System.out.println(xbbApiContractListResult.toJsonString());
    }


    @Test
    public void test_contract_list_data() throws Exception {
        Date date = getNowStartDateTime();
        Long startTime = LocalDateTime.ofInstant(date.toInstant(), ZoneOffset.UTC).toEpochSecond(ZoneOffset.UTC);
        Long endTime = LocalDateTime.ofInstant(new Date().toInstant(), ZoneOffset.UTC).toEpochSecond(ZoneOffset.UTC);
        XBBApiContractListInput input = new XBBApiContractListInput();
        input.setFormId(null);
        List<XBBApiContractListInput.ConditionsList> conditionsLists = new ArrayList<>();
        XBBApiContractListInput.ConditionsList conditionsList = new XBBApiContractListInput.ConditionsList();
        // 判断修改时间在设定范围
        conditionsList.setAttr("updateTime");
        conditionsList.setSymbol("range");
        List<String> value = new ArrayList<>();
        value.add(String.valueOf(startTime));
        value.add(String.valueOf(endTime));
        conditionsList.setValue(value);
        conditionsLists.add(conditionsList);
        input.setConditions(conditionsLists);
        XBBApiContractListResult xbbApiContractListResult = XBBAPIClientBuilder.getInstance().build().getCrmClient().apiContractList(input);
        System.out.println(xbbApiContractListResult.toJsonString());
    }

    @Test
    public void test_contract_detail() throws Exception {
        XBBApiContractDetailInput input = new XBBApiContractDetailInput();
        input.setDataId(null);
        XBBApiContractDetailResult xbbApiContractDetailResult = XBBAPIClientBuilder.getInstance().build().getCrmClient().apiContractDetail(input);
        System.out.println(xbbApiContractDetailResult.toJsonString());
    }


}
