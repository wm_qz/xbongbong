package com.jaka.framework.core.xbongbong.work;

import com.jaka.framework.core.xbongbong.XBBAPIClientBuilder;
import com.jaka.framework.core.xbongbong.api.work.enums.*;
import com.jaka.framework.core.xbongbong.api.work.input.ApiWorkOrderAddInput;
import com.jaka.framework.core.xbongbong.api.work.input.ApiWorkOrderTemplateDetailInput;
import com.jaka.framework.core.xbongbong.api.work.input.ApiWorkOrderTemplateListInput;
import com.jaka.framework.core.xbongbong.api.work.result.ApiWorkOrderAddResult;
import com.jaka.framework.core.xbongbong.api.work.result.ApiWorkOrderTemplateDetailResult;
import com.jaka.framework.core.xbongbong.api.work.result.ApiWorkOrderTemplateListResult;
import org.junit.Test;

/**
 * @author ：james.liu
 * @date ：Created in 2022/8/17 10:06
 * @description：
 * @version: 1.0
 */
public class WorkOrderTest {

    @Test
    public void test_work_order_template_list() throws Exception {
        ApiWorkOrderTemplateListInput input = new ApiWorkOrderTemplateListInput();
        ApiWorkOrderTemplateListResult result =
                XBBAPIClientBuilder.getInstance().build().getWorkOrderClient().workOrderTemplateList(input);
        System.out.println(result.toJsonString());
    }

    @Test
    public void test_work_order_template_detail() throws Exception {
        ApiWorkOrderTemplateDetailInput input = new ApiWorkOrderTemplateDetailInput();
        input.setFormId(null);
        ApiWorkOrderTemplateDetailResult result =
                XBBAPIClientBuilder.getInstance().build().getWorkOrderClient().workOrderTemplateDetail(input);
        System.out.println(result.toJsonString());
    }

    @Test
    public void test_work_order_add() throws Exception {
        ApiWorkOrderAddInput input = new ApiWorkOrderAddInput();
        input.setSerialNo("xxxx");
        input.setText_13("测试工单无需处理");
        input.setText_15(ServiceSourceEnums.SERVICE_SOURCE_2.getValue());
        input.setText_17(DemandDepartEnums.DEPART_5.getValue());
        input.setText_18(ServiceContentEnums.SERVICE_CONTENT_3.getValue());
        input.setDate_2("2022-08-17");
        input.setText_16(CustomerChannelEnums.CUSTOMER_CHANNEL_4.getValue());
        input.setNum_4("1");
        input.setText_22("Zu110110");
        input.setText_5("测试工单无需处理");
        input.setText_6(UrgencyEnums.URGENCY_1.getValue());
        input.setAddTime("2022-08-17 17:44:00");
        //项目编码
        input.setText_20("------");
        input.setStatus(PhaseStatusEnums.CUSTOMER_CHANNEL_2.getValue());
        //阶段状态
        input.setNum_3(WorkOrderStatusEnums.CUSTOMER_CHANNEL_2.getValue());
        //创建人
        input.setCreatorId("024661606726079850");
        // 负责人
        //input.setOwnerId("121");
        input.setDepartmentId("1212");
        // 部门
        input.setText_25("010");
        input.setUpdateTime("2022-08-17 17:44:00");
        // 客户名称
        input.setText_21("上海节卡机器人");
        //工单创建人
        input.setText_24("024661606726079850");
        //工单负责人
        input.setText_11("024661606726079850");
        // 地址
        input.setAddress_1("测试地址未知");
        //客户编码
        input.setText_19("位置");
        // 联系方式
        input.setSubForm_1("18501629108");
        //关联机会
        input.setText_3("无");
        //客户名称
        input.setText_2("上海节卡机器人");
        //工单负责人
        input.setText_1("024661606726079850");
        //协同人
        input.setCoUserId("024661606726079850");
        //海外客户名称
        input.setText_23("");
        ApiWorkOrderAddResult result =
                XBBAPIClientBuilder.getInstance().build().getWorkOrderClient().apiWorkOrderAdd(input);
        System.out.println(result.toJsonString());
    }

    /**
     *{
     *     "code": 1,
     *     "msg": "操作成功",
     *     "result": {
     *         "list": [
     *             {
     *                 "formId": 24109,
     *                 "isEnable": 0,
     *                 "name": "客诉工单"
     *             },
     *             {
     *                 "formId": 24006,
     *                 "isEnable": 0,
     *                 "name": "销售服务工单"
     *             },
     *             {
     *                 "formId": 24060,
     *                 "isEnable": 1,
     *                 "name": "服务工单"
     *             },
     *             {
     *                 "formId": 10548,
     *                 "isEnable": 0,
     *                 "name": "节卡机器人售前服务"
     *             },
     *             {
     *                 "formId": 10547,
     *                 "isEnable": 0,
     *                 "name": "节卡机器人售后服务"
     *             },
     *             {
     *                 "formId": 10606,
     *                 "isEnable": 0,
     *                 "name": "节卡机器人技术服务"
     *             },
     *             {
     *                 "formId": 18193,
     *                 "isEnable": 0,
     *                 "name": "工程支持工单"
     *             },
     *             {
     *                 "formId": 22177,
     *                 "isEnable": 0,
     *                 "name": "新工单"
     *             },
     *             {
     *                 "formId": 10543,
     *                 "isEnable": 0,
     *                 "name": "维护维修"
     *             },
     *             {
     *                 "formId": 10545,
     *                 "isEnable": 0,
     *                 "name": "走访"
     *             }
     *         ]
     *     },
     *     "success": true
     * }
     */

}
