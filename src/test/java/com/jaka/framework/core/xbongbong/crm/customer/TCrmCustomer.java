package com.jaka.framework.core.xbongbong.crm.customer;


import java.util.Date;

public class TCrmCustomer {

    private Long id;
    private Long formId;
    private Long dataId;
    private Date addTime;
    private Date updateTime;
    private String text1;
    private String text42;
    private String text44;
    private String text45;
    private String text46;
    private String ownerId;
    private String text27;
    private String text40;
    private String text4;
    private String text18;
    private String text41;
    private String text43;
    private String coUserId;
    private String text23;
    private String text24;
    private String text25;
    private String array4;
    private String text5;
    private String array3;
    private String text6;
    private String array2;
    private String array5;
    private String text7;
    private String text9;
    private String array31;
    private String array6;
    private String text3;
    private String text39;
    private String address1;
    private String text38;
    private String text50;
    private String text16;
    private String subForm1;
    private Integer num10;
    private Integer num9;
    private String text11;
    private String text47;
    private String text26;
    private String file1;
    private String file2;
    private String text13;
    private Date date2;
    private Date date1;
    private String text31;
    private String departmentId;
    private String text28;
    private String text29;
    private String text32;
    private String text33;
    private String text34;
    private String text35;
    private String text36;
    private String text30;
    private String text37;
    private String text17;
    private String text81;
    private String text48;
    private Integer num7;
    private Integer num8;
    private Integer num5;
    private Integer num6;
    private String text12;
    private String subForm2;
    private Date date3;
    private Date date4;
    private Integer num3;
    private Integer num4;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public Long getFormId() {
        return formId;
    }

    public void setFormId(Long formId) {
        this.formId = formId;
    }


    public Long getDataId() {
        return dataId;
    }

    public void setDataId(Long dataId) {
        this.dataId = dataId;
    }


    public String getText1() {
        return text1;
    }

    public void setText1(String text1) {
        this.text1 = text1;
    }


    public String getText42() {
        return text42;
    }

    public void setText42(String text42) {
        this.text42 = text42;
    }


    public String getText44() {
        return text44;
    }

    public void setText44(String text44) {
        this.text44 = text44;
    }


    public String getText45() {
        return text45;
    }

    public void setText45(String text45) {
        this.text45 = text45;
    }


    public String getText46() {
        return text46;
    }

    public void setText46(String text46) {
        this.text46 = text46;
    }


    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }


    public String getText27() {
        return text27;
    }

    public void setText27(String text27) {
        this.text27 = text27;
    }


    public String getText40() {
        return text40;
    }

    public void setText40(String text40) {
        this.text40 = text40;
    }


    public String getText4() {
        return text4;
    }

    public void setText4(String text4) {
        this.text4 = text4;
    }


    public String getText18() {
        return text18;
    }

    public void setText18(String text18) {
        this.text18 = text18;
    }


    public String getText41() {
        return text41;
    }

    public void setText41(String text41) {
        this.text41 = text41;
    }


    public String getText43() {
        return text43;
    }

    public void setText43(String text43) {
        this.text43 = text43;
    }


    public String getCoUserId() {
        return coUserId;
    }

    public void setCoUserId(String coUserId) {
        this.coUserId = coUserId;
    }


    public String getText23() {
        return text23;
    }

    public void setText23(String text23) {
        this.text23 = text23;
    }


    public String getText24() {
        return text24;
    }

    public void setText24(String text24) {
        this.text24 = text24;
    }


    public String getText25() {
        return text25;
    }

    public void setText25(String text25) {
        this.text25 = text25;
    }


    public String getArray4() {
        return array4;
    }

    public void setArray4(String array4) {
        this.array4 = array4;
    }


    public String getText5() {
        return text5;
    }

    public void setText5(String text5) {
        this.text5 = text5;
    }


    public String getArray3() {
        return array3;
    }

    public void setArray3(String array3) {
        this.array3 = array3;
    }


    public String getText6() {
        return text6;
    }

    public void setText6(String text6) {
        this.text6 = text6;
    }


    public String getArray2() {
        return array2;
    }

    public void setArray2(String array2) {
        this.array2 = array2;
    }


    public String getArray5() {
        return array5;
    }

    public void setArray5(String array5) {
        this.array5 = array5;
    }


    public String getText7() {
        return text7;
    }

    public void setText7(String text7) {
        this.text7 = text7;
    }


    public String getText9() {
        return text9;
    }

    public void setText9(String text9) {
        this.text9 = text9;
    }


    public String getArray31() {
        return array31;
    }

    public void setArray31(String array31) {
        this.array31 = array31;
    }


    public String getArray6() {
        return array6;
    }

    public void setArray6(String array6) {
        this.array6 = array6;
    }


    public String getText3() {
        return text3;
    }

    public void setText3(String text3) {
        this.text3 = text3;
    }


    public String getText39() {
        return text39;
    }

    public void setText39(String text39) {
        this.text39 = text39;
    }


    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }


    public String getText38() {
        return text38;
    }

    public void setText38(String text38) {
        this.text38 = text38;
    }


    public String getText50() {
        return text50;
    }

    public void setText50(String text50) {
        this.text50 = text50;
    }


    public String getText16() {
        return text16;
    }

    public void setText16(String text16) {
        this.text16 = text16;
    }


    public String getSubForm1() {
        return subForm1;
    }

    public void setSubForm1(String subForm1) {
        this.subForm1 = subForm1;
    }


    public Integer getNum10() {
        return num10;
    }

    public void setNum10(Integer num10) {
        this.num10 = num10;
    }


    public Integer getNum9() {
        return num9;
    }

    public void setNum9(Integer num9) {
        this.num9 = num9;
    }


    public String getText11() {
        return text11;
    }

    public void setText11(String text11) {
        this.text11 = text11;
    }


    public String getText47() {
        return text47;
    }

    public void setText47(String text47) {
        this.text47 = text47;
    }


    public String getText26() {
        return text26;
    }

    public void setText26(String text26) {
        this.text26 = text26;
    }


    public String getFile1() {
        return file1;
    }

    public void setFile1(String file1) {
        this.file1 = file1;
    }


    public String getFile2() {
        return file2;
    }

    public void setFile2(String file2) {
        this.file2 = file2;
    }


    public String getText13() {
        return text13;
    }

    public void setText13(String text13) {
        this.text13 = text13;
    }


    public Date getDate2() {
        return date2;
    }

    public void setDate2(Date date2) {
        this.date2 = date2;
    }


    public Date getDate1() {
        return date1;
    }

    public void setDate1(Date date1) {
        this.date1 = date1;
    }


    public String getText31() {
        return text31;
    }

    public void setText31(String text31) {
        this.text31 = text31;
    }


    public String getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(String departmentId) {
        this.departmentId = departmentId;
    }


    public String getText28() {
        return text28;
    }

    public void setText28(String text28) {
        this.text28 = text28;
    }


    public String getText29() {
        return text29;
    }

    public void setText29(String text29) {
        this.text29 = text29;
    }


    public String getText32() {
        return text32;
    }

    public void setText32(String text32) {
        this.text32 = text32;
    }


    public String getText33() {
        return text33;
    }

    public void setText33(String text33) {
        this.text33 = text33;
    }


    public String getText34() {
        return text34;
    }

    public void setText34(String text34) {
        this.text34 = text34;
    }


    public String getText35() {
        return text35;
    }

    public void setText35(String text35) {
        this.text35 = text35;
    }


    public String getText36() {
        return text36;
    }

    public void setText36(String text36) {
        this.text36 = text36;
    }


    public String getText30() {
        return text30;
    }

    public void setText30(String text30) {
        this.text30 = text30;
    }


    public String getText37() {
        return text37;
    }

    public void setText37(String text37) {
        this.text37 = text37;
    }


    public String getText17() {
        return text17;
    }

    public void setText17(String text17) {
        this.text17 = text17;
    }


    public String getText81() {
        return text81;
    }

    public void setText81(String text81) {
        this.text81 = text81;
    }


    public String getText48() {
        return text48;
    }

    public void setText48(String text48) {
        this.text48 = text48;
    }


    public Integer getNum7() {
        return num7;
    }

    public void setNum7(Integer num7) {
        this.num7 = num7;
    }


    public Integer getNum8() {
        return num8;
    }

    public void setNum8(Integer num8) {
        this.num8 = num8;
    }


    public Integer getNum5() {
        return num5;
    }

    public void setNum5(Integer num5) {
        this.num5 = num5;
    }


    public Integer getNum6() {
        return num6;
    }

    public void setNum6(Integer num6) {
        this.num6 = num6;
    }


    public String getText12() {
        return text12;
    }

    public void setText12(String text12) {
        this.text12 = text12;
    }


    public String getSubForm2() {
        return subForm2;
    }

    public void setSubForm2(String subForm2) {
        this.subForm2 = subForm2;
    }


    public Date getDate3() {
        return date3;
    }

    public void setDate3(Date date3) {
        this.date3 = date3;
    }


    public Date getDate4() {
        return date4;
    }

    public void setDate4(Date date4) {
        this.date4 = date4;
    }


    public Integer getNum3() {
        return num3;
    }

    public void setNum3(Integer num3) {
        this.num3 = num3;
    }


    public Integer getNum4() {
        return num4;
    }

    public void setNum4(Integer num4) {
        this.num4 = num4;
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
