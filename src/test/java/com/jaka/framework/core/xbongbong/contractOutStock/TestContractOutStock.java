package com.jaka.framework.core.xbongbong.contractOutStock;

import com.alibaba.fastjson.JSON;
import com.jaka.framework.core.xbongbong.XBBAPIClientBuilder;
import com.jaka.framework.core.xbongbong.api.contractOutstock.input.ProV2ApiContractOutStockAddInput;
import com.jaka.framework.core.xbongbong.api.contractOutstock.input.ProV2ApiContractOutStockDetailInput;
import com.jaka.framework.core.xbongbong.api.contractOutstock.input.ProV2ApiContractOutStockListInput;
import com.jaka.framework.core.xbongbong.api.contractOutstock.input.dto.save.*;
import com.jaka.framework.core.xbongbong.api.contractOutstock.result.ProV2ApiContractOutStockAddResult;
import com.jaka.framework.core.xbongbong.api.contractOutstock.result.ProV2ApiContractOutStockDetailResult;
import com.jaka.framework.core.xbongbong.api.contractOutstock.result.ProV2ApiContractOutStockListResult;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ：james.liu
 * @date ：Created in 2022/12/2 16:31
 * @description：
 * @version: 1.0
 */
public class TestContractOutStock {

    @Test
    public void test_xbb_api_contract_out_stock_detail() throws Exception {
        ProV2ApiContractOutStockDetailInput input = new ProV2ApiContractOutStockDetailInput();
        input.setDataId(null);
        ProV2ApiContractOutStockDetailResult result =
                XBBAPIClientBuilder.getInstance().build().getContractOutStockClient().proV2ApiContractOutStockDetail(input);
        System.out.println(JSON.toJSONString(result));
    }


    @Test
    public void test_xbb_api_contract_out_stock_list() throws Exception {
        ProV2ApiContractOutStockListInput input = new ProV2ApiContractOutStockListInput();
        List<ProV2ApiContractOutStockListInput.ConditionsList> conditionsLists = new ArrayList<>();
        ProV2ApiContractOutStockListInput.ConditionsList conditionsList =
                new ProV2ApiContractOutStockListInput.ConditionsList();
        conditionsList.setAttr("serialNo");
        conditionsList.setSymbol("equal");
        List<String> values = new ArrayList<>();
        values.add("");
        conditionsList.setValue(values);
        conditionsLists.add(conditionsList);
        input.setConditions(conditionsLists);
        ProV2ApiContractOutStockListResult result =
                XBBAPIClientBuilder.getInstance().build().getContractOutStockClient().proV2ApiContractOutStockList(input);
        System.out.println(result.toJsonString());
    }

    /**
     * 销售出库单
     */
    @Test
    public void test_xbb_api_contract_out_stock() throws Exception {
        ProV2ApiContractOutStockAddInput input = new ProV2ApiContractOutStockAddInput();
        ProV2ApiContractOutStockAddDataList dataList = new ProV2ApiContractOutStockAddDataList();
        dataList.setText_4(18137973);
        dataList.setSerialNo("TEST-XSCKD006502");
        dataList.setDate_1(1669858021);
        List<String> ownerIds = new ArrayList<>();
        ownerIds.add("206149002037796264");
        dataList.setOwnerId(ownerIds);
        dataList.setText_66("92199928");
        dataList.setText_2(13686);
        List<ProV2ApiContractOutStockAddArray_1> array_1 = new ArrayList<>();
        ProV2ApiContractOutStockAddArray_1 apiContractOutStockAddArray_1 = new ProV2ApiContractOutStockAddArray_1();
        apiContractOutStockAddArray_1.setNum_3(1);
//        num_2.setNum_2(1);
        apiContractOutStockAddArray_1.setNum_2(0);
        apiContractOutStockAddArray_1.setNum_4(2);
        apiContractOutStockAddArray_1.setText_1(4949688);
        apiContractOutStockAddArray_1.setText_6("13686");
//        apiContractOutStockAddArray_1.setText_8("台（单机）");
        array_1.add(apiContractOutStockAddArray_1);
        dataList.setArray_1(array_1);

        dataList.setNum_38(100);

        List<ProV2ApiContractOutStockSubForm_1> stockSubForm1s = new ArrayList<>();
        ProV2ApiContractOutStockSubForm_1 proV2ApiContractOutStockSubForm_1 = new ProV2ApiContractOutStockSubForm_1();
        proV2ApiContractOutStockSubForm_1.setText_1("工作");
        proV2ApiContractOutStockSubForm_1.setText_2("18501629108");
        stockSubForm1s.add(proV2ApiContractOutStockSubForm_1);
        dataList.setSubForm_3(stockSubForm1s);


        ProV2ApiContractOutStockAddAddress_2 apiContractOutStockAddAddress_2 = new ProV2ApiContractOutStockAddAddress_2();
        apiContractOutStockAddAddress_2.setAddress(" ");
        apiContractOutStockAddAddress_2.setCity(" ");
        apiContractOutStockAddAddress_2.setDistrict(" ");
        apiContractOutStockAddAddress_2.setProvince(" ");
//        ProV2ApiContractOutStockAddAddress_2.ProV2ApiContractOutStockAddAddress_2Location location =
//                new ProV2ApiContractOutStockAddAddress_2.ProV2ApiContractOutStockAddAddress_2Location();
//        location.setLat(30.18899f);
//        location.setLon(30.18899f);
//        apiContractOutStockAddAddress_2.setLocation(location);
        dataList.setAddress_2(apiContractOutStockAddAddress_2);

        input.setDataList(dataList);
        ProV2ApiContractOutStockAddResult result =
                XBBAPIClientBuilder.getInstance().build().getContractOutStockClient().saveContractOutStock(input);
        System.out.println(result.toJsonString());
    }

    /**
     * 金蝶 销售出库单 修改
     * @throws Exception
     */
    @Test
    public void test_xbb_api_update_contract_out_stock() throws Exception {
        ProV2ApiContractOutStockAddInput input = new ProV2ApiContractOutStockAddInput();
        ProV2ApiContractOutStockAddDataList dataList = new ProV2ApiContractOutStockAddDataList();
        dataList.setText_4(18137973);
        dataList.setSerialNo("TEST-XSCKD006502");
        dataList.setDate_1(1669858021);
        List<String> ownerIds = new ArrayList<>();
        ownerIds.add("206149002037796264");
        dataList.setOwnerId(ownerIds);
        dataList.setText_66("92199928");
        dataList.setText_2(13686);
        List<ProV2ApiContractOutStockAddArray_1> array_1 = new ArrayList<>();
        ProV2ApiContractOutStockAddArray_1 apiContractOutStockAddArray_1 = new ProV2ApiContractOutStockAddArray_1();
        apiContractOutStockAddArray_1.setNum_3(1);
//        num_2.setNum_2(1);
        apiContractOutStockAddArray_1.setNum_2(0);
        apiContractOutStockAddArray_1.setNum_4(2);
        apiContractOutStockAddArray_1.setText_1(4949688);
        apiContractOutStockAddArray_1.setText_6("13686");
//        apiContractOutStockAddArray_1.setText_8("台（单机）");
        array_1.add(apiContractOutStockAddArray_1);
        dataList.setArray_1(array_1);

        dataList.setNum_38(100);

        List<ProV2ApiContractOutStockSubForm_1> stockSubForm1s = new ArrayList<>();
        ProV2ApiContractOutStockSubForm_1 proV2ApiContractOutStockSubForm_1 = new ProV2ApiContractOutStockSubForm_1();
        proV2ApiContractOutStockSubForm_1.setText_1("工作");
        proV2ApiContractOutStockSubForm_1.setText_2("18501629108");
        stockSubForm1s.add(proV2ApiContractOutStockSubForm_1);
        dataList.setSubForm_3(stockSubForm1s);


        ProV2ApiContractOutStockAddAddress_2 apiContractOutStockAddAddress_2 = new ProV2ApiContractOutStockAddAddress_2();
        apiContractOutStockAddAddress_2.setAddress(" ");
        apiContractOutStockAddAddress_2.setCity(" ");
        apiContractOutStockAddAddress_2.setDistrict(" ");
        apiContractOutStockAddAddress_2.setProvince(" ");
//        ProV2ApiContractOutStockAddAddress_2.ProV2ApiContractOutStockAddAddress_2Location location =
//                new ProV2ApiContractOutStockAddAddress_2.ProV2ApiContractOutStockAddAddress_2Location();
//        location.setLat(30.18899f);
//        location.setLon(30.18899f);
//        apiContractOutStockAddAddress_2.setLocation(location);
        dataList.setAddress_2(apiContractOutStockAddAddress_2);

        input.setDataList(dataList);
        ProV2ApiContractOutStockAddResult result =
                XBBAPIClientBuilder.getInstance().build().getContractOutStockClient().saveContractOutStock(input);
        System.out.println(result.toJsonString());
    }

}
