package com.jaka.framework.core.xbongbong.crm.sales;

import com.jaka.framework.core.xbongbong.XBBAPIClientBuilder;
import com.jaka.framework.core.xbongbong.api.crm.sales.input.ProV2ApiOpportunityListInput;
import com.jaka.framework.core.xbongbong.api.crm.sales.result.ProV2ApiOpportunityListResult;
import org.junit.Test;

/**
 * @author ：james.liu
 * @date ：Created in 2022/1/29 9:26
 * @description：销售机会测试类
 * @version: 1.0
 */
public class Test_xbb_sales {

    /**
     * 销售机会列表
     *
     * @throws Exception
     */
    @Test
    public void test_xbb_pro_v2_api_opportunity_list() throws Exception {
        ProV2ApiOpportunityListInput input = new ProV2ApiOpportunityListInput();
        ProV2ApiOpportunityListResult result = XBBAPIClientBuilder.getInstance().build().getCrmClient().proV2ApiOpportunityList(input);
        System.out.println(result.toJsonString());
    }
}
