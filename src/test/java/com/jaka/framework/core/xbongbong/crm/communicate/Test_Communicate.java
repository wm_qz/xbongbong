package com.jaka.framework.core.xbongbong.crm.communicate;

import com.jaka.framework.core.xbongbong.XBBAPIClientBuilder;
import com.jaka.framework.core.xbongbong.api.crm.communicate.input.ProV2ApiCommunicateListInput;
import com.jaka.framework.core.xbongbong.api.crm.communicate.result.ProV2ApiCommunicateListResult;
import org.junit.Test;

/**
 * @author ：james.liu
 * @date ：Created in 2022/1/28 16:05
 * @description：
 * @version: 1.0
 */
public class Test_Communicate {


    @Test
    public void test_xbb_pro_v2_api_contact_list() throws Exception {
        ProV2ApiCommunicateListInput input = new ProV2ApiCommunicateListInput();
        input.setPage(1);
        input.setPageSize(100);
//        input.setFormId(1680749L);
        ProV2ApiCommunicateListResult apiCustomerList = XBBAPIClientBuilder.getInstance().build().getCrmClient().proV2ApiCommunicateList(input);
        System.out.println(apiCustomerList.toJsonString());
    }

}
